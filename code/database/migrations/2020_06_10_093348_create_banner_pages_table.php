<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBannerPagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('banner_pages', function (Blueprint $table) {
            $table->id();
            $table->string('banner_category');
            $table->string('sup_title')->nullable();
            $table->string('title')->nullable();
            $table->text('description');
            $table->text('image');
            $table->string('url_page')->nullable();
            $table->text('meta_tags')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('banner_pages');
    }
}
