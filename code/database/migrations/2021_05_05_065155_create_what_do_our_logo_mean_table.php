<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateWhatDoOurLogoMeanTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('what_do_our_logo_mean', function (Blueprint $table) {
            $table->id();
            $table->string('section');
            $table->string('title');
            $table->text('description');
            $table->string('image');
            $table->string('image_orientation');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('what_do_our_logo_mean');
    }
}
