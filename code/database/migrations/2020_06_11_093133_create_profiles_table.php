<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProfilesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('profiles', function (Blueprint $table) {
            $table->id();
            $table->string('title');
            $table->text('sup_description');
            $table->text('description');
            $table->string('image');
            $table->text('address');
            $table->text('customer_service')->nullable();
            $table->string('cs_image');
            $table->text('map_embed');
            $table->string('email');
            $table->string('no_telp');
            $table->string('sosmed_fb')->nullable();
            $table->string('sosmed_twitter')->nullable();
            $table->string('sosmed_ig')->nullable();
            $table->string('sosmed_ig_2')->nullable();
            $table->text('meta_tags')->nullable();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('profiles');
    }
}
