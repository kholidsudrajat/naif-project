<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {

//     return view('homepage');
// });
 Route::get('sidebar', function () {
     return view('web.layouts.sidebar');
 });
Route::get('/', 'HomeController@index');

Route::get('article/{slug}', 'HomeController@article');

Route::get('/loadmore', 'HomeController@index');
Route::post('/loadmore/load_data', 'HomeController@load_data')->name('loadmore.load_data');

Route::get('friends-club', 'Web\FriendsClubController@index');
Route::post('get-city', 'Web\FriendsClubController@get_city');
Route::post('get-subdistrict', 'Web\FriendsClubController@get_subdistrict');
Route::post('store-friends-club', 'Web\FriendsClubController@store_friendsClub');

Route::get('contact-us', 'Web\ContactUsController@index');
Route::post('store-contact-us', 'Web\ContactUsController@store_contact_us');

Route::get('ingredients', 'Web\IngredientsController@index');
Route::get('ingredients/get-maxid', 'Web\IngredientsController@get_maxId');
Route::get('ingredients/get-minid', 'Web\IngredientsController@get_minId');
Route::post('ingredients/load-data', 'Web\IngredientsController@load_data')->name('ingredients.load-data');
Route::post('ingredients/load-data-detail', 'Web\IngredientsController@load_data_detail')->name('ingredients.load-data-detail');

Route::resource('blogs', 'Web\BlogsController');
Route::resource('about-us', 'Web\AboutUsController');
Route::resource('what-do-our-logo-mean', 'Web\WhatDoOurLogoMeanController');
Route::resource('faq', 'Web\FaqController');

Route::get('result', 'Web\SearchResultController@search_data');
Route::get('livesearch', 'Web\SearchResultController@livesearch');


// Route::get('what-do-our-logo-mean', 'whatDoOurLogoMeanController@index');


// Auth::routes();

// Route::get('/home', 'HomeController@index')->name('home');



Route::get('roles', 'Admin\PermissionController@permission');



Route::group(['prefix' => 'admin'], function () {

    Auth::routes();

    Route::resource('roles', 'Admin\RoleController');

    Route::group(['middleware' => 'role:superadmin'], function () {

        Route::resource('user-access', 'Admin\UserAccessController');
    });


    Route::get('/home', 'Admin\AdminController@index');

    Route::resource('profile', 'Admin\ProfileController');

    // Route::get('register', 'Admin\UserAccessController@register');

    Route::resource('contact-us', 'Admin\ContactUsController');
    Route::get('contact-us/export', 'Admin\ContactUsController@index');
    Route::post('contact-us/load_data', 'Admin\ContactUsController@load_data');
    Route::get('/contact-confirm/confirm/{id}', 'Admin\ContactUsController@contact_confirm');

    Route::resource('friends-club', 'Admin\FriendsClubController');
    Route::get('/friends-club/export', 'Admin\FriendsClubController@index');
    Route::post('friends-club/load_data', 'Admin\FriendsClubController@load_data');

    Route::get('/friends-confirm/confirm/{id}', 'Admin\FriendsClubController@friends_confirm');

    Route::resource('article', 'Admin\ArticleController');
    Route::resource('banner', 'Admin\BannerPageController');

    Route::get('comfort/faq-product', 'Admin\FaqProductController@create');
    Route::post('comfort/store-faq', 'Admin\FaqProductController@store');
    Route::post('comfort/destroy-faq', 'Admin\FaqProductController@destroy');

    Route::resource('ingredients', 'Admin\IngredientsController');

    Route::resource('faq-naif-baby', 'Admin\FaqNaifBabyController');
    Route::resource('faq-naif-care', 'Admin\FaqNaifCareController');
    Route::resource('faq-other', 'Admin\FaqOtherController');

    Route::resource('url-menu', 'Admin\UrlMenuController');

    Route::resource('friends-club-benefit', 'Admin\FriendsClubBenefitController');

    Route::resource('about-us', 'Admin\AboutUsController');

    Route::resource('what-do-our-logo-mean', 'Admin\WhatDoOurLogoMeanController');

});


Route::get('date-range', function () {
    return view('admin/example/date-rangepicker');
});
