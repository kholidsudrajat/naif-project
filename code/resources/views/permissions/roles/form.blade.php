

<div class="form-group row">
    <label class="control-label col-md-3">Name</label>
    <div class="col-md-8">
        {!! Form::text('name', null,['class' => 'form-control', 'placeholder' => 'Enter username', 'required'=>'required']) !!}

    </div>
</div>
<div class="form-group row">
    <label class="control-label col-md-3">E-Mail Address</label>
    <div class="col-md-8">
        @foreach($permission as $value)
            <label>{{ Form::checkbox('permission[]', $value->id, false, array('class' => 'name')) }}
            {{ $value->name }}</label>
        <br/>
        @endforeach

    </div>
</div>
<div class="form-group row">
    <div class="col-md-8 col-md-offset-3">
    <div class="form-check">
        <label class="form-check-label">
            <input class="form-check-input" type="checkbox"> &nbsp; &nbsp; &nbsp; I accept the terms and conditions
        </label>
    </div>
    </div>
</div>

