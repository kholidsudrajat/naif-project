@extends('layouts.app')
@section('content')
<div class="app-title">
  <div>
      <h1><i class="fa fa-dashboard"></i> {{ $title }} </h1>
      {{-- <p>Fortal Cranium Website</p> --}}

  </div>

        <a href=" {{ url('/admin/roles/create') }} " hidden>
            <button class="btn btn-primary" type="submit"><i class="fa fa-fw fa-lg fa-check-circle"></i>Add Role</button>
        </a>


  {{-- <ul class="app-breadcrumb breadcrumb">
      <li class="breadcrumb-item"><i class="fa fa-home fa-lg"></i></li>
      <li class="breadcrumb-item"><a href="#">Dashboard</a></li>
  </ul> --}}
</div>
<div class="row">
    <div class="col-md-12">

      <div class="tile">

        <div class="tile-body">

          <table class="table table-hover table-bordered" id="sampleTable">

            <thead>
                <tr>
                    <th>No</th>
                    <th>Name</th>
                    <th width="11%">Action</th>
                </tr>
            </thead>
            <tbody>
            <?php $no=1;?>
            @foreach ($roles as $role_view)
                <tr>
                    <td> {{ $no++ }}</td>
                    <td> {{ $role_view->name }} </td>
                    <td>

                        {{-- @can('role-edit') --}}
                            {{-- <a class="btn btn-primary" href="{{ route('roles.edit',$role->id) }}">Edit</a> --}}
                            <a href=" {{ url('/admin/roles/'.$role_view->id.'/edit') }} ">
                                <button class="btn btn-sm btn-info" type="submit"><i class="fa fa-pencil fa-fw"></i>Edit</button>
                            </a>
                        {{-- @endcan --}}
                        @can('role-delete')
                            {!! Form::open(['method' => 'DELETE','route' => ['admin.roles.destroy', $role_view->id],'style'=>'display:inline']) !!}
                                {{-- {!! Form::submit('Delete', ['class' => 'btn btn-danger']) !!} --}}
                                <a href="#" onclick="deleteData({{ $role_view->id}})">
                                    <button class="btn btn-sm btn-danger" type="submit"><i class="fa fa-times"></i>Delete</button>
                                </a>
                            {!! Form::close() !!}
                        @endcan

                    </td>
                </tr>
            @endforeach

            </tbody>
          </table>
          {!! $roles->render() !!}
        </div>
      </div>
    </div>
  </div>
@endsection

@push('script')
    <script>
        function deleteData(id)
        {
            swal({
        title: "Are you sure?",
        text: "You will not be able to recover this imaginary file!",
        type: "warning",
        showCancelButton: true,
        confirmButtonText: "Yes, delete it!",
        cancelButtonText: "No, cancel plx!",
        closeOnConfirm: false,
        closeOnCancel: false
      	}, function(isConfirm) {
      		if (isConfirm)
            {
                $.ajax({
                    type: "POST",
                    url: '{{ url('/admin/user-access/') }}/' + id + '?' + $.param({"_token" : '{{ csrf_token() }}' }),
                    data: {
                        _method: 'delete'
                    },
                    complete: function (msg)
                    {
                        if(msg.status == 200)
                        {
                            window.location.reload();
                            swal("Deleted!", "Your imaginary file has been deleted.", "success");
                        }
                    }
                });

      		} else {
      			swal("Cancelled", "Your imaginary file is safe :)", "error");
      		}
        });
        }
    </script>
@endpush



