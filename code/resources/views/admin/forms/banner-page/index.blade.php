@extends('layouts.app')
@section('content')
<div class="app-title">
  <div>
      <h1><i class="fa fa-dashboard"></i> {{ $title }} </h1>
      {{-- <p>Fortal Cranium Website</p> --}}

  </div>
  <a href=" {{ url('/admin/banner/create') }} "> <button class="btn btn-primary" type="submit"><i class="fa fa-fw fa-lg fa-check-circle"></i>Add Banner</button></a>

</div>
<div class="row">
    <div class="col-md-12">

      <div class="tile">

        <div class="tile-body">

          <table class="table table-hover table-bordered" id="sampleTable">

            <thead>
                <tr>
                    <th>No</th>
                    <th>Banner Category</th>
                    <th>Sup Title</th>
                    <th>Title</th>
                    <th>Description</th>
                    <th width="250px">Image</th>
                    <th>Url Page</th>
                    <th width="125px">Action</th>
                </tr>
            </thead>
            <tbody>
            <?php $no=1;?>
            @foreach ($banner as $banner_view )
                <tr>
                    <td> {{ $no++ }}</td>
                    <td>

                        @if ($banner_view->banner_category == 1)
                            Homepage
                        @endif
                        @if ($banner_view->banner_category == 2)
                            About Us
                        @endif
                        @if ($banner_view->banner_category == 3)
                            What Do Our Logo Mean
                        @endif
                        @if ($banner_view->banner_category == 4)
                            Blogs
                        @endif
                        @if ($banner_view->banner_category == 5)
                            FAQ
                        @endif
                        @if ($banner_view->banner_category == 6)
                            Ingredients
                        @endif
                        @if ($banner_view->banner_category == 8)
                            Friends Club
                        @endif
                        @if ($banner_view->banner_category == 9)
                            Contact Us
                        @endif

                    </td>
                    <td> {{ $banner_view->sup_title }} </td>
                    <td> {{ $banner_view->title }} </td>
                    <td> {!! html_entity_decode($banner_view->description) !!} </td>
                    <td> <img src="{{ url('upload/banner/'.$banner_view->image) }}" class="img-fluid service-banner "></td>
                    <td> {{ $banner_view->url_page }} </td>


                    <td>
                        <a href=" {{ url('/admin/banner/'.$banner_view->id.'/edit') }} ">
                            <button class="btn btn-sm btn-info" type="submit"><i class="fa fa-pencil fa-fw"></i>Edit</button>
                        </a>

                        <a href="#" onclick="deleteData({{ $banner_view->id}})">
                            <button class="btn btn-sm btn-danger" type="submit"><i class="fa fa-times"></i>Delete</button>
                        </a>

                    </td>
                </tr>
            @endforeach

            </tbody>
          </table>
        </div>
      </div>
    </div>
  </div>
@endsection

@push('script')
    <script>
         function deleteData(id)
        {
            swal({
                title: "Are you sure?",
                text: "You will not be able to recover this imaginary file!",
                type: "warning",
                showCancelButton: true,
                confirmButtonText: "Yes, delete it!",
                cancelButtonText: "No, cancel plx!",
                closeOnConfirm: false,
                closeOnCancel: false
      	}, function(isConfirm) {
      		if (isConfirm)
                {
                    $.ajax({
                        type: "POST",
                        url: '{{ url('/admin/banner/') }}/' + id + '?' + $.param({"_token" : '{{ csrf_token() }}' }),
                        data: {
                            _method: 'delete'
                        },
                        complete: function (msg)
                        {
                            if(msg.status == 200)
                            {
                                window.location.reload();
                                swal("Deleted!", "Your imaginary file has been deleted.", "success");
                            }
                        }
                    });

                } else {
                    swal("Cancelled", "Your imaginary file is safe :)", "error");
                }
            });
        }
    </script>
@endpush



