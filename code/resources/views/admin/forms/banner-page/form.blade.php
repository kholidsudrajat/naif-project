
<div class="form-group row">
    <label class="control-label col-md-3">Banner Category</label>
    <div class="col-md-8">
        {!! Form::select('banner_category',
            [
                '1' => 'Homepage',
                '2' => 'About Us',
                '3' => 'What Do Our Logo Mean',
                '4' => 'Blogs',
                '5' => 'FAQ',
                '6' => "Ingredients",
                '8' => "Friends Club",
                '9' => 'Contact Us',
            ], null,
            ['class' => 'form-control', 'id'=>'banner_category', 'placeholder' => 'Enter title', 'required'=>'required']) !!}

    </div>
</div>
<div class="form-group row">
    <label class="control-label col-md-3">Sup Title</label>
    <div class="col-md-8">
        {!! Form::text('sup_title', null,['class' => 'form-control', 'placeholder' => 'Enter sup title']) !!}

    </div>
</div>
<div class="form-group row">
    <label class="control-label col-md-3">Title</label>
    <div class="col-md-8">
        {!! Form::text('title', null,['class' => 'form-control', 'placeholder' => 'Enter title']) !!}

    </div>
</div>
<div class="form-group row">
    <label class="control-label col-md-3">Description</label>
    <div class="col-md-8">
        {!! Form::textarea('description', null,['class' => 'form-control tinymce', 'placeholder' => 'Enter description', 'id'=>'top-form', 'rows'=>'10', 'required'=>'required']) !!}
    </div>
</div>
<div class="form-group row">
    <label class="control-label col-md-3">Image</label>
    <div class="col-md-8">
    {!! Form::file('images', null,['class' => 'form-control', 'placeholder' => 'Enter url image', 'required'=>'required']) !!}
    <br><br>
    @if (isset($banner))
        <img src="{{ url('upload/banner/'.$banner->image) }}" class="img-fluid service-icon " width="150px">
    @endif

    </div>
</div>
<div class="form-group row" id="url_page">
    <label class="control-label col-md-3">Url Page</label>
    <div class="col-md-8">
    {!! Form::text('url_page', null,['class' => 'form-control', 'placeholder' => 'Enter url page']) !!}

    </div>
</div>
<div class="form-group row">
    <label class="control-label col-md-3">Meta Tags</label>
    <div class="col-md-8">
        {!! Form::textarea('meta_tags', null,['class' => 'form-control', 'placeholder' => 'Enter meta tags', 'rows'=>'5', 'required'=>'required']) !!}
    </div>
</div>
<div class="form-group row" hidden>
    <div class="col-md-8 col-md-offset-3">
    <div class="form-check">
        <label class="form-check-label">
            <input class="form-check-input" type="checkbox"> &nbsp; &nbsp; &nbsp; I accept the terms and conditions
        </label>
    </div>
    </div>
</div>

<script>

    $(document).ready(function(){
        var banner = @json($banner);
        if (banner){
            if (banner.banner_category != "1"){
                $("#url_page").hide();
            }
        }
        $("#banner_category").on("change", function(){
            var banner_type = $(this).val();
                if (banner_type==1) {
                    $("#url_page").show();
                }
                else {
                    $("#url_page").hide();
            }
        });
    });

</script>