@extends('layouts.app')
@section('content')
<div class="app-title">
    <div>
        <h1><i class="fa fa-dashboard"></i> {{ $title }} </h1>
        <p>Naif Content Management System</p>
    </div>

    <ul class="app-breadcrumb breadcrumb">
        <li class="breadcrumb-item"><i class="fa fa-home fa-lg"></i></li>
        <li class="breadcrumb-item"><a href="#">Dashboard</a></li>
    </ul>
</div>
<div class="row justify-content-md-center">
    <div class="col-md-8">
        <div class="tile">
            <h3 class="tile-title"> {{ $title }} </h3>
            <div class="tile-body">
                {!! Form::open(['url' => '/admin/user-access', 'class' => 'form-horizontal', 'files' => true ]) !!}

                <div class="form-group row">
                    <label class="control-label col-md-3">Name</label>
                    <div class="col-md-8">
                        <input id="name" type="text" class="form-control @error('name') is-invalid @enderror" name="name" value="{{ old('name') }}" required autocomplete="name" autofocus placeholder="Enter name user">

                        @error('name')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror

                    </div>
                </div>
                <div class="form-group row">
                    <label class="control-label col-md-3">E-Mail Address</label>
                    <div class="col-md-8">
                        <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email" placeholder="Enter email address">

                        @error('email')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror

                    </div>
                </div>

                <div class="form-group row">
                    <label class="control-label col-md-3">Password</label>
                    <div class="col-md-8">
                        <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="new-password" placeholder="Enter password">

                        @error('password')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>
                </div>
                <div class="form-group row">
                    <label class="control-label col-md-3">Confirm Password</label>
                    <div class="col-md-8">
                        <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required autocomplete="new-password" placeholder="Enter password confirm">
                    </div>
                </div>
                <div class="form-group row">
                    <label class="control-label col-md-3">User Role</label>
                    <div class="col-md-8">
                        <select name="roles" id="" class="form-control" required="required">
                            <option value=""> Select role</option>
                            @foreach ($roles as $roles_view)
                                <option value="{{$roles_view->id}}">{{$roles_view->name}}</option>
                            @endforeach

                        </select>
                        {{-- {!! Form::select('roles', $roles, null, ['class' => 'form-control']); !!} --}}

                    </div>
                </div>




                <div class="form-group row">
                    <div class="col-md-8 col-md-offset-3">
                    <div class="form-check">
                        <label class="form-check-label">
                            <input class="form-check-input" type="checkbox"> &nbsp; &nbsp; &nbsp; I accept the terms and conditions
                        </label>
                    </div>
                    </div>
                </div>


                <div class="tile-footer">
                        <div class="row">
                        <div class="col-md-8 col-md-offset-3">
                            <button class="btn btn-primary" type="submit"><i class="fa fa-fw fa-lg fa-check-circle"></i>Save</button>
                            <a class="btn btn-secondary" href=" {{ url('/admin/user-access') }} "><i class="fa fa-fw fa-lg fa-times-circle"></i>Cancel</a>
                        </div>
                        </div>
                    </div>

                {!! Form::close() !!}
            </div>

        </div>
    </div>
</div>
@endsection

