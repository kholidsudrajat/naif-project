
<div class="form-group row">
    <label class="control-label col-md-3">Name</label>
    <div class="col-md-8">
        {!! Form::text('name', null,['class' => 'form-control', 'placeholder' => 'Enter name user', 'required'=>'required']) !!}
        {{-- <input id="name" type="text" class="form-control @error('name') is-invalid @enderror" name="name" value="{{ old('name') }}" required autocomplete="name" autofocus placeholder="Enter name user"> --}}

        @error('name')
            <span class="invalid-feedback" role="alert">
                <strong>{{ $message }}</strong>
            </span>
        @enderror

    </div>
</div>
<div class="form-group row">
    <label class="control-label col-md-3">E-Mail Address</label>
    <div class="col-md-8">
        {!! Form::text('email', null,['class' => 'form-control', 'placeholder' => 'Enter email address', 'required'=>'required', 'readonly']) !!}
        @error('email')
            <span class="invalid-feedback" role="alert">
                <strong>{{ $message }}</strong>
            </span>
        @enderror

    </div>
</div>

<div class="form-group row">
    <label class="control-label col-md-3">Password</label>
    <div class="col-md-8">
        <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="new-password" placeholder="Enter password">

        @error('password')
            <span class="invalid-feedback" role="alert">
                <strong>{{ $message }}</strong>
            </span>
        @enderror
    </div>
</div>
<div class="form-group row">
    <label class="control-label col-md-3">Confirm Password</label>
    <div class="col-md-8">
        <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required autocomplete="new-password" placeholder="Enter password confirm">
    </div>
</div>
<div class="form-group row">
    <label class="control-label col-md-3">User Role</label>
    <div class="col-md-8">
        <select name="roles" id="" class="form-control" required="required">
                <option value=""> Select role</option>
            @foreach ($roles as $roles_view)
                <option value="{{$roles_view->id}}">{{$roles_view->name}}</option>
            @endforeach

        </select>

    </div>
</div>
<div class="form-group row" hidden>
    <div class="col-md-8 col-md-offset-3">
    <div class="form-check">
        <label class="form-check-label">
            <input class="form-check-input" type="checkbox"> &nbsp; &nbsp; &nbsp; I accept the terms and conditions
        </label>
    </div>
    </div>
</div>

