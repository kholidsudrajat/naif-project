<table class="table table-hover table-bordered" id="sampleTable">
    <thead>
        <tr>
            <th>No</th>
            <th>Nama Depan</th>
            <th>Nama Belakang</th>
            <th>Email</th>
            <th>No Telp</th>
            <th>Alamat</th>
            <th>Subjek</th>
            <th>Deskripsi</th>
            <th width="7%">Tanggal</th>
        </tr>
    </thead>
    <tbody>
        @foreach ($contact_us as $no => $contact_us_view)
            <tr>
                <td> {{ $no+1 }}</td>
                <td> {{ $contact_us_view->name }} </td>
                <td> {{ $contact_us_view->email }} </td>
                <td> {{ $contact_us_view->no_telp }} </td>
                <td> {{ $contact_us_view->address }} </td>
                <td> {{ $contact_us_view->subject }} </td>
                <td> {{ $contact_us_view->description }} </td>
                <td> {{ date('d-m-Y', strtotime($contact_us_view->created_at))}} </td>
            </tr>
        @endforeach
    </tbody>
</table>
