@extends('layouts.app')
@section('content')



<div class="app-title">
  <div>
      <h1><i class="fa fa-dashboard"></i> {{ $title }} </h1>
  </div>
</div>
<div class="row">
    <div class="col-md-12">

      <div class="tile">

        <div class="tile-body">
            <table>
                <tr>
                    <td>
                        <div class="form-group">
                            <label>Select start date</label>
                            <input type="date" name="startDate" id="startDate" class="form-control">
                        </div>
                    </td>
                    <td>
                        <div class="form-group">
                            <label>Select end date</label>
                            <input type="date" name="endDate" id="endDate" class="form-control">
                        </div>
                    </td>
                    <td>
                        <div class="form-group">
                            <label style="visibility: hidden">a</label>
                            <button type="button" class="btn btn-primary form-control" id="btnShow">Show</button>
                        </div>
                    </td>
                    <td>
                        <div class="form-group">
                            <label style="visibility: hidden">a</label>
                            <a href="{{url('/admin/contact-us/export')}}" id="export-btn" class="btn btn-success form-control">EXPORT EXCEL</a>
                        </div>
                    </td>
                </tr>
            </table>


            {{-- <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModalCenter">Filter</button> --}}
              <!-- Modal -->
                <div class="modal fade" id="exampleModalCenter" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
                    <div class="modal-dialog modal-dialog-centered" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLongTitle">Filter Data</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                        </div>
                        <div class="modal-body">
                            <div class="form-group">
                                <label for="formGroupExampleInput2">Select Date</label>
                                <input type="text" class="form-control" id="formGroupExampleInput2" placeholder="Another input">
                              </div>
                              <div class="form-group">
                                <label for="formGroupExampleInput2">Sorted By</label>
                                <select id="inputState" class="form-control">
                                    <option selected>Choose...</option>
                                    <option>Address</option>
                                  </select>
                              </div>
                        </div>
                        <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                        <button type="button" class="btn btn-primary">Apply</button>
                        </div>
                    </div>
                    </div>
                </div>
                <table class="table table-hover table-bordered" id="sampleTable">
                    <thead>
                        <tr>
                            <th>No</th>
                            <th>Nama Depan</th>
                            <th>Nama Belakang</th>
                            <th>Email</th>
                            <th>No Telp</th>
                            <th>Alamat</th>
                            <th>Subjek</th>
                            <th width="30%">Deskripsi</th>
                            <th width="10%">Tanggal</th>
                        </tr>
                    </thead>
                    <tbody class="load-contact"></tbody>
                </table>
            </div>
        </div>
    </div>
  </div>
@endsection

@push('script')
    <script>
        $(document).ready(function(){

            get_data()

            $('#btnShow').click(function(){
                var start_date = document.getElementById("startDate").value;
                var end_date = document.getElementById("endDate").value;
                if(start_date == "")
                {
                $("#export-btn").attr("href", "{{url('/admin/contact-us/export')}}");
                }else
                {
                    // console.log(start_date);
                $("#export-btn").attr("href", "{{url('/admin/contact-us/export')}}/?start_date="+start_date+"&end_date="+end_date);
                }

                $.ajax({
                    url : "{{ url('admin/contact-us/load_data') }}",
                    method : "POST",
                    data:{
                        start_date:start_date,
                        end_date:end_date,
                        _token:"{{ csrf_token() }}"
                    },
                    success: function(data)
                    {
                        if(data.length > 0) {
                            // console.log(data);
                            $('.load-contact').html(data);
                        } else {
                            // console.log('Nothing in the DB with parameters');
                            $('.load-contact').html(data);
                        }
                    }
                });


            });

            function get_data()
            {
                var start_date = document.getElementById("startDate").value;
                var end_date = document.getElementById("endDate").value;
                $.ajax({
                    url : "{{ url('admin/contact-us/load_data') }}",
                    method : "POST",
                    data:{
                        start_date:start_date,
                        end_date:end_date,
                        _token:"{{ csrf_token() }}"
                    },
                    success: function(data)
                    {
                        if(data.length > 0) {
                            // console.log(data);
                            $('.load-contact').html(data);
                        } else {
                            // console.log('Nothing in the DB');
                            $('.load-contact').html(data);
                        }
                    }
                });
            }

        });



        function updateData(id)
        {
            swal({
                title: "Are you sure?",
                text: "You will not be able to recover this imaginary file!",
                type: "warning",
                showCancelButton: true,
                confirmButtonText: "Yes, confirm it!",
                cancelButtonText: "No, cancel plx!",
                closeOnConfirm: false,
                closeOnCancel: false
            },

            function(isConfirm) {
                if (isConfirm)
                    {
                        //url musti custom, jangan samain dengan yang di resource
                        $.ajax({
                            type: "GET",
                            url: '{{ url('/admin/contact-confirm/') }}/confirm/' + id,
                            complete: function (msg)
                            {
                                if(msg.status == 200)
                                {
                                    window.location.reload();
                                    swal("Updated!", "Your imaginary file has been update.", "success");
                                }
                            }
                        });

                    } else {
                        swal("Cancelled", "Your imaginary file is safe :)", "error");
                }
            });
        }
        // ===========================================
        function deleteData(id)
        {
            swal({
                title: "Are you sure?",
                text: "You will not be able to recover this imaginary file!",
                type: "warning",
                showCancelButton: true,
                confirmButtonText: "Yes, delete it!",
                cancelButtonText: "No, cancel plx!",
                closeOnConfirm: false,
                closeOnCancel: false
      	}, function(isConfirm) {
      		if (isConfirm)
                {
                    $.ajax({
                        type: "POST",
                        url: '{{ url('/admin/contact-us') }}/' + id + '?' + $.param({"_token" : '{{ csrf_token() }}' }),
                        data: {
                            _method: 'delete'
                        },
                        complete: function (msg)
                        {
                            if(msg.status == 200)
                            {
                                window.location.reload();
                                swal("Deleted!", "Your imaginary file has been deleted.", "success");
                            }
                        }
                    });

                } else {
                    swal("Cancelled", "Your imaginary file is safe :)", "error");
                }
            });
        }
    </script>
@endpush



