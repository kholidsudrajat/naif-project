@extends('layouts.app')
@section('content')



<div class="app-title">
  <div>
      <h1><i class="fa fa-dashboard"></i> {{ $title }} </h1>
  </div>
</div>
<div class="row">
    <div class="col-md-12">

      <div class="tile">

        <div class="tile-body">
            <table>
                <tr>
                    <td>
                        <div class="form-group">
                            <label>Start date</label>
                            <!-- <input type="text" name="datefilter" class="form-control" class="datefilter" value="" /> -->
                            <input type="date" name="start_date" id="start_date" class="form-control datepicker-autoclose" placeholder="Please select start date">
                        </div>
                    </td>
                    <td>
                        <div class="form-group">
                            <label>End date</label><input type="date" name="end_date" id="end_date" class="form-control datepicker-autoclose" placeholder="Please select end date">
                        </div>
                    </td>
                    <td>
                        <div class="form-group">
                            <label style="visibility: hidden">a</label>
                            <button type="text" id="btnFiterSubmitSearch" class="btn btn-info form-control">Show</button>
                        </div>
                    </td>
                    <td>
                        <div class="form-group">
                            <label style="visibility: hidden">a</label>
                            <a href="{{url('/admin/contact-us/export')}}" id="export-btn" class="btn btn-success form-control">EXPORT EXCEL</a>
                        </div>
                    </td>
                </tr>
            </table>



            <!-- <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModalCenter">Filter</button> -->
              <!-- Modal -->
                <div class="modal fade" id="exampleModalCenter" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
                    <div class="modal-dialog modal-dialog-centered" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLongTitle">Filter Data</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                        </div>
                        <div class="modal-body">
                            <div class="form-group">
                                <label for="formGroupExampleInput2">Select Date</label>
                                <input type="text" class="form-control" id="formGroupExampleInput2" placeholder="Another input">
                              </div>
                              <div class="form-group">
                                <label for="formGroupExampleInput2">Sorted By</label>
                                <select id="inputState" class="form-control">
                                    <option selected>Choose...</option>
                                    <option>Address</option>
                                  </select>
                              </div>
                        </div>
                        <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                        <button type="button" class="btn btn-primary">Apply</button>
                        </div>
                    </div>
                    </div>
                </div>

                <table  class="table table-hover table-bordered table-striped data-table">
                    <thead>
                        <tr>
                            <th>No</th>
                            <th>Nama</th>
                            <th>Email</th>
                            <th>No Telp</th>
                            <th>Alamat</th>
                            <th>Subjek</th>
                            <th width="30%">Deskripsi</th>
                            <th width="10%">Tanggal</th>
                        </tr>
                    </thead>
                </table>

            </div>
        </div>
    </div>
  </div>
@endsection

@push('script')
    <script>

        $(document).ready(function() {
            $(function() {

                $('input[name="datefilter"]').daterangepicker({
                    autoUpdateInput: false,
                    locale: {
                        cancelLabel: 'Clear'
                    }
                });
                $('input[name="datefilter"]').on('apply.daterangepicker', function(ev, picker) {
                    $(this).val(picker.startDate.format('MM/DD/YYYY') + ' - ' + picker.endDate.format('MM/DD/YYYY'));
                    // console.log("A new date selection was made: " + picker.startDate.format('YYYY-MM-DD') + ' to ' + picker.endDate.format('YYYY-MM-DD'));

                    if(picker.startDate == "")
                    {
                        $("#export-btn").attr("href", "{{url('/admin/contact-us/export')}}");
                    }else
                    {
                        $("#export-btn").attr("href", "{{url('/admin/contact-us/export')}}/?start_date="+picker.startDate.format('YYYY-MM-DD')+"&end_date="+picker.endDate.format('YYYY-MM-DD'));
                    }

                    $('input[name="datefilter"]').on('cancel.daterangepicker', function(ev, picker) {
                        $(this).val('');
                    });
                });
            });

            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            $('.data-table').DataTable({
                processing: true,
                serverSide: true,
                ajax: {
                    url: "{{ url('admin/contact-us') }}",
                    type: 'GET',
                    data: function (d) {
                        d.start_date = $('#start_date').val();
                        d.end_date = $('#end_date').val();
                    }
                },
                columns: [
                        {
                            data: "id", 
                            render: function (data, type, row, meta) {
                                return meta.row + meta.settings._iDisplayStart + 1;
                            }
                        },
                        { data: 'name',   name: 'name' },
                        { data: 'email',        name: 'email' },
                        { data: 'no_telp',      name: 'no_telp' },
                        { data: 'address',      name: 'address	' },
                        { data: 'subject',      name: 'subject' },
                        { data: 'description',  name: 'description' },
                        { data: 'created_at', render:function(data){
                            return moment(data).format('DD-MM-YYYY');
                            }
                        },
                    ],
                order: [[0, 'desc']]
            });


        });

        $('#btnFiterSubmitSearch').click(function(){
            var start_date = $('#start_date').val()+ ' 00:00:00';
            var end_date = $('#end_date').val()+ ' 23:59:59';

            // console.log(start_date, "result start date")
            // console.log(end_date, "result end date")
            $('.data-table').DataTable().draw(true);
            if(start_date == "")
            {
                $("#export-btn").attr("href", "{{url('/admin/contact-us/export')}}");
            }else
            {
                $("#export-btn").attr("href", "{{url('/admin/contact-us/export')}}/?start_date="+start_date+"&end_date="+end_date);
            }
        });




    </script>
@endpush



