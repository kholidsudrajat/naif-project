@extends('layouts.app')
@section('content')
<div class="app-title">
    <div>
        <h1><i class="fa fa-dashboard"></i> {{ $title }} </h1>
        <p>Naif Content Management System</p>
    </div>

    <ul class="app-breadcrumb breadcrumb">
        <li class="breadcrumb-item"><i class="fa fa-home fa-lg"></i></li>
        <li class="breadcrumb-item"><a href="#">Dashboard</a></li>
    </ul>
</div>
<div class="row justify-content-md-center">

    <div class="col-md-4">
        <div class="tile">
            <h3 class="tile-title"> {{ $title }} </h3>
            <div class="tile-body">
                {!! Form::open([
                    'url' => '/admin/comfort/store-detail/'.$id,
                    'class' => 'form-horizontal',
                    'files' => true ])
                !!}


                <div class="form-group row">
                    <label class="control-label col-md-3">Image Category</label>
                    <div class="col-md-8">

                    {!! Form::select('image_thumbnail', ['0' => 'Image Variant', '1' => 'Image Main'] ,null,['class' => 'form-control', 'required'=>'required']) !!}

                    </div>
                </div>

                <div class="form-group row">
                    <label class="control-label col-md-3">Image</label>
                    <div class="col-md-8">
                    {!! Form::file('images', null,['class' => 'form-control', 'placeholder' => 'Enter url image', 'required'=>'required']) !!}


                    </div>
                </div>
                <div class="form-group row" hidden>
                    <div class="col-md-8 col-md-offset-3">
                    <div class="form-check">
                        <label class="form-check-label">
                            <input class="form-check-input" type="checkbox"> &nbsp; &nbsp; &nbsp; I accept the terms and conditions
                        </label>
                    </div>
                    </div>
                </div>

                <div class="tile-footer">
                        <div class="row">
                        <div class="col-md-8 col-md-offset-3">
                            <button class="btn btn-primary" type="submit"><i class="fa fa-fw fa-lg fa-check-circle"></i>Save</button>
                            <a class="btn btn-secondary" href=" {{ url('/admin/comfort-product') }} "><i class="fa fa-fw fa-lg fa-times-circle"></i>Back</a>
                        </div>
                        </div>
                    </div>

                {!! Form::close() !!}
            </div>

        </div>
    </div>
    <div class="col-md-4 ">
        <div class="tile">
            <div class="tile-body">

                <table class="table table-hover table-bordered">

                  <thead>
                      <tr>
                          <th>No</th>
                          <th>Image Category</th>
                          <th width="100px">Image</th>
                          <th width="11%">Action</th>
                      </tr>
                  </thead>
                  <tbody>
                  <?php $no=1;?>
                  @foreach ($product_detail as $product_detail_view )
                      <tr>
                            <td> {{ $no++ }}</td>
                            <td>
                                @if ($product_detail_view->image_thumbnail == 1)
                                    Image Main
                                @else
                                    Image Variant
                                @endif
                            </td>
                            <td> <img src="{{ url('upload/products/'.$product_detail_view->image) }}" class="img-fluid service-banner "></td>
                            <td>
                                <a href="#" onclick="deleteData({{ $product_detail_view->id}})">
                                    <button class="btn btn-sm btn-danger" type="submit"><i class="fa fa-times"></i>Delete</button>
                                </a>

                            </td>
                      </tr>
                  @endforeach

                  </tbody>
                </table>
              </div>

        </div>
    </div>
</div>
@endsection
@push('script')
    <script>
         function deleteData(id)
        {
            swal({
                title: "Are you sure?",
                text: "You will not be able to recover this imaginary file!",
                type: "warning",
                showCancelButton: true,
                confirmButtonText: "Yes, delete it!",
                cancelButtonText: "No, cancel plx!",
                closeOnConfirm: false,
                closeOnCancel: false
      	}, function(isConfirm) {
      		if (isConfirm)
                {
                    $.ajax({
                        type: "POST",
                        url: '{{ url('/admin/comfort/destroy-detail/') }}/' + id + '?' + $.param({"_token" : '{{ csrf_token() }}' }),

                        complete: function (msg)
                        {
                            if(msg.status == 200)
                            {
                                swal("Deleted!", "Your imaginary file has been deleted.", "success");
                            }

                            window.location.reload();
                        }
                    });

                } else {
                    swal("Cancelled", "Your imaginary file is safe :)", "error");
                }
            });
        }
    </script>
@endpush

