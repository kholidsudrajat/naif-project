@extends('layouts.app')
@section('content')
<div class="app-title">
    <div>
        <h1><i class="fa fa-dashboard"></i> {{ $title }} </h1>
        <p>Naif Content Management System</p>
    </div>

    <ul class="app-breadcrumb breadcrumb">
        <li class="breadcrumb-item"><i class="fa fa-home fa-lg"></i></li>
        <li class="breadcrumb-item"><a href="#">Dashboard</a></li>
    </ul>
</div>
<div class="row justify-content-md-center">

    <div class="col-md-6">
        <div class="tile">
            <h3 class="tile-title"> {{ $title }} </h3>
            <div class="tile-body">
                {!! Form::open([
                    'url' => '/admin/comfort/store-faq/'.$id,
                    'class' => 'form-horizontal',
                    'files' => true ])
                !!}


                <div class="form-group row">
                    <label class="control-label col-md-3">Question</label>
                    <div class="col-md-8">
                        {!! Form::text('question', null,['class' => 'form-control', 'placeholder' => 'Enter question', 'required'=>'required']) !!}

                    </div>
                </div>
                <div class="form-group row">
                    <label class="control-label col-md-3"> Asnwer</label>
                    <div class="col-md-8">
                        {!! Form::textarea('answer', null,['class' => 'form-control tinymce', 'placeholder' => 'Enter anwer', 'id'=>'top-form', 'rows'=>'10', 'required'=>'required']) !!}
                    </div>
                </div>
                <div class="form-group row" hidden>
                    <div class="col-md-8 col-md-offset-3">
                    <div class="form-check">
                        <label class="form-check-label">
                            <input class="form-check-input" type="checkbox"> &nbsp; &nbsp; &nbsp; I accept the terms and conditions
                        </label>
                    </div>
                    </div>
                </div>

                <div class="tile-footer">
                        <div class="row">
                        <div class="col-md-8 col-md-offset-3">
                            <button class="btn btn-primary" type="submit"><i class="fa fa-fw fa-lg fa-check-circle"></i>Save</button>
                            <a class="btn btn-secondary" href=" {{ url('/admin/comfort-product') }} "><i class="fa fa-fw fa-lg fa-times-circle"></i>Back</a>
                        </div>
                        </div>
                    </div>

                {!! Form::close() !!}
            </div>

        </div>
    </div>
    <div class="col-md-5">
        <div class="tile">
            <div class="tile-body">

                <table class="table table-hover table-bordered">

                  <thead>
                      <tr>
                          <th width="10px" >No</th>
                          <th width="30px">Question</th>
                          <th>Answer</th>
                          <th width="11%">Action</th>
                      </tr>
                  </thead>
                  <tbody>
                  <?php $no=1;?>
                  @foreach ($faq_product as $faq_product_view )
                      <tr>
                          <td> {{ $no++ }}</td>
                          <td> {{ $faq_product_view->question }} </td>
                          <td> {!! html_entity_decode($faq_product_view->answer) !!} </td>
                          <td>
                              <a href="#" onclick="deleteData({{ $faq_product_view->id}})">
                                  <button class="btn btn-sm btn-danger" type="submit"><i class="fa fa-times"></i>Delete</button>
                              </a>

                          </td>
                      </tr>
                  @endforeach

                  </tbody>
                </table>
              </div>

        </div>
    </div>
</div>
@endsection
@push('script')
<script>
    function deleteData(id)
   {
       swal({
           title: "Are you sure?",
           text: "You will not be able to recover this imaginary file!",
           type: "warning",
           showCancelButton: true,
           confirmButtonText: "Yes, delete it!",
           cancelButtonText: "No, cancel plx!",
           closeOnConfirm: false,
           closeOnCancel: false
     }, function(isConfirm) {
         if (isConfirm)
           {
               $.ajax({
                   type: "POST",
                   url: '{{ url('/admin/comfort/destroy-faq/') }}/' + id + '?' + $.param({"_token" : '{{ csrf_token() }}' }),

                   complete: function (msg)
                   {
                       if(msg.status == 200)
                       {

                           swal("Deleted!", "Your imaginary file has been deleted.", "success");
                       }
                       window.location.reload();

                   }
               });

           } else {
               swal("Cancelled", "Your imaginary file is safe :)", "error");
           }
       });
   }
</script>
@endpush

