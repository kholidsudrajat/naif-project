@extends('layouts.app')
@section('content')
<div class="app-title">
  <div>
      <h1><i class="fa fa-dashboard"></i> {{ $title }} </h1>
      {{-- <p>Fortal Cranium Website</p> --}}

  </div>
  <!-- <a href=" {{ url('/admin/friends-club-benefit/create') }} "> <button class="btn btn-primary" type="submit"><i class="fa fa-fw fa-lg fa-check-circle"></i>Add Friends Club Benefit</button></a> -->
  {{-- <ul class="app-breadcrumb breadcrumb">
      <li class="breadcrumb-item"><i class="fa fa-home fa-lg"></i></li>
      <li class="breadcrumb-item"><a href="#">Dashboard</a></li>
  </ul> --}}
</div>
<div class="row">
    <div class="col-md-12">

      <div class="tile">

        <div class="tile-body">

          <table class="table table-hover table-bordered" id="sampleTable">

            <thead>
                <tr>
                    <th>No</th>
                    <th>Title</th>
                    <th>Description</th>
                    <th>Orientation</th>
                    <th width="150px">Image</th>
                    <th width="11%">Action</th>
                </tr>
            </thead>
            <tbody>
            <?php $no=1;?>
            @foreach ($friends_club_benefit as $friends_club_benefit_view )
                <tr>
                    <td> {{ $no++ }}</td>
                    <td> {{ $friends_club_benefit_view->title }} </td>
                    <td> {!! html_entity_decode($friends_club_benefit_view->description) !!} </td>
                    <td>
                        @if ($friends_club_benefit_view->image_orientation == 'P')
                            Potrait
                        @else
                            Landscape
                        @endif
                    </td>
                    <td> <img src="{{ url('upload/friends-club-benefit/'.$friends_club_benefit_view->image) }}" class="img-fluid service-banner "></td>
                    <td>
                        <a href=" {{ url('/admin/friends-club-benefit/'.$friends_club_benefit_view->id.'/edit') }} ">
                            <button class="btn btn-sm btn-info" type="submit"><i class="fa fa-pencil fa-fw"></i>Edit</button>
                        </a>

                        <a href="#" onclick="deleteData({{ $friends_club_benefit_view->id}})" hidden>
                            <button class="btn btn-sm btn-danger" type="submit"><i class="fa fa-times"></i>Delete</button>
                        </a>

                    </td>
                </tr>
            @endforeach

            </tbody>
          </table>
        </div>
      </div>
    </div>
  </div>
@endsection

@push('script')
    <script>
         function deleteData(id)
        {
            swal({
                title: "Are you sure?",
                text: "You will not be able to recover this imaginary file!",
                type: "warning",
                showCancelButton: true,
                confirmButtonText: "Yes, delete it!",
                cancelButtonText: "No, cancel plx!",
                closeOnConfirm: false,
                closeOnCancel: false
      	}, function(isConfirm) {
      		if (isConfirm)
                {
                    $.ajax({
                        type: "POST",
                        url: '{{ url('/admin/friends-club-benefit/') }}/' + id + '?' + $.param({"_token" : '{{ csrf_token() }}' }),
                        data: {
                            _method: 'delete'
                        },
                        complete: function (msg)
                        {
                            if(msg.status == 200)
                            {
                                window.location.reload();
                                swal("Deleted!", "Your imaginary file has been deleted.", "success");
                            }
                        }
                    });

                } else {
                    swal("Cancelled", "Your imaginary file is safe :)", "error");
                }
            });
        }
    </script>
@endpush



