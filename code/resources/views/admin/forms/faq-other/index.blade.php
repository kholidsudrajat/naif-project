@extends('layouts.app')
@section('content')
<div class="app-title">
  <div>
      <h1><i class="fa fa-dashboard"></i> {{ $title }} </h1>
      {{-- <p>Fortal Cranium Website</p> --}}

  </div>
  <a href=" {{ url('/admin/faq-other/create') }} "> <button class="btn btn-primary" type="submit"><i class="fa fa-fw fa-lg fa-check-circle"></i>Add FAQ</button></a>
  {{-- <ul class="app-breadcrumb breadcrumb">
      <li class="breadcrumb-item"><i class="fa fa-home fa-lg"></i></li>
      <li class="breadcrumb-item"><a href="#">Dashboard</a></li>
  </ul> --}}
</div>
<div class="row">
    <div class="col-md-12">

      <div class="tile">

        <div class="tile-body">

          <table class="table table-hover table-bordered" id="sampleTable">

            <thead>
                <tr>
                    <th>No</th>
                    <th>Question</th>
                    <th>Answer</th>
                    <th width="11%">Action</th>
                </tr>
            </thead>
            <tbody>
            <?php $no=1;?>
            @foreach ($faq_other as $faq_other_view )
                <tr>
                    <td> {{ $no++ }}</td>
                    <td> {{ $faq_other_view->question }} </td>
                    <td> {!! html_entity_decode($faq_other_view->answer) !!} </td>
                    <td>
                        <a href=" {{ url('/admin/faq-other/'.$faq_other_view->id.'/edit') }} ">
                            <button class="btn btn-sm btn-info" type="submit"><i class="fa fa-pencil fa-fw"></i>Edit</button>
                        </a>

                        <a href="#" onclick="deleteData({{ $faq_other_view->id}})" hidden>
                            <button class="btn btn-sm btn-danger" type="submit"><i class="fa fa-times"></i>Delete</button>
                        </a>

                    </td>
                </tr>
            @endforeach

            </tbody>
          </table>
        </div>
      </div>
    </div>
  </div>
@endsection

@push('script')
    <script>
         function deleteData(id)
        {
            swal({
                title: "Are you sure?",
                text: "You will not be able to recover this imaginary file!",
                type: "warning",
                showCancelButton: true,
                confirmButtonText: "Yes, delete it!",
                cancelButtonText: "No, cancel plx!",
                closeOnConfirm: false,
                closeOnCancel: false
      	}, function(isConfirm) {
      		if (isConfirm)
                {
                    $.ajax({
                        type: "POST",
                        url: '{{ url('/admin/faq_other/') }}/' + id + '?' + $.param({"_token" : '{{ csrf_token() }}' }),
                        data: {
                            _method: 'delete'
                        },
                        complete: function (msg)
                        {
                            if(msg.status == 200)
                            {
                                window.location.reload();
                                swal("Deleted!", "Your imaginary file has been deleted.", "success");
                            }
                        }
                    });

                } else {
                    swal("Cancelled", "Your imaginary file is safe :)", "error");
                }
            });
        }
    </script>
@endpush



