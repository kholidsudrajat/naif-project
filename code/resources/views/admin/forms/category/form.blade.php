
<div class="form-group row">
    <label class="control-label col-md-3">Product Category</label>
    <div class="col-md-8">
    {!! Form::text('category', null,['class' => 'form-control', 'placeholder' => 'Enter Product Category', 'required'=>'required']) !!}
  
    </div>
</div>



<div class="form-group row">
    <div class="col-md-8 col-md-offset-3">
    <div class="form-check">
        <label class="form-check-label">
            <input class="form-check-input" type="checkbox"> &nbsp; &nbsp; &nbsp; I accept the terms and conditions
        </label>
    </div>
    </div>
</div>

