
<div class="form-group row">
    <label class="control-label col-md-3">Question</label>
    <div class="col-md-8">
        {!! Form::text('question', null,['class' => 'form-control', 'placeholder' => 'Enter question', 'required'=>'required']) !!}

    </div>
</div>
<div class="form-group row">
    <label class="control-label col-md-3">Answer</label>
    <div class="col-md-8">
        {!! Form::textarea('answer', null,['class' => 'form-control tinymce', 'placeholder' => 'Enter answer', 'id'=>'top-form', 'rows'=>'10', 'required'=>'required']) !!}
    </div>
</div>





<div class="form-group row" hidden>
    <div class="col-md-8 col-md-offset-3">
    <div class="form-check">
        <label class="form-check-label">
            <input class="form-check-input" type="checkbox"> &nbsp; &nbsp; &nbsp; I accept the terms and conditions
        </label>
    </div>
    </div>
</div>

