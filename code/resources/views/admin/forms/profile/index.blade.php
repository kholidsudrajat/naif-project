@extends('layouts.app')
@section('content')
<div class="app-title">
  <div>
      <h1><i class="fa fa-dashboard"></i> {{ $title }} </h1>
      {{-- <p>Fortal Cranium Website</p> --}}

  </div>
  <a href=" {{ url('/admin/profile/create') }} " hidden> <button class="btn btn-primary" type="submit"><i class="fa fa-fw fa-lg fa-check-circle"></i>Add Profile</button></a>
  {{-- <ul class="app-breadcrumb breadcrumb">
      <li class="breadcrumb-item"><i class="fa fa-home fa-lg"></i></li>
      <li class="breadcrumb-item"><a href="#">Dashboard</a></li>
  </ul> --}}
</div>
<div class="row">
    <div class="col-md-12">

        <div class="tile">

            <div class="tile-body">

                @foreach ($profile as $profile_view)
                    <table class="table table-hover table-bordered" id="sampleTable">
                        <tr>
                            <th width="250px">Title</th>
                            <td>:</td>
                            <td> {{ $profile_view->title }} </td>
                        </tr>
                        <tr>
                            <th>Sup Description</th>
                            <td>:</td>
                            <td> {!! html_entity_decode($profile_view->sup_description) !!} </td>
                        </tr>
                        <tr>
                            <th>Description Form</th>
                            <td>:</td>
                            <td> {!! html_entity_decode($profile_view->description) !!} </td>
                        </tr>
                        <tr>
                            <th>Image</th>
                            <td>:</td>
                            <td><img src="{{ url('upload/profile/'.$profile_view->image) }}" class="img-fluid service-banner" width="200px"> </td>
                        </tr>
                        <tr>
                            <th>Address</th>
                            <td>:</td>
                            <td> {!! html_entity_decode($profile_view->address) !!} </td>
                        </tr>
                        <tr>
                            <th>Customer Service</th>
                            <td>:</td>
                            <td> {!! html_entity_decode($profile_view->customer_service) !!} </td>
                        </tr>
                        <tr>
                            <th>CS Image</th>
                            <td>:</td>
                            <td><img src="{{ url('upload/profile/'.$profile_view->cs_image) }}" class="img-fluid service-banner" width="200px"> </td>
                        </tr>
                        <tr>
                            <th>URL Map Embed</th>
                            <td>:</td>
                            <td> {{ $profile_view->map_embed }} </td>
                        </tr>
                        <tr>
                            <th>Email</th>
                            <td>:</td>
                            <td> {{ $profile_view->email }} </td>
                        </tr>
                        <tr>
                            <th>Telephone</th>
                            <td>:</td>
                            <td> {{ $profile_view->no_telp }} </td>
                        </tr>
                        <tr>
                            <th>Official Facebook</th>
                            <td>:</td>
                            <td> {{ $profile_view->sosmed_fb }} </td>
                        </tr>
                        <tr>
                            <th>Official Instagram</th>
                            <td>:</td>
                            <td> {{ $profile_view->sosmed_ig }} </td>
                        </tr>
                        <tr>
                            <th>Official Instagram</th>
                            <td>:</td>
                            <td> {{ $profile_view->sosmed_ig_2 }} </td>
                        </tr>
                        <tr>
                            <th>Official Youtube</th>
                            <td>:</td>
                            <td> {{ $profile_view->sosmed_twitter }} </td>
                        </tr>
                        <tr>
                            <th>Meta Tags</th>
                            <td>:</td>
                            <td> {{ $profile_view->meta_tags }} </td>
                        </tr>
                        <tr>
                            <td colspan="3">
                                <a href=" {{ url('/admin/profile/'.$profile_view->id.'/edit') }} ">
                                    <button class="btn btn-sm btn-info" type="submit"><i class="fa fa-pencil fa-fw"></i>Edit Profile</button>
                                </a>
                            </td>
                        </tr>
                    </table>
                @endforeach
            </div>
        </div>
    </div>
</div>
@endsection

@push('script')
    <script>
        function deleteData(id)
        {
            swal({
        title: "Are you sure?",
        text: "You will not be able to recover this imaginary file!",
        type: "warning",
        showCancelButton: true,
        confirmButtonText: "Yes, delete it!",
        cancelButtonText: "No, cancel plx!",
        closeOnConfirm: false,
        closeOnCancel: false
      	}, function(isConfirm) {
      		if (isConfirm)
            {
                $.ajax({
                    type: "POST",
                    url: '{{ url('/admin/profile/') }}/' + id + '?' + $.param({"_token" : '{{ csrf_token() }}' }),
                    data: {
                        _method: 'delete'
                    },
                    complete: function (msg)
                    {
                        if(msg.status == 200)
                        {
                            window.location.reload();
                            swal("Deleted!", "Your imaginary file has been deleted.", "success");
                        }
                    }
                });

      		} else {
      			swal("Cancelled", "Your imaginary file is safe :)", "error");
      		}
        });
        }
    </script>
@endpush



