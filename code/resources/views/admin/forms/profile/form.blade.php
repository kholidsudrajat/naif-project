
<div class="form-group row">
    <label class="control-label col-md-3">Title</label>
    <div class="col-md-8">
        {!! Form::text('title', null,['class' => 'form-control', 'placeholder' => 'Enter title', 'required'=>'required']) !!}
    </div>
</div>
<div class="form-group row">
    <label class="control-label col-md-3">Sup Description</label>
    <div class="col-md-8">
        {!! Form::textarea('sup_description', null,['class' => 'form-control tinymce', 'placeholder' => 'Enter description', 'id'=>'form-ckeditor', 'rows'=>'10', 'required'=>'required']) !!}
    </div>
</div>
<div class="form-group row">
    <label class="control-label col-md-3"> Description</label>
    <div class="col-md-8">
        {!! Form::textarea('description', null,['class' => 'form-control tinymce', 'placeholder' => 'Enter description', 'id'=>'top-form', 'rows'=>'10', 'required'=>'required']) !!}
    </div>
</div>
<div class="form-group row">
    <label class="control-label col-md-3">Image</label>
    <div class="col-md-8">
        {!! Form::file('images', null,['class' => 'form-control', 'placeholder' => 'Enter url image', 'required'=>'required']) !!}
        <br><br>
        @if (isset($profile))
            <img src="{{ url('upload/profile/'.$profile->image) }}" class="img-fluid service-icon " width="150px">
        @endif
    </div>
</div>
<div class="form-group row">
    <label class="control-label col-md-3">Address</label>
    <div class="col-md-8">
        {!! Form::textarea('address', null,['class' => 'form-control tinymce', 'placeholder' => 'Enter description', 'id'=>'form-ckeditor1', 'rows'=>'10', 'required'=>'required']) !!}
    </div>
</div>
<div class="form-group row">
    <label class="control-label col-md-3">Customer Service</label>
    <div class="col-md-8">
        {!! Form::textarea('customer_service', null,['class' => 'form-control tinymce', 'placeholder' => 'Enter description', 'id'=>'form-ckeditor2', 'rows'=>'10', 'required'=>'required']) !!}
    </div>
</div>
<div class="form-group row">
    <label class="control-label col-md-3">CS Image</label>
    <div class="col-md-8">
        {!! Form::file('cs_images', null,['class' => 'form-control', 'placeholder' => 'Enter url CS image', 'required'=>'required']) !!}
        <br><br>
        @if (isset($profile))
            <img src="{{ url('upload/profile/'.$profile->cs_image) }}" class="img-fluid service-icon " width="150px">
        @endif
    </div>
</div>
<div class="form-group row">
    <label class="control-label col-md-3">URL Map Embed</label>
    <div class="col-md-8">
        {!! Form::text('map_embed', null,['class' => 'form-control', 'placeholder' => 'Enter title', 'required'=>'required']) !!}
    </div>
</div>
<div class="form-group row">
    <label class="control-label col-md-3">Email</label>
    <div class="col-md-8">
        {!! Form::text('email', null,['class' => 'form-control', 'placeholder' => 'Enter title', 'required'=>'required']) !!}
    </div>
</div>
<div class="form-group row">
    <label class="control-label col-md-3">No Telp</label>
    <div class="col-md-8">
        {!! Form::text('no_telp', null,['class' => 'form-control', 'placeholder' => 'Enter title', 'required'=>'required']) !!}
    </div>
</div>
<div class="form-group row">
    <label class="control-label col-md-3">Official Facebook</label>
    <div class="col-md-8">
        {!! Form::text('sosmed_fb', null,['class' => 'form-control', 'placeholder' => 'Enter link facebook official']) !!}
    </div>
</div>
<div class="form-group row">
    <label class="control-label col-md-3">Official Instagram</label>
    <div class="col-md-8">
        {!! Form::text('sosmed_ig', null,['class' => 'form-control', 'placeholder' => 'Enter link instagram official']) !!}
    </div>
</div>
<div class="form-group row">
    <label class="control-label col-md-3">Official Instagram 2</label>
    <div class="col-md-8">
        {!! Form::text('sosmed_ig_2', null,['class' => 'form-control', 'placeholder' => 'Enter link instagram official 2']) !!}
    </div>
</div>
<div class="form-group row">
    <label class="control-label col-md-3">Official Youtube</label>
    <div class="col-md-8">
        {!! Form::text('sosmed_twitter', null,['class' => 'form-control', 'placeholder' => 'Enter link youtube official']) !!}
    </div>
</div>
<div class="form-group row">
    <label class="control-label col-md-3">Meta Tags</label>
    <div class="col-md-8">
        {!! Form::textarea('meta_tags', null,['class' => 'form-control', 'placeholder' => 'Enter meta tags', 'rows'=>'5', 'required'=>'required']) !!}
    </div>
</div>
<div class="form-group row" hidden>
    <div class="col-md-8 col-md-offset-3">
    <div class="form-check">
        <label class="form-check-label">
            <input class="form-check-input" type="checkbox"> &nbsp; &nbsp; &nbsp; I accept the terms and conditions
        </label>
    </div>
    </div>
</div>

