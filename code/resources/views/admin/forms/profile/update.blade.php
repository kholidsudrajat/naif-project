@extends('layouts.app')
@section('content')
<div class="app-title">
    <div>
        <h1><i class="fa fa-dashboard"></i> {{ $title }} </h1>
        <p>Naif Content Management System</p>
    </div>
    
    <ul class="app-breadcrumb breadcrumb">
        <li class="breadcrumb-item"><i class="fa fa-home fa-lg"></i></li>
        <li class="breadcrumb-item"><a href="#">Dashboard</a></li>
    </ul>
</div>
<div class="row justify-content-md-center">

    <div class="col-md-8">
        <div class="tile">
            <h3 class="tile-title"> {{ $title }} </h3>
            <div class="tile-body">
                {!! Form::model($profile, [
                    'method' => 'PATCH',
                    'url' => ['/admin/profile', $profile->id],
                    'class' => 'form-horizontal',
                    'files' => true
                ]) !!}
                
                    @include('admin.forms.profile.form')
                    
                    <div class="tile-footer">
                        <div class="row">
                            <div class="col-md-8 col-md-offset-3">
                                <button class="btn btn-primary" type="submit"><i class="fa fa-fw fa-lg fa-check-circle"></i>Update</button>
                                <a class="btn btn-secondary" href=" {{ url('/admin/profile') }}  "><i class="fa fa-fw fa-lg fa-times-circle"></i>Cancel</a>
                            </div>
                        </div>
                    </div>
                {!! Form::close() !!}
            </div>
        </div>
    </div>
</div>

    
@endsection