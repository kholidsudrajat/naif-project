
<div class="form-group row">
    <label class="control-label col-md-3">Section</label>
    <div class="col-md-8">
        @if (isset($about_us))
            {{ $about_us->section }}
        @endif
    </div>
</div>
<div class="form-group row">
    <label class="control-label col-md-3">Title</label>
    <div class="col-md-8">
        {!! Form::text('title', null,['class' => 'form-control', 'placeholder' => 'Enter title', 'required'=>'required']) !!}
    </div>
</div>
<div class="form-group row">
    <label class="control-label col-md-3"> Description</label>
    <div class="col-md-8">
        {!! Form::textarea('description', null,['class' => 'form-control tinymce', 'placeholder' => 'Enter description', 'id'=>'top-form', 'rows'=>'10', 'required'=>'required']) !!}
    </div>
</div>
<div class="form-group row">
    <label class="control-label col-md-3">Image</label>
    <div class="col-md-8">
    {!! Form::file('images', null,['class' => 'form-control', 'placeholder' => 'Enter url image', 'required'=>'required']) !!}
    <br><br>
    @if (isset($about_us))
        <img src="{{ url('upload/about-us/'.$about_us->image) }}" class="img-fluid service-icon " width="150px">
    @endif

    </div>
</div>
<div class="form-group row">
    <label class="control-label col-md-3">Image Orientation</label>
    <div class="col-md-8">
        {!! Form::select('image_orientation', ['P' => 'Potrait', 'L' => 'Landscape'], null,['class' => 'form-control', 'placeholder' => 'Select orientation', 'required'=>'required']) !!}

    </div>
</div>





<div class="form-group row" hidden>
    <div class="col-md-8 col-md-offset-3">
    <div class="form-check">
        <label class="form-check-label">
            <input class="form-check-input" type="checkbox"> &nbsp; &nbsp; &nbsp; I accept the terms and conditions
        </label>
    </div>
    </div>
</div>

