<table class="table table-hover table-bordered" id="sampleTable">
    <thead>
        <tr>
            <th>No</th>
            <th>Nama</th>
            <th>Email</th>
            <th>Instagram</th>
            <th>Alamat</th>
            <th>Provinsi</th>
            <th>Kota/Kabupaten</th>
            <th>Kecamatan</th>
            <th>Kode Pos</th>
            <th>No Telp</th>
            <th>Tanggal</th>
        </tr>
    </thead>
    <tbody>
        @foreach ($friends_club as $no => $friends_club_view )
            <tr>
                <td> {{ $no+1 }}</td>
                <td> {{ $friends_club_view->name }} </td>
                <td> {{ $friends_club_view->email }} </td>
                <td> {{ $friends_club_view->instagram }} </td>
                <td> {{ $friends_club_view->address }} </td>
                <td> {{ $friends_club_view->provinsi }} </td>
                <td> {{ $friends_club_view->kota }} </td>
                <td> {{ $friends_club_view->kecamatan }} </td>
                <td> {{ $friends_club_view->pos_code }} </td>
                <td> {{ $friends_club_view->no_telp }} </td>
                <td> {{ date('d-m-Y', strtotime($friends_club_view->created_at))}} </td>
            </tr>
        @endforeach
    </tbody>
</table>
