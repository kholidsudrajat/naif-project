@extends('layouts.app')
@section('content')
<div class="app-title">
  <div>
      <h1><i class="fa fa-dashboard"></i> {{ $title }} </h1>
      <!-- <p>Fortal Cranium Website</p> -->

  </div>

</div>
<div class="row">
    <div class="col-md-12">

        <div class="tile">

            <div class="tile-body">
                <table>
                    <tr>
                        <td>
                            <div class="form-group">
                                <label>Start date</label>
                                <!-- <input type="text" name="datefilter" class="form-control" value="" /> -->
                                <input type="date" name="start_date" id="start_date" class="form-control datepicker-autoclose" placeholder="Please select start date">
                            </div>
                        </td>
                        <td>
                            <div class="form-group">
                                <label>End date</label><input type="date" name="end_date" id="end_date" class="form-control datepicker-autoclose" placeholder="Please select end date">
                            </div>
                        </td>
                        <td>
                            <div class="form-group">
                                <label style="visibility: hidden">a</label>
                                <button type="text" id="btnFiterSubmitSearch" class="btn btn-info form-control">Show</button>
                            </div>
                        </td>
                        <td>
                            <div class="form-group">
                                <label style="visibility: hidden">a</label>
                                <a href="{{url('/admin/friends-club/export')}}" id="export-btn" class="btn btn-success form-control">EXPORT EXCEL</a>
                            </div>
                        </td>
                    </tr>
                </table>
                <table  class="table table-hover table-bordered table-striped data-table">
                    <thead>
                        <tr>
                            <th>No</th>
                            <th>Nama</th>
                            <th>Email</th>
                            <th>Instagram</th>
                            <th>Alamat</th>
                            <th>Provinsi</th>
                            <th>Kota/Kabupaten</th>
                            <th>Kecamatan</th>
                            <th>Kode Pos</th>
                            <th>No Telp</th>
                            <th width="70px">Tanggal</th>
                        </tr>
                    </thead>
                </table>
            </div>
        </div>
    </div>
</div>
@endsection

@push('script')
    <script>
        $(document).ready(function(){
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $('.data-table').DataTable({
                processing: true,
                serverSide: true,
                ajax: {
                    url: "{{ url('admin/friends-club') }}",
                    type: 'GET',
                    data: function (d) {
                        d.start_date = $('#start_date').val();
                        d.end_date = $('#end_date').val();
                    }
                },
                columns: [
                        {
                            data: "id", 
                            render: function (data, type, row, meta) {
                                return meta.row + meta.settings._iDisplayStart + 1;
                            }
                        },
                        { data: 'name',             name: 'name' },
                        { data: 'email',            name: 'email' },
                        { data: 'instagram',        name: 'instagram' },
                        { data: 'address',          name: 'address' },
                        { data: 'provinsi',         name: 'provinsi' },
                        { data: 'kota',             name: 'kota' },
                        { data: 'kecamatan',        name: 'kecamatan' },
                        { data: 'pos_code',         name: 'pos_code' },
                        { data: 'no_telp',          name: 'no_telp' },
                        { data: 'created_at', render:function(data){
                            return moment(data).format('DD-MM-YYYY');
                            }
                        },
                    ],
                order: [[0, 'desc']]
            });

        });
        $('#btnFiterSubmitSearch').click(function(){
            var start_date = $('#start_date').val()+ ' 00:00:00';
            var end_date = $('#end_date').val()+ ' 23:59:59';
            $('.data-table').DataTable().draw(true);
            if(start_date == "")
            {
                $("#export-btn").attr("href", "{{url('/admin//friends-club/export')}}");
            }else
            {
                $("#export-btn").attr("href", "{{url('/admin//friends-club/export')}}/?start_date="+start_date+"&end_date="+end_date);
            }
        });

    </script>
@endpush



