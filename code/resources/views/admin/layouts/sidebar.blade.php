<div class="app-sidebar__overlay" data-toggle="sidebar"></div>
<aside class="app-sidebar">
  <div class="app-sidebar__user">
    <img class="app-sidebar__user-avatar" src=" {{ url('assets/admin/images/profile.jpg') }} " alt="User Image">

    <div>
      <!-- <p class="app-sidebar__user-name"> {{ Auth::user()->name }} <span class="caret"></span></p> -->
      @if (session('status'))
        <div class="alert alert-success">
          {{ session('status') }}
        </div>
      @endif
      <p class="app-sidebar__user-designation">Administrator</p>
    </div>
  </div>
  <ul class="app-menu">
    <li><a class="app-menu__item active" href=" {{ url('admin/home') }} "><i class="app-menu__icon fa fa-dashboard"></i><span class="app-menu__label">Dashboard</span></a></li>

    <li class="treeview">
      <a class="app-menu__item" href="#" data-toggle="treeview">
        <i class="app-menu__icon fa fa-laptop"></i>
        <span class="app-menu__label">Inbox</span><i class="treeview-indicator fa fa-angle-right"></i>
      </a>
      <ul class="treeview-menu">

        <li><a class="treeview-item" href=" {{ url('/admin/friends-club') }} "><i class="icon fa fa-circle-o"></i>Friends Club</a></li>
        <li><a class="treeview-item" href=" {{ url('/admin/contact-us') }} "><i class="icon fa fa-circle-o"></i>Contact Us</a></li>
      </ul>
    </li>

    <li><a class="app-menu__item" href=" {{ url('admin/url-menu') }} "><i class="app-menu__icon fa fa-edit"></i><span class="app-menu__label">URL Menu</span></a></li>
    
    <li class="treeview">
      <a class="app-menu__item" href="#" data-toggle="treeview">
        <i class="app-menu__icon fa fa-edit"></i> <span class="app-menu__label">Pages</span><i class="treeview-indicator fa fa-angle-right"></i>
      </a>
      <ul class="treeview-menu">
        <li><a class="treeview-item" href=" {{ url('/admin/ingredients') }} "><i class="icon fa fa-circle-o"></i>Ingredients</a></li>
        <li><a class="treeview-item" href=" {{ url('/admin/friends-club-benefit') }} "><i class="icon fa fa-circle-o"></i>Friens Club's Benefit</a></li>
        <li><a class="treeview-item" href=" {{ url('/admin/about-us') }} "><i class="icon fa fa-circle-o"></i>Abous Us</a></li>
        <li><a class="treeview-item" href=" {{ url('/admin/what-do-our-logo-mean') }} "><i class="icon fa fa-circle-o"></i>What Do Our Logo's Mean</a></li>
      </ul>
    </li>

    <li class="treeview">
      <a class="app-menu__item" href="#" data-toggle="treeview">
        <i class="app-menu__icon fa fa-edit"></i> <span class="app-menu__label">FAQ</span><i class="treeview-indicator fa fa-angle-right"></i>
      </a>
      <ul class="treeview-menu">
        <li><a class="treeview-item" href=" {{ url('/admin/faq-naif-baby') }} "><i class="icon fa fa-circle-o"></i>FAQ Naif Baby</a></li>
      </ul>
      <ul class="treeview-menu">
        <li><a class="treeview-item" href=" {{ url('/admin/faq-naif-care') }} "><i class="icon fa fa-circle-o"></i>FAQ Naif Care</a></li>
      </ul>
      <ul class="treeview-menu">
        <li><a class="treeview-item" href=" {{ url('/admin/faq-other') }} "><i class="icon fa fa-circle-o"></i>FAQ Other</a></li>
      </ul>
    </li>

    <li class="treeview">
      <a class="app-menu__item" href="#" data-toggle="treeview">
        <i class="app-menu__icon fa fa-edit"></i> <span class="app-menu__label">Master Data</span><i class="treeview-indicator fa fa-angle-right"></i>
      </a>
      <ul class="treeview-menu">
        <li><a class="treeview-item" href=" {{ url('/admin/article') }} "><i class="icon fa fa-circle-o"></i>Article</a></li>
        <li><a class="treeview-item" href=" {{ url('/admin/banner') }} "><i class="icon fa fa-circle-o"></i>Banner</a></li>
      </ul>
    </li>

    <li class="treeview">
      <a class="app-menu__item" href="#" data-toggle="treeview">
        <i class="app-menu__icon fa fa-edit"></i> <span class="app-menu__label">Setting</span><i class="treeview-indicator fa fa-angle-right"></i>
      </a>
      <ul class="treeview-menu">
        <li><a class="treeview-item" href=" {{ url('/admin/profile') }} "><i class="icon fa fa-circle-o"></i>Profile</a></li>
        <li><a class="treeview-item" href=" {{ url('/admin/user-access') }} "><i class="icon fa fa-circle-o"></i>User Access</a></li>
        <li><a class="treeview-item" href=" {{ url('/admin/roles') }} " hidden><i class="icon fa fa-circle-o"></i>Role Access</a></li>
      </ul>
    </li>

  </ul>
</aside>
