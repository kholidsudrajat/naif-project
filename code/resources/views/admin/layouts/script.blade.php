

    <!-- Essential javascripts for application to work-->

    <script src=" {{ url('assets/admin/js/jquery-3.2.1.min.js') }} "></script>
    <script src=" {{ url('assets/admin/js/popper.min.js') }} "></script>
    <script src=" {{ url('assets/admin/js/bootstrap.min.js') }} "></script>
    <script src=" {{ url('assets/admin/js/main.js') }} "></script>

    <!-- The javascript plugin to display page loading on top-->
    <script src=" {{ url('assets/admin/plugins/pace.min.js') }} "></script>

    <!-- Page specific javascripts-->
    <script type="text/javascript" src=" {{ url('assets/admin/plugins/chart.js') }} "></script>

    <!-- Data table plugin-->
    <script type="text/javascript" src=" {{ url('assets/admin/plugins/jquery.dataTables.min.js') }} "></script>
    <script type="text/javascript" src=" {{ url('assets/admin/plugins/dataTables.bootstrap.min.js') }} "></script>

    <!-- Page specific javascripts-->
    <script type="text/javascript" src=" {{ url('assets/admin/plugins/bootstrap-notify.min.js')}} "></script>
    <script type="text/javascript" src=" {{ url('assets/admin/plugins/sweetalert.min.js') }} "></script>

    <script type="text/javascript" src=" {{ url('js/ckeditor/ckeditor.js') }} "></script>
    <script type="text/javascript" src=" {{ url('js/ckeditor/styles.js') }} "></script>

    <!-- jQuery -->
    <script type="text/javascript" src="{{ url('assets/admin/plugins/daterangepicker/moment.min.js') }}"></script>
    <script type="text/javascript" src="{{ url('assets/admin/plugins/daterangepicker/daterangepicker.min.js') }}"></script>
    <!-- DataTables -->
    <script type="text/javascript" src="{{ url('assets/admin/plugins/datatables/jquery.dataTables.min.js')}}"></script>
    {{-- <script type="text/javascript" src="http://cdn.datatables.net/plug-ins/1.10.15/dataRender/datetime.js"></script> --}}


    <script type="text/javascript">


        $('#sampleTable').DataTable();

        CKEDITOR.replace('top-form');
        CKEDITOR.replace('form-ckeditor');
        CKEDITOR.replace('form-ckeditor1');
        CKEDITOR.replace('form-ckeditor2');
        // var data = {
        //     labels: ["January", "February", "March", "April", "May"],
        //     datasets: [
        //         {
        //             label: "My First dataset",
        //             fillColor: "rgba(220,220,220,0.2)",
        //             strokeColor: "rgba(220,220,220,1)",
        //             pointColor: "rgba(220,220,220,1)",
        //             pointStrokeColor: "#fff",
        //             pointHighlightFill: "#fff",
        //             pointHighlightStroke: "rgba(220,220,220,1)",
        //             data: [65, 59, 80, 81, 56]
        //         },
        //         {
        //             label: "My Second dataset",
        //             fillColor: "rgba(151,187,205,0.2)",
        //             strokeColor: "rgba(151,187,205,1)",
        //             pointColor: "rgba(151,187,205,1)",
        //             pointStrokeColor: "#fff",
        //             pointHighlightFill: "#fff",
        //             pointHighlightStroke: "rgba(151,187,205,1)",
        //             data: [28, 48, 40, 19, 86]
        //         }
        //     ]
        // };
        // var pdata = [
        //     {
        //         value: 300,
        //         color: "#46BFBD",
        //         highlight: "#5AD3D1",
        //         label: "Complete"
        //     },
        //     {
        //         value: 50,
        //         color:"#F7464A",
        //         highlight: "#FF5A5E",
        //         label: "In-Progress"
        //     }
        // ]

        // var ctxl = $("#lineChartDemo").get(0).getContext("2d");
        // var lineChart = new Chart(ctxl).Line(data);

        // var ctxp = $("#pieChartDemo").get(0).getContext("2d");
        // var pieChart = new Chart(ctxp).Pie(pdata);
      </script>

