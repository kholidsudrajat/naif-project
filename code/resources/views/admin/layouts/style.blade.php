

<link rel="stylesheet" type="text/css" href=" {{ url('assets/admin/css/main.css') }} ">
<!-- Font-icon css-->
<link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">


<link rel="stylesheet" type="text/css" href="{{ url('assets/admin/plugins/daterangepicker/daterangepicker.css') }}" />
{{-- <link rel="stylesheet" href="{{ url('assets/admin/plugins/datatables/jquery.dataTables.min.css') }}"> --}}
<style>

  .table-bordered .unconfirm {
      background-color: #ffc107;
      color: #ffffff;
    }
    .table-bordered .confirm {
      background-color: #009688;
      color: #ffffff;
    }
</style>
