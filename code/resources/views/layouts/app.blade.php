<!-- <!doctype html> -->
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <link rel="icon" href="{{ url('/assets/web/icons/logo.svg') }}" type="image/svg">
    <title>{{ config('app.name', 'Naif Indonesia') }}</title>

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}"></script>
    <!-- Styles -->
   @include('admin.layouts.style')
</head>
<body>
     <!-- Navbar-->
   @include('admin.layouts.header')
     <!-- Sidebar menu-->
    @include('admin.layouts.sidebar')
        <main class="app-content">
            @yield('content')
        </main>
    @include('admin.layouts.script')
    @stack('script')
</body>
</html>
