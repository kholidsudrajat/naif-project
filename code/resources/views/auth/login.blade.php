<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="title" content="Naif Indonesia">
        <meta name="description" content="Naif CMS">
        <link rel="icon" href="{{ url('/assets/web/icons/logo.svg') }}" type="image/svg">
        @include('admin.layouts.style')
        <!-- Font-icon css-->
        <link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
        <title>{{ config('app.name', 'Laravel') }}</title>
    </head>
    <body>
        <section class="material-half-bg">
        <div class="cover"></div>
        </section>
        <section class="login-content">
        <div class="logo">
            <h1>Administrator</h1>
        </div>
        <div class="login-box">
            <form class="login-form"  method="POST" action="{{ route('login') }}">
                {{ csrf_field() }}
            <h3 class="login-head"><i class="fa fa-lg fa-fw fa-user"></i>SIGN IN</h3>
            <div class="form-group">
                <label class="control-label">USERNAME</label>
                <input class="form-control" type="text" placeholder="Email" name="email" value="{{ old('email') }}" required autofocus>
                    @if ($errors->has('email'))
                        <span class="help-block">
                            <strong>{{ $errors->first('email') }}</strong>
                        </span>
                    @endif
            </div>
            <div class="form-group">
                <label class="control-label">PASSWORD</label>
                <input class="form-control" type="password" placeholder="Password" name="password" required>
                    @if ($errors->has('password'))
                        <span class="help-block">
                            <strong>{{ $errors->first('password') }}</strong>
                        </span>
                    @endif
            </div>
            <div class="form-group">
                <div class="utility">
                <div class="animated-checkbox">
                    <label>
                    <input type="checkbox" name="remember" {{ old('remember') ? 'checked' : '' }}><span class="label-text">Remember Me</span>
                    </label>
                </div>
                <p class="semibold-text mb-2" hidden><a href="#" data-toggle="flip">Forgot Password ?</a></p>
                </div>
            </div>
            <div class="form-group btn-container">
                <button type="submit" class="btn btn-primary btn-block"><i class="fa fa-sign-in fa-lg fa-fw"></i>SIGN IN</button>
            </div>
            </form>
            <form class="forget-form" action="index.html" hidden>
            <a href="{{ route('password.request') }}"><h3 class="login-head"><i class="fa fa-lg fa-fw fa-lock"></i>Forgot Password ?</h3></a>
            <div class="form-group">
                <label class="control-label">EMAIL</label>
                <input class="form-control" type="text" placeholder="Email">
            </div>
            <div class="form-group btn-container">
                <button class="btn btn-primary btn-block"><i class="fa fa-unlock fa-lg fa-fw"></i>RESET</button>
            </div>
            <div class="form-group mt-3">
                <p class="semibold-text mb-0"><a href="#" data-toggle="flip"><i class="fa fa-angle-left fa-fw"></i> Back to Login</a></p>
            </div>
            </form>
        </div>
        </section>
        <!-- Essential javascripts for application to work-->
        @include('admin.layouts.script')
        <script type="text/javascript">
        // Login Page Flipbox control
        $('.login-content [data-toggle="flip"]').click(function() {
            $('.login-box').toggleClass('flipped');
            return false;
        });
        </script>
    </body>
</html>
