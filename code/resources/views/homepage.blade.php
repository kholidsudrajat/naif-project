<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <link rel="icon" href="{{ url('/assets/web/icons/logo.svg') }}" type="image/svg">
  <title>Naif Indonesia</title>
  <meta property="og:url" content="{{ url('/') }}" />
  <meta property="og:type" content="website" />
  <meta property="og:title" content="Naif Indonesia">
  @foreach ($profile as $profile_about)
    <meta name="description" content="{{ $profile_about->meta_tags }}">
    <meta property="og:image" content="{{ url('upload/profile/'.$profile_about->image) }}" />
  @endforeach
  @include('web.layouts.style')

</head>
<body>
  @include('web.layouts.header')
  <div id="homepage">
    <div class="">
      <div class="content">
        <div class="page-container homepage">

          <div class="main-banner-container swiper-container">

            @if (count($banner) >0)
              <div class="slider homepage-slide swiper-wrapper main-banner banner-home">
                @foreach ($banner as $banner_view)
                  @if ($banner_view->sup_title != null || $banner_view->title != null || $banner_view->description != null)

                    <div class="main-banner-img swiper-slide" style="background-image:url('{{ url('upload/banner/'.$banner_view->image) }}')">
                      <!-- <img src="{{ url('upload/banner/'.$banner_view->image) }}" class="img-fluid img-center img-desktop"> -->
                      <div class="main-banner-text" data-swiper-parallax="-500">

                        <div class="main-banner-text-inner container">
                          <div class="flex-wrap-center">
                            <div class="page-container-outer">
                              <div class="box-main-text">
                                <h6>{{ $banner_view->sup_title }}</h6>
                                <h1>{{ $banner_view->title }}</h1>
                                <div class="wrapper-desc-slider d-md-block d-lg-block d-sm-none d-none">
                                  {!! html_entity_decode($banner_view->description) !!}
                                </div>
                                <div class="wrapper-desc-slider d-sm-block d-xs-block d-md-none d-lg-none">
                                  <!-- {!! html_entity_decode($banner_view->description) !!} -->
                                  {!!(Str::limit($banner_view->description, 80, $end='...')) !!}
                                </div>
                                @if ($banner_view->url_page != null)
                                  <a href="{{ url($banner_view->url_page) }}">
                                    <button type="button" class="btn btn-info">Lihat Selengkapnya</button>
                                  </a>
                                @else
                                @endif
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>

                  @else
                  <div>
                    @if ($banner_view->url_page != null)
                      <a href="{{ url($banner_view->url_page) }}">
                        <div class="main-banner-img  swiper-slide" style="background-image:url('{{ url('upload/banner/'.$banner_view->image) }}')"></div>
                        <!-- <img src="{{ url('upload/banner/'.$banner_view->image) }}" class="img-fluid img-center img-desktop"> -->
                      </a>
                    @else
                      <div class="main-banner-img  swiper-slide" style="background-image:url('{{ url('upload/banner/'.$banner_view->image) }}')"></div>
                    @endif
                  </div>
                  @endif
                @endforeach
              </div>
            @else
              <div class="main-banner banner-master">
                <div class="main-banner-img ">
                  <img src="{{ url('upload/banner/placeholder.jpg') }}" class="img-fluid img-center img-desktop" style="visibility: hidden">
                </div>
              </div>
            @endif
            
            <!-- Add Pagination -->
            <div class="swiper-pagination swiper-pagination-white"></div>
            <!-- Add Navigation -->
            <div class="swiper-button-prev swiper-button-white"></div>
            <div class="swiper-button-next swiper-button-white"></div>

          </div>
          <div class="page-container-outer">
            <div class="page-counter-inner">
              @foreach ($profile as $profile_about)
                <div class="about-container">
                  <div class="container">
                    <div class="box-about-title">
                      <h2>ABOUT NAIF</h2>
                    </div>
                    <div class="box-about-image-home">
                      <div class="about-image-inner-home row">
                        <div class="col-md-7">
                          <div class="about-img-home">
                            <img src="{{ url('upload/profile/'.$profile_about->image) }}" class="img-fluid img-center" alt=" ">
                          </div>
                        </div>
                        <div class="col-md-5">
                          <div class="about-image-text-outer-home">
                            <div class="about-image-text-inner-home">
                              <div class="about-image-text">
                                <h4 class="title_about">{!! $profile_about->title !!}</h4>
                                {!!(Str::limit($profile_about->description, 320, $end='...')) !!}
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              @endforeach
            </div>
          </div>

          <div class="article-container ">
            <div class="box-article-inner" id="list-article">
              <div class="box-article-wrapper">
                <h2 class="font-h2">BLOGS</h2>
                {{ csrf_field() }}
                <div class="container">
                  <div id="post_data" class="row"></div>
                </div>
                <div class="load_more_button d-sm-block d-block d-md-none d-lg-none">
                  <a href={{ url('blogs') }}>
                    <button class="more-articles">
                      Load more
                    </button>
                  </a>
                </div>
              </div>
              <div class="load_more_button d-md-block d-lg-block d-sm-none d-none">
                <a href={{ url('blogs') }}>
                  <button class="more-articles">
                    Load more
                  </button>
                </a>
              </div>
            </div>
          </div>

          <!-- <div class="page-container article-container">
            <div class="page-container-outer">
              <div class="page-counter-inner">
                <div class="box-article-inner" id="list-article">
                  <h2 class="font-h2">BLOGS</h2>
                  {{ csrf_field() }}
                  <div class="box-article-wrapper">
                    <div class="container">
                      <div id="post_data" class="row"></div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div> -->

        </div>
      </div>
      <div class="overlay"></div>
    </div>
  </div>
  @include('web.layouts.footer')

   @include('web.layouts.script')

  <script>
    $(document).ready(function(){
      var _token = $('input[name="_token"]').val();
      load_data('', _token);
      function load_data(id="", _token) {
        $.ajax({
          url: "{{ route('loadmore.load_data')}}",
          method: "POST",
          data:{id:id, _token:_token},
          success: function(data)
          {
            $('#load_more_button').remove();
            $('#post_data').append(data);
          }
        });
      }
      $(document).on('click', '#load_more_button', function() {
        var id= $(this).data('id');
        $('#load_more_button').html('<b>Loading ...</b>');

        load_data(id, _token);
      });
    });

    var swiper = new Swiper('.swiper-container', {
      speed: 2600,
      loop: true,
      slidesPerView: 1,
      parallax: true,
      pagination: {
        el: '.swiper-pagination',
        clickable: true,
      },
      autoplay: {
        delay: 4500,
        disableOnInteraction: false,
      },
      navigation: {
        nextEl: '.swiper-button-next',
        prevEl: '.swiper-button-prev',
      },
    });
  </script>
</body>
</html>
