<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>{{ $value_search }} | Search | Naif Indonesia </title>
    <link rel="icon" href="{{ url('/assets/web/icons/logo.svg') }}" type="image/svg">
    @include('web.layouts.style')
</head>
<body>
	@include('web.layouts.header')
	<div class="container">
		<div class="page-container search-result">
			<div class="page-container-outer">
				<div class="page-counter-inner">
					<div class="box-search-results">

						@if ($value_search == '')
							<div class="box-result-title">
								<h2>Ingredients</h2>
								<h6> 0 results</h6>
							</div>
						@else
							<div class="box-result-title flex-between">
								<h2>Ingredients</h2>
								@if($ingredientsCount = count($ingredients) >0)
									<h6> {{ $ingredientsCount = count($ingredients) }} results</h6>
								@else
									<h6> 0 results</h6>
								@endif
							</div>
							<div class="box-ingredient">
								<div class="row">
									@foreach ($ingredients as $ingredients_view)
										<div class="col-md-6 col-sm-6 col-6">
											<div class="custom-card text-center">
												<div class="custom-card-img">
													<img src=" {{ url('upload/ingredients/'.$ingredients_view->image) }} " alt="" class="img-fluid">
												</div>
												<h4 class="card-title-content">{{ $ingredients_view->title}}</h4>
												{!! html_entity_decode($ingredients_view->description) !!}
											</div>
										</div>
									@endforeach
								</div>
								
							</div>
						@endif
						@if ($value_search == '')
							<div class="box-article">
								<div class="box-result-title flex-between">
									<h2>Artikel</h2>
									<h6> 0 results</h6>
								</div>
							</div>
						@else
							<div class="box-article">
								<div class="box-result-title flex-between">
									<h2>Artikel</h2>
									@if($articleCount = count($article) >0)
										<h6> {{ $articleCount = count($article) }} results</h6>
									@else
										<h6> 0 results</h6>
									@endif

								</div>
								<div class="row">
									@foreach ($article as $article_view)
										<div class="col-md-4 col-sm-6 col-xs-6 col-6">
											<div class="box-blogs">
												<div class="blogs-image">
													<img src="{{ url('upload/article/'.$article_view->image) }}" class="img-fluid img-center" alt="Responsive image">
												</div>
												<div class="blogs-text blogs-limit">
													<a href="{{ url('article/'.$article_view->slug) }}">
														<h5 class="text-center">{{ $article_view->title }}</h5>
													</a>
													{!!(Str::limit($article_view->description, 120, $end='...')) !!}
													<!-- <p class="text-center">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed imperdiet non elit et molestie. In non erat nec ipsum scelerisque feugiat. </p> -->
	
													<div class="blogs-post-on">
														<div class="d-flex">
															<div class="mr-auto posted-on">Posted on:</div>
															<div class="date-post">{{ date('d F Y', strtotime($article_view->created_at)) }}</div>
														</div>
													</div>
												</div>
											</div>
										</div>

									<!-- <div class="box-article-inner flex-between">
										<div class="article-image">
											<img src=" {{ url('upload/article/'.$article_view->image) }} " class="img-center">
										</div>
										<div class="article-text">
											<h5> {{ $article_view->title }} </h5>
											{!! Str::limit($article_view->description, 500, $end='...') !!}

											<div class="article-readmore">
												<a href=" {{ url("article/".$article_view->slug) }} ">
													<button type="button" class="btn btn-light">Read more</button>
												</a>
											</div>
										</div>
									</div> -->
									@endforeach
								</div>
							</div>
							@endif
					</div>
				</div>
			</div>
		</div>
		<div class="overlay"></div>
	</div>
	@include('web.layouts.footer')
	@include('web.layouts.script')
</body>
</html>
