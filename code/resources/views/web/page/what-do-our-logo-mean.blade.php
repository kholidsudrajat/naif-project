<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <link rel="icon" href="{{ url('/assets/web/icons/logo.svg') }}" type="image/svg">

  <title> What do our logo’s mean? | Naif Indonesia </title>
  @include('web.layouts.style')

</head>

<body>
@include('web.layouts.header')
  <div id="what-do">
    <div class="container">
      <div class="what_do_wrapper_top">
        @if (count($banner)>0)
          @foreach($banner as $banner_view)
            <h1 class="title-page">{{ $banner_view->title }}</h1>
            <div class="desc-head-page">
            {!! html_entity_decode($banner_view->description) !!}
            </div>
          @endforeach
        @endif
      </div>
    </div>
    <div class="container">
      <div class="what_do_wrapper_bottom">
        @if(count($section1)>0)
          @foreach($section1 as $section1_view)
            <div class="card_what_do_logo">
              <div class="d-lg-block d-md-block d-sm-none d-none">
                <div class="media">
                  <img class="what_do_img" src="{{ url('upload/what-do-our-logo-mean/'.$section1_view->image) }}" alt="">
                  <div class="media-body">
                    <div class="fp-table">
                      <div class="fp-tableCell">
                        <h5 class="what_do_title">{{ $section1_view->title}}</h5>
                        
                          {!! html_entity_decode($section1_view->description) !!}
                        
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <div class="d-lg-none d-md-none d-sm-block d-block">
                <img class="what_do_img pos_left" src="{{ url('upload/what-do-our-logo-mean/'.$section1_view->image) }}" alt="">
                <h5 class="what_do_title text-right">{{ $section1_view->title}}</h5>
                {!! html_entity_decode($section1_view->description) !!}
              </div>
            </div>
          @endforeach
        @endif

        @if(count($section2)>0)
          @foreach($section2 as $section2_view)
            <div class="card_what_do_logo">
              <div class="d-lg-block d-md-block d-sm-none d-none">
                <div class="media">
                  <img class="what_do_img" src="{{ url('upload/what-do-our-logo-mean/'.$section2_view->image) }}" alt="">
                  <div class="media-body">
                    <div class="fp-table">
                      <div class="fp-tableCell">
                        <h5 class="what_do_title">{{ $section2_view->title}}</h5>
                        {!! html_entity_decode($section2_view->description) !!}
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <div class="d-lg-none d-md-none d-sm-block d-block">
                <img class="what_do_img pos_right" src="{{ url('upload/what-do-our-logo-mean/'.$section2_view->image) }}" alt="">
                <h5 class="what_do_title text-left">{{ $section2_view->title}}</h5>
                {!! html_entity_decode($section2_view->description) !!}
              </div>
            </div>
          @endforeach
        @endif

        @if(count($section3)>0)
          @foreach($section3 as $section3_view)
            <div class="card_what_do_logo">
              <div class="d-lg-block d-md-block d-sm-none d-none">
                <div class="media">
                  <img class="what_do_img" src="{{ url('upload/what-do-our-logo-mean/'.$section3_view->image) }}" alt="">
                  <div class="media-body">
                    <div class="fp-table">
                      <div class="fp-tableCell">
                        <h5 class="what_do_title">{{ $section3_view->title}}</h5>
                        {!! html_entity_decode($section3_view->description) !!}
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <div class="d-lg-none d-md-none d-sm-block d-block">
                <img class="what_do_img pos_left" src="{{ url('upload/what-do-our-logo-mean/'.$section3_view->image) }}" alt="">
                <h5 class="what_do_title text-right">{{ $section3_view->title}}</h5>
                {!! html_entity_decode($section3_view->description) !!}
              </div>
            </div>
          @endforeach
        @endif

      </div>
    </div>
  </div>
@include('web.layouts.footer')

@include('web.layouts.script')
</body>