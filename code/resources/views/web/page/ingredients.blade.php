<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <link rel="icon" href="{{ url('/assets/web/icons/logo.svg') }}" type="image/svg">
  <title>Ingredients | Naif Indonesia </title>
  <meta property="og:url" content="{{ url('/ingredients') }}" />
  <meta property="og:type" content="website" />
  @foreach ($banner as $banner_view)
    <meta property="og:title" content="{{ $banner_view->title }} - Naif Indonesia">
    <meta property="og:description" content="{{ $banner_view->meta_tags }}">
    <meta property="og:image" content="{{ url('upload/banner/'.$banner_view->image) }}" />
  @endforeach
  @include('web.layouts.style')

</head>
<body>
@include('web.layouts.header')
<div id="ingredient">
  <div class="wrapper">
    <div class="content">
      <div class="page-container moms-testimonial product-master width-100">
        <div class="container">
          <div class="header-text">
            <div class="text-center">
              <h1>{{ $banner_view->title }}</h1>
              {!! html_entity_decode($banner_view->description) !!}
            </div>
          </div>
        </div>
        
        <div class="page-container-outer">
          <div class="page-counter-inner">
            <div class="box-ingredient">
              <div class="container">
                <div class="row" id="post_data"></div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="overlay"></div>
  </div>
</div>
@include('web.layouts.footer')
<input hidden id="maxIDTemp">
<input hidden id="minIDTemp">

@include('web.layouts.script')


<script>

  // ====================================================
  $(document).ready(function(){

    var _token = '{{ csrf_token() }}';

    load_data('', _token);
    get_maxId();
    get_minId();
    function load_data(id="", _token)
    {
      $.ajax({
        url: "{{ route('ingredients.load-data')}}",
        method: "POST",
        data:
        {
          id:id,
          _token:_token
        },
        success: function(data)
        {
          $('#load_more_button').remove();
          $('#post_data').append(data);
        }
      });
    }
    $(document).on('click', '#load_more_button', function(){
      var id= $(this).data('id');
      $('#load_more_button').html('<b>Loading ...</b>');

      load_data(id, _token);
    });
  });
</script>

</body>
</html>
