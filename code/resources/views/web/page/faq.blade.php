<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <link rel="icon" href="{{ url('/assets/web/icons/logo.svg') }}" type="image/svg">

  <title> FAQ | Naif Indonesia </title>
  @include('web.layouts.style')

</head>

<body>
@include('web.layouts.header')
  <div id="faq">
    <div class="container">
      <div class="faq_wrapper">
        @if (count($banner)>0)
          @foreach($banner as $banner_view)
            <h1 class="title-page">{{ $banner_view->title }}</h1>
            {!! html_entity_decode($banner_view->description) !!}
          @endforeach
        @endif
        <div class="d-lg-block d-md-block d-sm-none d-none">
          <div class="row">
            <div class="col-md-6">
              <div class="card_faq">
                <h3 class="card_title">NAIF Baby</h3>
                <ul class="list-group help-group">
                  <div class="faq-list list-group nav nav-tabs">
                    @if (count($faq_naif_baby)>0)
                      @foreach($faq_naif_baby as $faq_naif_baby_view)
                        <a href="{{ '#naifbaby'.$faq_naif_baby_view->id }}" class="list-group-item" role="tab" data-toggle="tab"> {{ $faq_naif_baby_view->question }} </a>
                      @endforeach
                    @endif
                  </div>
                </ul>
              </div>
            </div>
            <div class="col-md-6">
              <div class="tab-content panels-faq">
                @if (count($faq_naif_baby)>0)
                  @foreach($faq_naif_baby as $faq_naif_baby_view)
                    <div class="tab-pane" id="{{ 'naifbaby'.$faq_naif_baby_view->id }}">
                      <h4>Jawaban</h4>
                      {!! html_entity_decode($faq_naif_baby_view->answer) !!}
                    </div>
                  @endforeach
                @endif
              </div>
            </div>
          </div>
          <div class="row">
            <div class="col-md-6">
              <div class="card_faq">
                <h3 class="card_title">NAIF Care</h3>
                <ul class="list-group help-group">
                  <div class="faq-list list-group nav nav-tabs">
                    @if (count($faq_naif_care)>0)
                      @foreach($faq_naif_care as $faq_naif_care_view)
                        <a href="{{ '#naifcare'.$faq_naif_care_view->id }}" class="list-group-item" role="tab" data-toggle="tab"> {{ $faq_naif_care_view->question }} </a>
                      @endforeach
                    @endif
                  </div>
                </ul>
              </div>
            </div>
            <div class="col-md-6">
              <div class="tab-content panels-faq">
                @if (count($faq_naif_care)>0)
                  @foreach($faq_naif_care as $faq_naif_care_view)
                    <div class="tab-pane" id="{{ 'naifcare'.$faq_naif_care_view->id }}">
                      <h4>Jawaban</h4>
                      {!! html_entity_decode($faq_naif_care_view->answer) !!}
                    </div>
                  @endforeach
                @endif
              </div>
            </div>
          </div>
          <div class="row">
            <div class="col-md-6">
              <div class="card_faq">
                <h3 class="card_title">Other</h3>
                <ul class="list-group help-group">
                  <div class="faq-list list-group nav nav-tabs">
                    @if (count($faq_other)>0)
                      @foreach($faq_other as $faq_other_view)
                        <a href="{{ '#other'.$faq_other_view->id }}" class="list-group-item" role="tab" data-toggle="tab"> {{ $faq_other_view->question }} </a>
                      @endforeach
                    @endif
                  </div>
                </ul>
              </div>
            </div>
            <div class="col-md-6">
              <div class="tab-content panels-faq">
                @if (count($faq_other)>0)
                  @foreach($faq_other as $faq_other_view)
                    <div class="tab-pane" id="{{ 'other'.$faq_other_view->id }}">
                      <h4>Jawaban</h4>
                      {!! html_entity_decode($faq_other_view->answer) !!}
                    </div>
                  @endforeach
                @endif
              </div>
            </div>
          </div>
        </div>
        <div class="d-lg-none d-md-none d-sm-block d-block">
          <div class="card_faq">
            <h3 class="card_title">NAIF Baby</h3>
            <div class="faq-list">
              @if (count($faq_naif_baby)>0)
                @foreach($faq_naif_baby as $faq_naif_baby_view)
                  <a class="list-group-item collapsed" data-toggle="collapse" href="{{ '#naifbaby'.$faq_naif_baby_view->id }}" role="button" aria-expanded="false" aria-controls="{{ 'naifbaby'.$faq_naif_baby_view->id }}">
                    {{ $faq_naif_baby_view->question }}
                  </a>
                  <div class="collapse" id="{{ 'naifbaby'.$faq_naif_baby_view->id }}">
                    <div class="panels-faq">
                      <div class="tab-pane">
                        <h4>Jawaban</h4>
                        {!! html_entity_decode($faq_naif_baby_view->answer) !!}
                      </div>
                    </div>
                  </div>
                @endforeach
              @endif
            </div>
          </div>
          
          <div class="card_faq">
            <h3 class="card_title">NAIF Care</h3>
            <div class="faq-list">
            @if (count($faq_naif_care)>0)
                @foreach($faq_naif_care as $faq_naif_care_view)
                  <a class="list-group-item collapsed" data-toggle="collapse" href="{{ '#naifcare'.$faq_naif_care_view->id }}" role="button" aria-expanded="false" aria-controls="{{ 'naifcare'.$faq_naif_care_view->id }}">
                    {{ $faq_naif_care_view->question }}
                  </a>
                  <div class="collapse" id="{{ 'naifcare'.$faq_naif_care_view->id }}">
                    <div class="panels-faq">
                      <div class="tab-pane">
                        <h4>Jawaban</h4>
                        {!! html_entity_decode($faq_naif_care_view->answer) !!}
                      </div>
                    </div>
                  </div>
                @endforeach
              @endif
            </div>
          </div>
          
          <div class="card_faq">
            <h3 class="card_title">Other</h3>
            <div class="faq-list">
            @if (count($faq_other)>0)
                @foreach($faq_other as $faq_other_view)
                  <a class="list-group-item collapsed" data-toggle="collapse" href="{{ '#other'.$faq_other_view->id }}" role="button" aria-expanded="false" aria-controls="{{ 'other'.$faq_other_view->id }}">
                    {{ $faq_other_view->question }}
                  </a>
                  <div class="collapse" id="{{ 'other'.$faq_other_view->id }}">
                    <div class="panels-faq">
                      <div class="tab-pane">
                        <h4>Jawaban</h4>
                        {!! html_entity_decode($faq_other_view->answer) !!}
                      </div>
                    </div>
                  </div>
                @endforeach
              @endif
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
@include('web.layouts.footer')

@include('web.layouts.script')
<script>
  $(".list-group-item").click(function() {
    $(".list-group-item").removeClass('active');
    $(".tab-pane").removeClass('active');
  });
  // $('.list-group-item').on('click', function(){
  //   if($(this).hasClass('active')){
  //     $(this).toggleClass('active');
  //     $('#' + this.hash.substr(1).toLowerCase()).toggleClass('active');
  //     return false;
  //   }
  // });
  // $(document).ready( function(){

  //   function unactiveOther( source ){
  //     // Source element
  //     var $sourceTabLink = $(source),
  //       $sourceTab = $sourceTabLink.parent('a');
          
  //     // wrapper element
  //     var activeNav = $sourceTab.closest('.faq_wrapper')
  //       // find all active
  //       .find('[role=tab].active');
      
  //     activeNav.each( function(){
        
  //       // Get current active nav
  //       var curActiveNav = $(this),
  //         curActiveNavLink = curActiveNav.find('a');
        
  //       // inactive unmatched nav
  //       if ( curActiveNavLink.attr('data-target') !== $sourceTabLink.attr('data-target') ){
  //         curActiveNav.removeClass('active');
  //       }
        
  //     })
  //   }

  //   $('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
  //     //console.log( e.target ); // newly activated tab
  //     //console.log( e.relatedTarget );// previous active tab
  //     unactiveOther( e.target );
  //   })
  //   });
</script>
</body>