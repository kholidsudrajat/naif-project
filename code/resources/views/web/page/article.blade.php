<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <link rel="icon" href="{{ url('/assets/web/icons/logo.svg') }}" type="image/svg">
  @foreach ($article_detail as $article_title)
  <title>{{ $article_title->title }} | Article | Naif Indonesia </title>
  <meta property="og:url" content="{{ url('/article/'.$article_title->slug) }}" />
  <meta property="og:type" content="website" />
  <meta property="og:title" content="{{ $article_title->title }}" />
  <meta property="og:description" content="{{ ($article_title->meta_tags) }}" />
  <meta property="og:image" content="{{ url('upload/article/'.$article_title->image) }}" />
  @endforeach
  @include('web.layouts.style')
</head>
<body>

@include('web.layouts.header')
<div id="fb-root"></div>
<div id="detail_blog" class="detail_blog">
  <div class="article-detail-head">
    <div class="container">
      <div class="img-header-container">
        @if (count($article_detail) >0)
          @foreach ($article_detail as $article_view)
            <h3>{{ $article_view->title }}</h3>
            <div class="main-img-header">
              <div class="main-header-img">
                <img src="{{ url('upload/article/'.$article_view->image) }}" class="img-fluid img-desktop">
              </div>
              <div class="date-post">Post on {{ date('d F Y', strtotime($article_view->created_at)) }}</div>
            </div>
          @endforeach
        @else
          <div class="main-banner">
            <div class="main-banner-img">
              <img src="{{ url('upload/banner/placeholder.jpg') }}" class="img-fluid img-center img-desktop" style="visibility: hidden">
            </div>
          </div>
        @endif
      </div>
    </div>
  </div>
  <div class="page-container-outer container">
    <div class="page-counter-inner">
      <div class="box-article-container">
        {!! html_entity_decode($article_view->description) !!}
      </div>
      <div class="link_wrapper">
        <a href={{ url('blogs') }} class="link-back"><span><</span> Back to Blogs</a>
        <div class="article-share">
          <p>Share on:</p>
          <div class="share-icon">
            <a href="{{ 'https://www.facebook.com/sharer/sharer.php?u=https://naifcare.cranium.id/article/'.$article_view->slug.'&display=popup' }}">
              <div class="fb-share-button" data-href="{{ 'https://naifcare.cranium.id/article/'.$article_view->slug }}"
                data-layout="button_count">
                <img src="{{ url('assets/web/icons/fb_blue.svg') }}" class="img-fluid" alt="Responsive image">
              </div>
            </a>

          </div>
          <div class="share-icon">
            <a a href="{{ 'https://twitter.com/share?url=https://naifcare.cranium.id/article/'.$article_view->slug.'&text='.$article_view->title }}" rel="me" title="Twitter" target="_blank">
              <img src="{{ url('assets/web/icons/twitter.svg') }}" class="img-fluid" alt="Responsive image">
            </a>
          </div>

        </div>
      </div>
    </div>
  </div>
</div>
<div class="overlay"></div>
@include('web.layouts.footer')
<!-- <script>
  (function(d, s, id) {
    var js, fjs = d.getElementsByTagName(s)[0];
    if (d.getElementById(id)) return;
    js = d.createElement(s); js.id = id;
    js.src = "https://connect.facebook.net/en_US/sdk.js#xfbml=1&version=v3.0";
    fjs.parentNode.insertBefore(js, fjs);
  }(document, 'script', 'facebook-jssdk'));
</script> -->
<!-- <script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script> -->

@include('web.layouts.script')


</body>
</html>
