<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <link rel="icon" href="{{ url('/assets/web/icons/logo.svg') }}" type="image/svg">
  <title>Friends Club | Naif Indonesia</title>
  <meta property="og:url" content="{{ url('/friends-club') }}" />
  <meta property="og:type" content="website" />
  @foreach ($banner as $banner_view)
    <meta property="og:title" content="{{ $banner_view->title }} - Naif Indonesia">
    <meta property="og:description" content="{{ $banner_view->meta_tags }}">
    <meta property="og:image" content="{{ url('upload/banner/'.$banner_view->image) }}" />
  @endforeach

  @include('web.layouts.style')
</head>
<body>
@include('web.layouts.header')
<div id="friendsClub">
  <div class="friendsClub_wrapper">
    <div class="content">
      <div class="container">
        <div class="main-banner-container">
          @if (count($banner) >0)
            @foreach ($banner as $banner_view)
              <div class="row">
                <div class="col-md-6">
                  <img src="{{ url('upload/banner/'.$banner_view->image) }}" class="img-fluid img-center img-desktop">
                </div>
                <div class="col-md-6">
                  <div class="fp-table">
                    <div class="fp-tableCell">

                      <div class="header-title-friends">
                        <h4 class="d-lg-block d-md-block d-sm-none d-none">Join  NAIF Friends club and get <span class="color-tosca">Exclusive Benefit!</span></h4>
                        <h4 class="d-lg-none d-md-none d-sm-block d-block">Join  NAIF Friends club and get<br/><span class="color-tosca">Exclusive Benefit!</span></h4>
                        <div class="button-wrapper text-center d-lg-block d-md-block d-sm-none d-none">
                          <button class="btn-reg-friends" type="button" data-toggle="collapse" data-target="#collapseFormRegister" aria-expanded="false" aria-controls="collapseFormRegister">Register Now</button>
                        </div>
                        <div class="button-wrapper text-center d-lg-none d-md-none d-sm-block d-block">
                          <button class="btn-reg-friends btn-reg-mobile" type="button" >Register Now</button>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            @endforeach
          @else
            <div class="main-banner banner-master">
              <div class="main-banner-img ">
                <img src="{{ url('upload/banner/placeholder.jpg') }}" class="img-fluid img-center img-desktop" style="visibility: hidden">
              </div>
            </div>
          @endif
        </div>
      </div>
      <div class="box-friends-club-mobile d-lg-none d-md-none d-sm-block d-block">
        <div class="box-club-content points">
          <h4>Exclusive Benefit</h4>
          <div class="club-content-inner row">
            @if (count($friends_club_benefit) >0)
              @foreach ($friends_club_benefit as $friends_club_benefit_view)
                <div class="col-sm-6 col-6">
                  <div class="custom-card text-center">
                    <div class="custom-card-img">
                      <img src="{{ url('upload/friends-club-benefit/'.$friends_club_benefit_view->image) }} " alt="">
                    </div>
                    <h4 class="card-title-content">{{ $friends_club_benefit_view->title }}</h4>
                    <p>{!! html_entity_decode($friends_club_benefit_view->description) !!}</p>
                  </div>
                </div>
              @endforeach
            @else
              <div class="col-sm-6 col-6">
                <div class="custom-card text-center">
                  <div class="custom-card-img">
                    <img src="{{ url('assets/web/images/lingkaran.png') }} " alt="">
                  </div>
                  <h4 class="card-title-content">Lorem Ipsum</h4>
                  <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit</p>
                </div>
              </div>
              <div class="col-sm-6 col-6">
                <div class="custom-card text-center">
                  <div class="custom-card-img">
                    <img src="{{ url('assets/web/images/lingkaran.png') }} " alt="">
                  </div>
                  <h4 class="card-title-content">Lorem Ipsum</h4>
                  <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit</p>
                </div>
              </div>

              <div class="col-sm-6 col-6">
                <div class="custom-card text-center">
                  <div class="custom-card-img">
                    <img src="{{ url('assets/web/images/lingkaran.png') }} " alt="">
                  </div>
                  <h4 class="card-title-content">Lorem Ipsum</h4>
                  <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit</p>
                </div>
              </div>
              <div class="col-sm-6 col-6">
                <div class="custom-card text-center">
                  <div class="custom-card-img">
                    <img src="{{ url('assets/web/images/lingkaran.png') }} " alt="">
                  </div>
                  <h4 class="card-title-content">Lorem Ipsum</h4>
                  <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit</p>
                </div>
              </div>

              <div class="col-sm-6 col-6">
                <div class="custom-card text-center">
                  <div class="custom-card-img">
                    <img src="{{ url('assets/web/images/lingkaran.png') }} " alt="">
                  </div>
                  <h4 class="card-title-content">Lorem Ipsum</h4>
                  <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit</p>
                </div>
              </div>
              <div class="col-sm-6 col-6">
                <div class="custom-card text-center">
                  <div class="custom-card-img">
                    <img src="{{ url('assets/web/images/lingkaran.png') }} " alt="">
                  </div>
                  <h4 class="card-title-content">Lorem Ipsum</h4>
                  <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit</p>
                </div>
              </div>
            @endif
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="collapse collapse-form-friend" id="collapseFormRegister">
    <div class="d-lg-none d-md-none d-sm-block d-block">
      <button class="close_form_register"><span aria-hidden="true">&times;</span></button>
    </div>
    <div class="page-container-outer container">
      <div class="page-counter-inner">
        <div class="parents-club-container row">
          <div class="box-club-content form col-md-6">
            <h4>REGISTER NOW!</h4>
            <div class="club-content-inner ">
              <form action="{{ url('store-friends-club') }}" method="post">
                {{ csrf_field() }}
                <div class="box-form">

                  <label>Full Name</label>
                  <div class="input-group">
                    {!! Form::text('name', null,['class' => 'form-control', 'placeholder'=>'Full Name','required'=>'required']) !!}
                  </div>

                  <label>Instagram</label>
                  <div class="input-group">
                    {!! Form::text('instagram', null,['class' => 'form-control', 'placeholder'=>'Instagram']) !!}
                  </div>

                  <label>Email</label>
                  <div class="input-group">
                    {!! Form::email('email', null,['class' => 'form-control', 'placeholder'=>'Email','required'=>'required']) !!}
                  </div>

                  <label>Address</label>
                  <div class="input-group">
                    {!! Form::textarea('address', null,['class' => 'form-control', 'placeholder'=>'Address','required'=>'required','cols'=>'30','rows'=>'3']) !!}
                  </div>

                  <div class="row">
                    <div class="col-md-6">
                      <label>Province</label>
                      <div class="input-group regional">
                        <div class="box-select" for="id_label_prov">
                          {!! Form::select('provinsi', $province_data ,null,['class' => 'js-example-basic-single js-states form-control', 'id' => 'id_label_prov', 'required'=>'required']) !!}
                        </div>
                      </div>                            
                    </div>

                    <div class="col-md-6">
                      <label>City or Districts</label>
                      <div class="input-group regional">
                        <div class="box-select" for="id_label_kota">
                          <select name="kota" class="js-example-basic-single js-states form-control" id="id_label_kota" required>
                            <option value="">- City or Districts -</option>
                          </select>
                        </div>
                      </div>

                    </div>
                  </div>
                  <div class="row">
                    <div class="col-md-6">
                      <label>Sub-district</label>
                      <div class="input-group regional">
                        <div class="box-select" for="id_label_kec">
                          <select name="kecamatan" class="js-example-basic-single js-states form-control" id="id_label_kec" required>
                            <option value="">- Sub-district -</option>
                          </select>
                        </div>
                      </div>
                    </div>
                    <div class="col-md-6">
                      <label>Post Code</label>
                      <div class="input-group regional">
                        <input name="pos_code" id="pos_code" type="text" value="" class="form-control pos-code" required placeholder="Post Code">
                      </div>
                    </div>
                  </div>
                  <label>Phone</label>
                  <div class="input-group">
                    {!! Form::text('no_telp', null,['class' => 'form-control', 'id'=>'no_telp', 'placeholder'=>'Phone', 'maxlength' => '13', 'required'=>'required']) !!}
                  </div>
                  <div class="input-group">
                    <div class="g-recaptcha" data-sitekey="6LdODsoaAAAAAOwKk1mGLl03rjlq6fY9odSe8JAJ" data-callback="correctCaptcha"></div>
                    <script type="text/javascript" src="https://www.google.com/recaptcha/api.js?hl=en"></script>
                  </div>
                  <div class="input-group ">
                    <button type="submit" class="btn btn-info ">Submit</button>
                  </div>
                </div>
              </form>
            </div>
          </div>

          <div class="box-club-content points col-md-6 d-lg-block d-md-block d-sm-none d-none">
            <h4>Exclusive Benefit</h4>
            <div class="club-content-inner row">
              @if (count($friends_club_benefit) >0)
                @foreach ($friends_club_benefit as $friends_club_benefit_view)
                  <div class="col-md-6">
                    <div class="custom-card text-center">
                      <div class="custom-card-img">
                        <img src="{{ url('upload/friends-club-benefit/'.$friends_club_benefit_view->image) }} " alt="">
                      </div>
                      <h4 class="card-title-content">{{ $friends_club_benefit_view->title }}</h4>
                      <p>{!! html_entity_decode($friends_club_benefit_view->description) !!}</p>
                    </div>
                  </div>
                @endforeach
              @else
                <div class="col-md-6">
                  <div class="custom-card text-center">
                    <div class="custom-card-img">
                      <img src="{{ url('assets/web/images/lingkaran.png') }} " alt="">
                    </div>
                    <h4 class="card-title-content">Lorem Ipsum</h4>
                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit</p>
                  </div>
                </div>
                <div class="col-md-6">
                  <div class="custom-card text-center">
                    <div class="custom-card-img">
                      <img src="{{ url('assets/web/images/lingkaran.png') }} " alt="">
                    </div>
                    <h4 class="card-title-content">Lorem Ipsum</h4>
                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit</p>
                  </div>
                </div>

                <div class="col-md-6">
                  <div class="custom-card text-center">
                    <div class="custom-card-img">
                      <img src="{{ url('assets/web/images/lingkaran.png') }} " alt="">
                    </div>
                    <h4 class="card-title-content">Lorem Ipsum</h4>
                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit</p>
                  </div>
                </div>
                <div class="col-md-6">
                  <div class="custom-card text-center">
                    <div class="custom-card-img">
                      <img src="{{ url('assets/web/images/lingkaran.png') }} " alt="">
                    </div>
                    <h4 class="card-title-content">Lorem Ipsum</h4>
                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit</p>
                  </div>
                </div>

                <div class="col-md-6">
                  <div class="custom-card text-center">
                    <div class="custom-card-img">
                      <img src="{{ url('assets/web/images/lingkaran.png') }} " alt="">
                    </div>
                    <h4 class="card-title-content">Lorem Ipsum</h4>
                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit</p>
                  </div>
                </div>
                <div class="col-md-6">
                  <div class="custom-card text-center">
                    <div class="custom-card-img">
                      <img src="{{ url('assets/web/images/lingkaran.png') }} " alt="">
                    </div>
                    <h4 class="card-title-content">Lorem Ipsum</h4>
                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit</p>
                  </div>
                </div>
              @endif
            </div>
          </div>
        </div>

      </div>
    </div>
  </div>
  <div class="overlay"></div>
</div>
@include('web.layouts.footer')



@include('web.layouts.script')

<script type="text/javascript">
  $(document).ready(function(){

    $("#id_label_kota").prop('disabled', true);
    $("#id_label_kec").prop('disabled', true);

    $("#no_telp").on("blur", function(){
      var mobNum = $(this).val();
      var filter = /^\d*(?:\.\d{1,2})?$/;
      var minLength = 10;
      var maxLength = 12;
      var value = $(this).val();

        if (filter.test(mobNum)) {
          if (value.length < minLength){
            alert('Please put min 10  digit mobile number');
            return false;
          }
          else if (value.length > maxLength){
            alert('Please put max 12  digit mobile number');
            return false;
          }
          else{

          }
        }
        else {
          alert('Not a valid number');
          return false;
      }
    });

    $('#id_label_prov').on('change', function() {
      var provinsi = $('#id_label_prov').val();

      $.ajax({
        type : "POST",
        url: '{{ url('/get-city') }}',
        data: {
          provinsi  : provinsi,

          cache: "false",
          _token: '{{ csrf_token() }}'
        },

        complete: function(datas) {
          $("#id_label_kec").prop('disabled', true);
          $("#id_label_kota").html(datas.responseText);
          $("#id_label_kota").prop('disabled', false);

        }
      });

    });

    $('#id_label_kota').on('change', function() {
      var city = $('#id_label_kota').val();
      console.log(city)

      $.ajax({
        type : "POST",
        url: '{{ url('/get-subdistrict') }}',
        data: {
          city  : city,

          cache: "false",
          _token: '{{ csrf_token() }}'
        },

        complete: function(datas) {
          $("#id_label_kec").html(datas.responseText);
          $("#id_label_kec").prop('disabled', false);
        }
      });

    });

  });
  $("form").each(function() {
    $(this).find(':input[type="submit"]').prop('disabled', true);
  });
  function correctCaptcha() {
  $("form").each(function() {
    $(this).find(':input[type="submit"]').prop('disabled', false);
  });
  }
  @if ($message = Session::get('success'))
    Swal.fire(
      'Sukses!',
      'Pendaftaran Berhasil Dikirim!',
      'success'
    )
  @endif
</script>
<script type="text/javascript">
  $(document).ready(function(){
    $("button.btn-reg-mobile").click(function() {
      $(".collapse-form-friend").show(500);
      $(".friendsClub_wrapper").hide(500);
    });
    $(".close_form_register").click(function() {
      $(".collapse-form-friend").hide(500);
      $(".friendsClub_wrapper").show(500);
    });
  });
</script>
</body>
</html>
