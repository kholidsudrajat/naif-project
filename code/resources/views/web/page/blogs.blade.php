<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <link rel="icon" href="{{ url('/assets/web/icons/logo.svg') }}" type="image/svg">

  <title> Article | Naif Indonesia </title>
  @include('web.layouts.style')

</head>

<body>
@include('web.layouts.header')
<div id="blogs">
  <div class="container">
    <div class="wrapper">
      <div class="content">
        <h1 class="title-page">Blogs</h1>
        <div id="blogs_post" class="row">
          @foreach( $article as $article_view )
            <div class="col-md-4 col-sm-6 col-xs-6 col-6">
              <div class="box-blogs">
                <div class="blogs-image">
                  <img src="{{ url('upload/article/'.$article_view->thumbnail) }}" class="img-fluid img-center" alt="Responsive image">
                </div>
                <div class="blogs-text blogs-limit">
                  <a href="{{ url('article/'.$article_view->slug) }}">
                    <h5 class="text-center">{{ $article_view->title }}</h5>
                  </a>
                  <!-- <div>{!!(Str::limit($article_view->description, 180, $end='...')) !!}</div> -->
                  <div class="truncate-text">{!! html_entity_decode($article_view->description) !!}</div>
                  <!-- <p class="text-center">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed imperdiet non elit et molestie. In non erat nec ipsum scelerisque feugiat. </p> -->

                  <div class="blogs-post-on">
                    <div class="d-flex">
                      <div class="mr-auto posted-on">Posted on:</div>
                      <div class="date-post">{{ date('d F Y', strtotime($article_view->created_at)) }}</div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          @endforeach
          
        </div>
        <nav class="pagination_blogs" aria-label="blogs navigation">
          <ul class="pagination justify-content-center">
            {{ $article->links() }}
          </ul>
        </nav>
      </div>
    </div>
    <div class="overlay"></div>
  </div>
</div>
@include('web.layouts.footer')

<script>
  (function(d, s, id) {
    var js, fjs = d.getElementsByTagName(s)[0];
    if (d.getElementById(id)) return;
    js = d.createElement(s); js.id = id;
    js.src = "https://connect.facebook.net/en_US/sdk.js#xfbml=1&version=v3.0";
    fjs.parentNode.insertBefore(js, fjs);
  }(document, 'script', 'facebook-jssdk'));
</script>
<script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>

@include('web.layouts.script')


</body>
</html>
