<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <link rel="icon" href="{{ url('/assets/web/icons/logo.svg') }}" type="image/svg">

  <title> About Us | Naif Indonesia </title>
  @include('web.layouts.style')

</head>

<body>
@include('web.layouts.header')
<div id="about-us">
  <div class="container">
    <div class="wrapper">
      <div class="content">
        <h1 class="title-page">About Us</h1>
        @foreach ($section1 as $section1_view)
          <div class="d-lg-block d-md-block d-sm-none d-none">
            <div class="about_content">
              <div class="media">
                <div class="media-body">
                    {!! html_entity_decode($section1_view->description) !!}
                </div>
                <img class="ml-3" src="{{ url('upload/about-us/'.$section1_view->image) }}" alt="Generic placeholder image">
              </div>
            </div>
          </div>
          <div class="d-lg-none d-md-none d-sm-block d-block">
            <div class="about_content">
              <div class="media">
                <img class="mr-3" src="{{ url('upload/about-us/'.$section1_view->image) }}" alt="Generic placeholder image">
                <div class="media-body">
                    {!! html_entity_decode($section1_view->description) !!}
                </div>
              </div>
            </div>
          </div>
        @endforeach

        @foreach ($section2 as $section2_view)
          <div class="box-about-image">
            <div class="about-image-inner">
              <div class="d-lg-block d-md-block d-sm-none d-none">
                <div class="about-image-text-outer-right">
                  <div class="about-image-text-inner">
                    <div class="about-image-text">
                      <h5 class="title_about_text">{{ $section2_view->title }}</h5>
                      {!! html_entity_decode($section2_view->description) !!}
                    </div>
                  </div>
                </div>
              </div>
              <div class="about-img-left">
                <img src="{{ url('upload/about-us/'.$section2_view->image) }}" class="img-fluid img-center" alt=" ">
              </div>
              <div class="d-lg-none d-md-none d-sm-block d-block">
                <div class="about-image-text-outer-bottom">
                  <div class="about-image-text-inner">
                    <div class="about-image-text">
                      <h5 class="title_about_text">{{ $section2_view->title }}</h5>
                      {!! html_entity_decode($section2_view->description) !!}
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        @endforeach

        @foreach ($section3 as $section3_view)
          <div class="box-about-image">
            <div class="about-image-inner">
              <div class="d-lg-block d-md-block d-sm-none d-none">
                <div class="about-image-text-outer">
                  <div class="about-image-text-inner">
                    <div class="about-image-text">
                      <h5 class="title_about_text">{{ $section3_view->title }}</h5>
                      {!! html_entity_decode($section3_view->description) !!}
                    </div>
                  </div>
                </div>
              </div>
              <div class="about-img-right">
                <img src="{{ url('upload/about-us/'.$section3_view->image) }}" class="img-fluid img-center" alt=" ">
              </div>
              <div class="d-lg-none d-md-none d-sm-block d-block">
                <div class="about-image-text-outer-bottom">
                  <div class="about-image-text-inner">
                    <div class="about-image-text">
                      <h5 class="title_about_text">{{ $section3_view->title }}</h5>
                      {!! html_entity_decode($section3_view->description) !!}
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        @endforeach

      </div>
    </div>
  </div>
</div>
@include('web.layouts.footer')

<script>
  (function(d, s, id) {
    var js, fjs = d.getElementsByTagName(s)[0];
    if (d.getElementById(id)) return;
    js = d.createElement(s); js.id = id;
    js.src = "https://connect.facebook.net/en_US/sdk.js#xfbml=1&version=v3.0";
    fjs.parentNode.insertBefore(js, fjs);
  }(document, 'script', 'facebook-jssdk'));
</script>
<script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>

@include('web.layouts.script')


</body>
</html>
