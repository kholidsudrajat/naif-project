<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <link rel="icon" href="{{ url('/assets/web/icons/logo.svg') }}" type="image/svg">
  <title>Contact Us | Naif Indonesia</title>
  <meta property="og:url"             content="{{ url('/contact-us') }}" />
  <meta property="og:type"            content="website" />
  @foreach ($banner as $banner_view)
    <meta property="og:title"       content="{{ $banner_view->title }} - Naif Indonesia">
    <meta property="og:description" content="{{ $banner_view->meta_tags }}">
    <meta property="og:image"       content="{{ url('upload/banner/'.$banner_view->image) }}" />
  @endforeach

  @include('web.layouts.style')
</head>
<body>
@include('web.layouts.header')
<div id="contactUs" class="box-wrapper">
  <div class="wrapper">
    <div class="content">
      <div class="container contact-us">
        <div class="contact-us-container">
          <div class="">
            <div class="row">
              <div class="col-md-6">
                <div class="main-banner-container">
                    @foreach ($profile as $profile_service)
                      <div class="main-banner banner-master">
                        <div class="main-banner-img">
                          <img src="{{ url('upload/profile/'.$profile_service->image) }}" class="img-fluid img-center img-desktop">
                        </div>
                      </div>
                    @endforeach

                </div>
              </div>
              <div class="col-md-6">
                <div class="page-container-outer">
                  <div class="page-counter-inner">
                    <div class="contactus-form-wrapper">

                      <div class="box-club-content form">
                        <div class="header-title-wrapper">
                          <h4>Any Question?</h4>
                          <p>We are here to help you.<br/>Hubungi kami atau isi formulir di bawah ini.</p>
                        </div>
                        <div class="club-content-inner">
                          <form action="{{ url('store-contact-us') }}" method="post" class="custom-form">
                            {{ csrf_field() }}
                            <div class="box-form">

                              <label>Name</label>
                              <div class="input-group">
                                {!! Form::text('name', null,['class' => 'form-control', 'placeholder'=>'Masukan nama', 'required'=>'required']) !!}
                              </div>

                              <label>Email</label>
                              <div class="input-group">
                                {!! Form::email('email', null,['class' => 'form-control', 'placeholder'=>'Masukan email', 'required'=>'required']) !!}
                              </div>

                              <label>Phone</label>
                              <div class="input-group">
                                {!! Form::text('no_telp', null,['class' => 'form-control', 'id'=>'no_telp', 'placeholder'=>'Masukan no telp anda', 'maxlength' => '13', 'required'=>'required']) !!}
                              </div>

                              <label>Address</label>
                              <div class="input-group">
                                {!! Form::textarea('address', null,['class' => 'form-control', 'placeholder'=>'Masukan alamat anda','required'=>'required', 'cols'=> '30', 'rows' => '3']) !!}
                              </div>

                              <label>Subject</label>
                              <div class="input-group">
                                {!! Form::text('subject', null,['class' => 'form-control', 'placeholder'=>'Masukan subject', 'required'=>'required']) !!}
                              </div>

                              <label>Message</label>
                              <div class="input-group">
                                {!! Form::textarea('description', null,['class' => 'form-control', 'placeholder'=>'Masukan deskripsi', 'cols' => '30', 'rows' => '8', 'required'=>'required']) !!}
                              </div>

                              <div class="input-group">
                                <div class="g-recaptcha" data-sitekey="6LdODsoaAAAAAOwKk1mGLl03rjlq6fY9odSe8JAJ" data-callback="correctCaptcha"></div>
                                <script type="text/javascript" src="https://www.google.com/recaptcha/api.js?hl=en"></script>
                              </div>

                              <div class="input-group">
                                <button type="submit" class="btn btn-info">SUBMIT</button>
                              </div>
                            </div>
                          </form>
                        </div>
                      </div>
                    </div>

                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="contactus-footer">
        <div class="container">
          <div class="text-center">
            <h3 class="contactus-title color-tosca">Contact Us</h3>
          </div>
          <div class="row justify-content-md-center">
            <div class="col-md-3">
              <div class="box-contact-content">
                @foreach ($profile as $profile_service)
                  <div class="club-content-inner width-100">
                    <div class="map-place">
                      <h4 class="color-tosca">Map</h4>
                      <div class="maps-wrapper">
                        <iframe src={{ $profile_service->map_embed }} width="100%" height="236" style="border:0;" allowfullscreen="" loading="lazy"></iframe>
                      </div>
                    </div>
                    <div class="group-address">
                      <h6 class="color-tosca">Address:</h6>
                      {!! html_entity_decode($profile_service->address) !!}
                    </div>
                  </div>
                @endforeach
              </div>
            </div>
              <div class="col-md-auto">
                <p style="opacity: 0">Variable width</p>
              </div>
            <div class="col-md-3">
              <div class="box-contact-content">
                @foreach ($profile as $profile_service)
                  <div class="club-content-inner width-100">
                    <div class="group-address">
                      <h4 class="color-tosca">Customer Services</h4>
                      <div class="img-service-wrapper">
                        <img src="{{ url('upload/profile/'.$profile_service->cs_image) }}">
                      </div>
                    </div>
                    <!-- <div class="input-group group-address width-100">
                      <h6 class="width-100">E-mail:</h6>
                      <p>{{ $profile_service->email }}</p>
                    </div> -->
                    <div class="group-address">
                      {!! html_entity_decode($profile_service->customer_service) !!}
                    </div>
                  </div>
                @endforeach
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="overlay"></div>
  </div>
</div>
@include('web.layouts.footer')



@include('web.layouts.script')
<script>

    $(document).ready(function(){
        $("#no_telp").on("blur", function(){
            var mobNum = $(this).val();
            var filter = /^\d*(?:\.\d{1,2})?$/;
            var minLength = 10;
            var maxLength = 12;
            var value = $(this).val();
                if (filter.test(mobNum)) {
                    if (value.length < minLength){
                        alert('Please put min 10  digit mobile number');
                        return false;
                    }
                    else if (value.length > maxLength){
                        alert('Please put max 12  digit mobile number');
                        return false;
                    }
                    else{

                    }
                }
                else {
                    alert('Not a valid number');
                    return false;
            }
        });
    });
    $("form").each(function() {
      $(this).find(':input[type="submit"]').prop('disabled', true);
    });
    function correctCaptcha() {
      $("form").each(function() {
          $(this).find(':input[type="submit"]').prop('disabled', false);
      });
    }
    @if ($message = Session::get('success'))
        Swal.fire(
            'Sukses!',
            'Terimakasih Anda telah menghubungi kami!',
            'success'
        )
    @endif

</script>

</body>
</html>
