<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <link rel="icon" href="{{ url('/assets/web/icons/logo.svg') }}" type="image/svg">
  <title>Ingredients | Naif Indonesia </title>
  <meta property="og:url" content="{{ url('/ingredients') }}" />
  <meta property="og:type" content="website" />
  @foreach ($banner as $banner_view)
    <meta property="og:title" content="{{ $banner_view->title }} - Naif Indonesia">
    <meta property="og:description" content="{{ $banner_view->meta_tags }}">
    <meta property="og:image" content="{{ url('upload/banner/'.$banner_view->image) }}" />
  @endforeach
  @include('web.layouts.style')

</head>
<body>

</body>