<script src=" {{ url('assets/web/js/jquery-3.5.1.min.js') }} "></script>

<script src=" {{ url('assets/web/js/popper.min.js')}} "></script>
<script src=" {{ url('assets/plugins/bootstrap/js/bootstrap.min.js') }} "></script>

<script src=" {{ url('assets/web/js/jquery-migrate-3.0.0.min.js') }} "></script>

<script src="https://unpkg.com/swiper/swiper-bundle.js"></script>
<script src="https://unpkg.com/swiper/swiper-bundle.min.js"></script>
<!-- Page specific javascripts-->
<script src=" {{ url('assets/plugins/js/sweetalert2@9.js') }} "></script>

<script src="{{ url('assets/plugins/select2/js/select2.min.js') }}"></script>
<script src="{{ url('assets/web/js/jquery.mCustomScrollbar.concat.min.js') }} "></script>

<!-- jquery ui -->
<script src=" {{ url('assets/plugins/jquery-ui/js/jquery-ui.js')}} "></script>
<script src=" {{ url('assets/plugins/jquery-autocomplete/js/jquery.mockjax.js')}} "></script>
<script src=" {{ url('assets/plugins/jquery-autocomplete/js/jquery.autocomplete.js')}} "></script>

<script src=" {{ url('assets/plugins/jquery-zoom/jquery.zoom.js')}} "></script>


<script>
  $(document).ready(function() {
    $('.js-example-basic-single').select2();
  });
</script>
<script type="text/javascript">
  $(document).ready(function () {
    $("#sidebar").mCustomScrollbar({
      theme: "minimal"
    });

    $('#dismiss, .overlay').on('click', function () {
      $('#sidebar').removeClass('active');
      $('.overlay').removeClass('active');
    });

    $('#sidebarCollapse').on('click', function () {
      $('#sidebar').addClass('active');
      $('.overlay').addClass('active');
      $('.collapse.in').toggleClass('in');
      $('a[aria-expanded=true]').attr('aria-expanded', 'false');
    });
  });
</script>
<script type="text/javascript">
  $(function () {
    var results = "";
    $.ajax({
      url: "{{ url('livesearch')}}",
      method: "GET",
      success: function(datas) {
        var articles = $.map(datas['articles'], function(item) {
          return {
            value: item.title, label: item.title, id: item.id, data: { category: 'Articles' }
          };
        });

        var products = $.map(datas['products'], function(item) {
          return {
            value: item.name, label: item.name, id: item.id, data: { category: 'Products' }
          };
        });

        results = products.concat(articles);

        $('#search').devbridgeAutocomplete ({
          lookup: results,
          minChars: 2,
          onSelect: function(suggestion) {
            console.log('You selected: '+ suggestion.id + ', ' + suggestion.value + ', ' + suggestion.data.category);
          },
          showNoSuggestionNotice: true,
          noSuggestionNotice: 'Sorry, no matching results',
          groupBy: 'category'
        });
        $('#search-mobile').devbridgeAutocomplete ({
          lookup: results,
          minChars: 2,
          onSelect: function(suggestion) {
            console.log('You selected: '+ suggestion.id + ', ' + suggestion.value + ', ' + suggestion.data.category);
          },
          showNoSuggestionNotice: true,
          noSuggestionNotice: 'Sorry, no matching results',
          groupBy: 'category'
        });
      }
    });
  });
  // Input number only
  // Restricts input for each element in the set of matched elements to the given inputFilter.
  (function($) {
    $.fn.inputFilter = function(inputFilter) {
      return this.on("input keydown keyup mousedown mouseup select contextmenu drop", function() {
        if (inputFilter(this.value)) {
          this.oldValue = this.value;
          this.oldSelectionStart = this.selectionStart;
          this.oldSelectionEnd = this.selectionEnd;
        } else if (this.hasOwnProperty("oldValue")) {
          this.value = this.oldValue;
          this.setSelectionRange(this.oldSelectionStart, this.oldSelectionEnd);
        } else {
          this.value = "";
        }
      });
    };
  }(jQuery));
  // Install input filters.
  $("#no_telp").inputFilter(function(value) {
    return /^-?\d*$/.test(value);
  });
  $("#pos_code").inputFilter(function(value) {
    return /^-?\d*$/.test(value);
  });
</script>
