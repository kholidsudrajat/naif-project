<div class="page-container footer-container">
  <div class="page-container-outer">
    <div class="page-counter-inner container">
      <div class="global-content-inner row">
        <div class="column-footer-logo col-md-3">
          <a href={{ url('/') }}>
            <img src=" {{ url('assets/web/icons/logo.svg') }} " class="img-fluid " alt="Responsive image">
          </a>
          <!-- <p>©2021 naif indonesia. All rights reserved</p> -->
        </div>
        @foreach ($profile as $profile_footer)
          <div class="column-media col-md-3 col-sm-6 col-6">
            <h6>Follow us</h6>
            <div class="media-icon">
              <a href="{{ $profile_footer->sosmed_fb }}">
                <img src=" {{ url('assets/web/icons/fb.png') }} " class="img-fluid" alt="Responsive image">
                <span class="media-icon-name">Facebook</span>
              </a>
            </div>

            <div class="media-icon">
              <a href="{{ $profile_footer->sosmed_ig }}">
                <img src=" {{ url('assets/web/icons/instagram.svg') }} " class="img-fluid" alt="Responsive image">
                <span class="media-icon-name">naifbaby_id</span>
              </a>
            </div>

            <div class="media-icon">
              <a href="{{ $profile_footer->sosmed_ig_2 }}">
                <img src=" {{ url('assets/web/icons/instagram.svg') }} " class="img-fluid" alt="Responsive image">
                <span class="media-icon-name">naifcare_id</span>
              </a>
            </div>
            
            <div class="media-icon">
              <a href="{{ $profile_footer->sosmed_twitter }}">
                <img src=" {{ url('assets/web/icons/yt.svg') }} " class="img-fluid" alt="Responsive image">
                <span class="media-icon-name">Youtube</span>
              </a>
            </div>
          </div>
        @endforeach
        <div class="column-footer-menu col-md-3 col-sm-6 col-6">
          <h6>Information</h6>
          <ul>
            <li><a href={{ url('about-us') }}>About us</a></li>
            <li><a href={{ url('blogs') }}>Blogs</a></li>
            <li><a href={{ url('faq') }}>FAQ</a></li>
            <li><a href={{ url('what-do-our-logo-mean') }}>What do our  logo’s mean?</a></li>
          </ul>
        </div>
        @foreach ($profile as $profile_footer)
          <div class="column-footer-care col-md-3">
            <h6>Customer care</h6>
            <p class="email_cs">{!! html_entity_decode($profile_footer->email) !!}</p>
            {!! html_entity_decode($profile_footer->address) !!}

            <!-- <p class="care-copy">©2021 naif indonesia. All rights reserved</p> -->
          </div>
        @endforeach
      </div>
    </div>
  </div>
</div>
