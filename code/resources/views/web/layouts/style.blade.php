<link rel="stylesheet" href="{{ url('assets/web/css/fonts.css') }}">
<link rel="stylesheet" href="{{ url('assets/plugins/bootstrap/css/bootstrap.css') }}">
<link rel="stylesheet" href="{{ url('assets/plugins/swiper/swiper.css') }} ">

<!-- jquery ui -->
<link rel="stylesheet" href="{{ url('assets/plugins/jquery-ui/css/jquery-ui.css') }}">
<!-- jquery autocomplete -->
<link rel="stylesheet" href="{{ url('assets/plugins/jquery-autocomplete/css/styles.css') }}">

<link href="{{ url('assets/plugins/select2/css/select2.min.css') }}" rel="stylesheet" />
<!-- Our Custom CSS -->
<link rel="stylesheet" href="{{ url('assets/web/css/style-sidebar.css') }}">
<!-- Scrollbar Custom CSS -->
<link rel="stylesheet" href="{{ url('assets/web/css/jquery.mCustomScrollbar.min.css') }}">

<script src="https://www.google.com/recaptcha/api.js" async defer></script>

<link rel="stylesheet" href="{{ url('assets/web/css/style-desktop.css?v=1.3-20200219-1') }}">
<link rel="stylesheet" href="{{ url('assets/web/css/style-tablet.css?v=1.2-20200219-1') }}">
<link rel="stylesheet" href="{{ url('assets/web/css/style-mobile.css?v=1.2-20200219-1') }}">
