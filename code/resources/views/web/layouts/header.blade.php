<nav id="sidebar">
  <div id="dismiss">
    <span class="icon-clear"></span>
  </div>

  <!-- <div class="sidebar-header">
    <div class="box-header-logo flex-between">
      <div class="box-language">
        <img src=" {{ url('assets/web/icons/language-id.svg') }} " alt="" class="img-fluid">
      </div>
      <div class="box-search">
        <div class="input-group">

        </div>
        <form action=" {{ url('result') }} " method="get">
          <div class="input-group">
            <button class="btn btn-search" type="submit" id="button-addon2">
              <img src=" {{ url('assets/web/icons/mirror-search.svg') }} ">
            </button>
            <input type="text" name="search" id="search-mobile" class="form-control" placeholder="Search" aria-label="Search" aria-describedby="button-addon2">
          </div>
        </form>
      </div>
    </div>
  </div> -->

  <ul class="list-unstyled components">
    <li>
      <a href={{ url('/') }}>Home</a>
    </li>
    @if (count($url_menu) >0)
      @foreach ($url_menu as $url_menu_view)
        <li class="{{ (Request::segment(1) == $url_menu_view->name) ? 'active-menu' : '' }}">
          <a href={{ url($url_menu_view->url) }}>{{ $url_menu_view->name }}</a>
        </li>
      @endforeach
    @endif
    <li class="{{ (Request::segment(1) == 'ingredients') ? 'active-menu' : '' }}">
      <a href={{ url('ingredients') }}>INGREDIENTS</a>
    </li>
    <li class="{{ (Request::segment(1) == 'friends-club') ? 'active-menu' : '' }}">
      <a href={{ url('friends-club') }}>Friends Club</a>
    </li>
    <li class="{{ (Request::segment(1) == 'contact-us') ? 'active-menu' : '' }}">
      <a href={{ url('contact-us') }}>Contact Us</a>
    </li>
  </ul>
</nav>

<div class="header-container">
  <div class="header-container-inner">
    <div class="box-header-logo">
      <div class="box-sidebar-toggle">
        <button type="button" id="sidebarCollapse" class="btn">
          <img src=" {{ url('assets/web/icons/burger.svg') }} " alt="" class="img-fluid">
        </button>
      </div>
      <div class="d-sm-block d-block d-md-none d-lg-none">
        <div class="box-logo">
          <a href={{ url('/') }}>
            <img src=" {{ url('assets/web/icons/logo.svg') }} " alt="" class="img-fluid">
          </a>
        </div>
        
        <div class="box-language">
          <button class="btn-search">
            <a data-toggle="collapse" href="#box-search" role="button" aria-expanded="false" aria-controls="box-search">
              <img src=" {{ url('assets/web/icons/mirror-search.svg') }} ">
            </a>
          </button>
          <img src=" {{ url('assets/web/icons/language-id.svg') }} " alt="" class="img-fluid">
        </div>
      </div>
      <div class="d-md-block d-lg-block d-sm-none d-none">
        <div class="box-search">
          <form action=" {{ url('result') }} " method="get">
            <div class="input-group">
              <button class="btn btn-search" type="submit" id="button-addon2">
                <img src=" {{ url('assets/web/icons/mirror-search.svg') }} ">
              </button>
              <input type="text" name="search" id="search" value="{{ $value_search }}" class="form-control" placeholder="Search" aria-label="Search" aria-describedby="button-addon2">
            </div>
          </form>
        </div>
        <a href={{ url('/') }}>
          <div class="box-logo">
            <img src=" {{ url('assets/web/icons/logo.svg') }} " alt="" class="img-fluid">
          </div>
        </a>
        <div class="box-language">
          <img src=" {{ url('assets/web/icons/language-id.svg') }} " alt="" class="img-fluid">
        </div>
      </div>
    </div>
    <div class="box-header-menu">
      <div class="btn-group btn-group-outer">
        <a href={{ url('/') }}>
          <button type="button" class="btn btn-info">Home</button>
        </a>
        @if (count($url_menu) >0)
          @foreach ($url_menu as $url_menu_view)
          <a href={{ url($url_menu_view->url) }} class="{{ (Request::segment(1) == $url_menu_view->name) ? 'active-menu' : '' }}">
            <button type="button" class="btn btn-info">{{ $url_menu_view->name }}</button>
          </a>
          @endforeach
        @endif
        <a href={{ url('ingredients') }} class="{{ (Request::segment(1) == 'ingredients') ? 'active-menu' : '' }}">
          <button type="button" class="btn btn-info">INGREDIENTS</button>
        </a>
        <a href={{ url('friends-club') }} class="{{ (Request::segment(1) == 'friends-club') ? 'active-menu' : '' }}">
          <button type="button" class="btn btn-info">Friends Club</button>
        </a>
        <a href={{ url('contact-us') }} class="{{ (Request::segment(1) == 'contact-us') ? 'active-menu' : '' }}">
          <button type="button" class="btn btn-info">Contact Us</button>
        </a>
      </div>
    </div>
  </div>
</div>
<div class="collapse" id="box-search">
  <div class="panels-search">
    <div class="tab-pane">
      <div class="box-search">
        <form action=" {{ url('result') }} " method="get">
          <div class="input-group">
            <button class="btn btn-search" type="submit" id="button-addon2">
              <img src=" {{ url('assets/web/icons/mirror-search.svg') }} ">
            </button>
            <input type="text" name="search" id="search" value="{{ $value_search }}" class="form-control" placeholder="Search" aria-label="Search" aria-describedby="button-addon2">
            <button class="btn-close-search">
              <a data-toggle="collapse" href="#box-search" role="button" aria-expanded="false" aria-controls="box-search">
                <span class="icon-clear"></span>
              </a>
            </button>
          </div>
        </form>
      </div>
    </div>
  </div>
</div>
