<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Friends_Club_Benefit extends Model
{
   protected $table = 'friends_clubs_benefit';
   protected $fillable = [
       'title',
       'description',
       'image',
       'link',
       'image_orientation',
   ];
}
