<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Mom_Story;

class MomStory extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'mom:story';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $result = [];

        $playlistId = 'PL7vdwX11yNOh9N-89TGVL0ItZtCwzUTRT';
        $maxResults = '50';

        $api_url = 'https://www.googleapis.com/youtube/v3/playlistItems?part=snippet&key=AIzaSyB6Nw5sAh6FjxRnTayqKvRgT0dSjS2W9_A&playlistId='.$playlistId.'&maxResults='.$maxResults;


        $sc = curl_init();
        curl_setopt($sc, CURLOPT_URL, $api_url);
        curl_setopt($sc, CURLOPT_RETURNTRANSFER, 1);
        $data = curl_exec($sc);
        curl_close($sc);

        $html= '';
        $datas = json_decode($data, true);

        foreach($datas["items"] as $item){
            $views = $this->getViewCount($item["snippet"]["resourceId"]["videoId"]);

            foreach($views["items"] as $view){
                $result[] = [
                    "id" => $item["id"],
                    "title" => $item["snippet"]["title"],
                    "description" => $item["snippet"]["description"],
                    "statistic_videos" => $view["statistics"],
                    "resourceId" =>$item["snippet"]["resourceId"]["videoId"],
                    "published_at" => $item["snippet"]["publishedAt"],
                    "thumbnail" => [
                        $item["snippet"]["thumbnails"]
                    ],

                ];
            }
        }


        $requestData = array();
        foreach($result as $key=>$value){
            $requestData[$key]['video_id'] = $value['resourceId'];
            $requestData[$key]['title'] = $value['title'];
            $requestData[$key]['description'] = $value['description'];
            $requestData[$key]['viewer'] = $value['statistic_videos']['viewCount'];
            $requestData[$key]['published_at'] = $value['published_at'];
            // print_r($value);
            foreach ($value['thumbnail'] as $img_video){
                $requestData[$key]['thumbnail'] = $img_video['medium']['url'];
            }

            if (Mom_Story::where('video_id', '=', $requestData[$key]['video_id'])->exists()) {
                echo(" video updated ");

                $user = Mom_Story::firstOrNew(array('video_id' => $requestData[$key]['video_id']));
                $user->title = $requestData[$key]['title'];
                $user->description = $requestData[$key]['description'];
                $user->thumbnail = $requestData[$key]['thumbnail'];
                $user->viewer = $requestData[$key]['viewer'];
                $user->update();
             }
             else
             {
                echo(" video saved ");

                $user = Mom_Story::firstOrNew(array('video_id' => $requestData[$key]['video_id']));
                $user->title = $requestData[$key]['title'];
                $user->description = $requestData[$key]['description'];
                $user->thumbnail = $requestData[$key]['thumbnail'];
                $user->video_id = $requestData[$key]['video_id'];
                $user->viewer = $requestData[$key]['viewer'];
                $user->published_at = $requestData[$key]['published_at'];
                $user->save();

            }

        }
    }
    private function getViewCount($video_id){
        $api_viewCount ='https://www.googleapis.com/youtube/v3/videos?part=statistics&key=AIzaSyB6Nw5sAh6FjxRnTayqKvRgT0dSjS2W9_A&id='.$video_id;

        $sc = curl_init();
        curl_setopt($sc, CURLOPT_URL, $api_viewCount);
        curl_setopt($sc, CURLOPT_RETURNTRANSFER, 1);
        $data = curl_exec($sc);
        curl_close($sc);

        $datas = json_decode($data, true);

       return $datas;
    }
}
