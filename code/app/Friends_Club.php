<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Friends_Club extends Model
{
    protected $table = 'friends_clubs';
    protected $fillable = [
        'name',
        'email',
        'address',
        'instagram',
        'provinsi',
        'kota',
        'kecamatan',
        'pos_code',
        'no_telp',
        'status',
    ];
}
