<?php

namespace App\Exports;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;
use DB;

class ContactUsExport implements FromView
{
    private $startDate, $endDate;

    public function startDate(string $startDate)
    {
        $this->startDate = $startDate;
    }
    public function endDate(string $endDate)
    {
        $this->endDate = $endDate;
    }

    public function view(): View
    {
        if(is_null($this->startDate) && is_null($this->endDate))
        {
            $contact_us = DB::table('contact_us')
            ->orderBy('created_at', 'desc')
            ->get();
        }
        else
        {
            $contact_us = DB::table('contact_us')
            ->whereBetween('created_at', [$this->startDate, $this->endDate])
            ->orderBy('created_at', 'desc')
            ->get();
        }

        return view('admin.forms.contact-us.contact-us-excel', ['contact_us' => $contact_us]);

    }
}
