<?php

namespace App\Exports;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;
use DB;


class FriendsClubExport implements FromView
{
    private $startDate, $endDate;

    public function startDate(string $startDate)
    {
        $this->startDate = $startDate;
    }
    public function endDate(string $endDate)
    {
        $this->endDate = $endDate;
    }

    public function view(): View
    {
        if(is_null($this->startDate) && is_null($this->endDate))
        {
            $friends_club = DB::table('friends_clubs')
            ->join('provinces', 'provinces.id', '=', 'friends_clubs.provinsi')
            // ->join('cities', 'cities.id', '=', 'friends_clubs.kota')
            // ->join('subdistrict', 'subdistrict.id', '=', 'friends_clubs.kecamatan')
            ->select('friends_clubs.*', 'provinces.provinsi')
            ->orderBy('friends_clubs.created_at', 'DESC')
            ->get();
        }
        else
        {
            $friends_club = DB::table('friends_clubs')
            ->join('provinces', 'provinces.id', '=', 'friends_clubs.provinsi')
            // ->join('cities', 'cities.id', '=', 'friends_clubs.kota')
            // ->join('subdistrict', 'subdistrict.id', '=', 'friends_clubs.kecamatan')
            ->select('friends_clubs.*', 'provinces.provinsi')
            ->whereBetween('friends_clubs.created_at', [$this->startDate, $this->endDate])
            ->orderBy('friends_clubs.created_at', 'DESC')
            ->get();


        }

        return view('admin.forms.friends-club.friends-club-excel', ['friends_club' => $friends_club]);

    }
}
