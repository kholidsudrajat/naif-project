<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class What_Do_Our_Logo_Mean extends Model
{
   protected $table = 'what_do_our_logo_mean';
   protected $fillable = [
       'section',
       'title',
       'description',
       'image',
       'image_orientation',
   ];
}
