<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Ingredients extends Model
{
   protected $table = 'ingredients';
   protected $fillable = [
       'title',
       'description',
       'image',
       'link',
       'image_orientation',
   ];
}
