<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Profile extends Model
{
    protected $table = 'profiles';
    protected $fillable = [
        'title',
        'sup_description',
        'description',
        'image',
        'address',
        'customer_service',
        'cs_image',
        'map_embed',
        'email',
        'no_telp',
        'sosmed_fb',
        'sosmed_twitter',
        'sosmed_ig',
        'sosmed_ig_2',
        'meta_tags',
    ];
}
