<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Faq_Naif_Care extends Model
{
   protected $table = 'faq_naif_care';
   protected $fillable = [
       'question',
       'answer',
   ];
}
