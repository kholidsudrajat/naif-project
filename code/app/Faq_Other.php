<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Faq_Other extends Model
{
   protected $table = 'faq_other';
   protected $fillable = [
       'question',
       'answer',
   ];
}
