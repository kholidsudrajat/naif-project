<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Url_Menu extends Model
{
   protected $table = 'url_menu';
   protected $fillable = [
       'name',
       'url',
   ];
}
