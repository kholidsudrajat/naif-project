<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Faq_Naif_Baby extends Model
{
   protected $table = 'faq_naif_baby';
   protected $fillable = [
       'question',
       'answer',
   ];
}
