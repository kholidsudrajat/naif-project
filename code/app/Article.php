<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Article extends Model
{
    protected $table = 'articles';
    protected $fillable = [
        'title',
        'description',
        'image',
        'thumbnail',
        'share_fb',
        'share_twitter',
        'share_ig',
        'slug',
        'meta_tags',
    ];
}
