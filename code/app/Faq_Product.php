<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Faq_Product extends Model
{
    protected $table = 'faq_products';
    protected $fillable =  [
        'name',
        'question',
        'answer',
    ];
}
