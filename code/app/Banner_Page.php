<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Banner_Page extends Model
{
    protected $table = 'banner_pages';
    protected $fillable = [
        'banner_category',
        'sup_title',
        'title',
        'description',
        'image',
        'url_page',
        'meta_tags',
    ];
}
