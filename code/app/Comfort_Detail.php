<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Comfort_Detail extends Model
{
    protected $table = 'comfort_details';
    protected $fillable = [
        'name',
        'image',
        'image_thumbnail',
    ];
}
