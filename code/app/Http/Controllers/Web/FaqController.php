<?php

namespace App\Http\Controllers\Web;
use App\Http\Controllers\Controller;


use Illuminate\Http\Request;
use DB;
use App\Faq_Naif_Baby;
use App\Faq_Naif_Care;
use App\Faq_Other;

use App\Profile;
use App\Url_Menu;

class FaqController extends Controller
{

    public function index(Request $request)
    {
        $key = $request->search;
        $value_search = $key;
        $profile = Profile::all();
        $url_menu = Url_Menu::all();
        $banner = DB::select("SELECT * FROM banner_pages WHERE banner_category ='5' ORDER BY created_at DESC LIMIT 1");


        $faq_naif_baby = Faq_Naif_Baby::all();
        $faq_naif_care = Faq_Naif_Care::all();
        $faq_other = Faq_Other::all();
        return view('web.page.faq', compact('profile', 'url_menu', 'banner', 'value_search', 'faq_naif_baby', 'faq_naif_care', 'faq_other'));
    }


}
