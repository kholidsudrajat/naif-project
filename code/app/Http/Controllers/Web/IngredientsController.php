<?php

namespace App\Http\Controllers\Web;
use App\Http\Controllers\Controller;


use Illuminate\Http\Request;
use DB;
use App\Profile;
use App\Ingredients;
use App\Url_Menu;

class IngredientsController extends Controller
{

    public function index(Request $request)
    {

        $slider_testimonial = DB::table('ingredients')
                                            ->orderBy('id', 'DESC')
                                            ->get();
        $key = $request->search;
        $value_search = $key;
        $profile = Profile::all();
        $url_menu = Url_Menu::all();
        $banner = DB::select("SELECT * FROM banner_pages WHERE banner_category ='6' ORDER BY created_at DESC LIMIT 1");
        return view('web.page.ingredients', compact('profile', 'url_menu', 'value_search', 'banner', 'slider_testimonial'));
    }


    function load_data(Request $request)
    {
        $output = '';
        $last_id = '';

        if($request->ajax()){
            if($request->id > 0){
                $data = DB::table('ingredients')
                            ->where('id', '<', $request->id)
                            ->orderBy('id', 'DESC')
                            
                            ->get();
            }
            else
            {
                $data = DB::table('ingredients')
                            ->orderBy('id', 'DESC')
                            
                            ->get();
            }

            if(!$data->isEmpty())
            {
                foreach($data as $row)
                {
                    $output .= '
                    <div class="col-md-6 col-sm-6 col-6">
                        <div class="custom-card text-center">
                            <div class="custom-card-img">
                                <img src="'.url('upload/ingredients/'.$row->image).'" alt="">
                            </div>
                            <h4 class="card-title-content">'.$row->title.'</h4>
                            '.html_entity_decode($row->description).'
                        </div>
                    </div>';

                    $last_id = $row->id;
                }

                // $output .= '
                //     <div class="box-show-more width-100" id="load_more_button" data-id="'.$last_id.'">
                //         <div class="show-more-inner" >
                //             <h5>Tampilkan lebih banyak</h5>
                //             <img src="assets/web/icons/arrow-bottom.svg" alt="" class="img-fluid">
                //         </div>
                //     </div>';
            }
            else
            {
                // $output .= '
                //     <div id="load_more" class="box-show-more width-100">
                //         <div class="show-more-inner">
                //             <h5>Data Tidak ditemukan</h5>
                //             <img src="assets/web/icons/arrow-bottom.svg" alt="" class="img-fluid">
                //         </div>
                //     </div>';
            }
            echo $output;
        }
    }

    public function load_data_detail(Request $request){
        $html = '';

        $content = DB::table('ingredients')
                ->where('id', $request->id)
                ->first();

        $html = '<div class="box-content-view-inner width-100">';
            $html .=  '<div class="content-view-image flex-wrap-center">';

                if ($content->image_orientation == 'P') {
                    $html .= '<div class="potrait width-50">';
                        $html .= '<img class="img-fluid img-center" src="'.url('upload/ingredients/'.$content->image).'" class="img-center">';
                    $html .= '</div>';
                } else {
                    $html .= '<div class="landscape width-100">';
                        $html .= '<img class="img-fluid img-center" src="'.url('upload/ingredients/'.$content->image).'" class="img-center">';
                    $html .= '</div>';
                }
            $html .= '</div>';
            $html .= '<div class="content-view-description">';
                    $html .= '<h4>'. $content->title . '</h4>';
                    $html .= html_entity_decode($content->description);
            $html .= '</div>';
        $html .= '</div>';

        return $html;
    }
    public function get_maxId(){
        $getMax = Ingredients::max('id');
        return $getMax;
    }
    public function get_minId(){
        $getMin = Ingredients::min('id');
        return $getMin;
    }
}
