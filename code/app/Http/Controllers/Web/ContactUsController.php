<?php

namespace App\Http\Controllers\Web;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use DB;
use App\Profile;
use App\Contact_Us;
use App\Url_Menu;


class ContactUsController extends Controller
{

    public function index(Request $request)
    {
        $key = $request->search;
        $value_search = $key;
        $profile = Profile::all();
        $url_menu = Url_Menu::all();
        $banner = DB::select("SELECT * FROM banner_pages WHERE banner_category ='9' ORDER BY created_at DESC LIMIT 1");
        return view('web.page.contact-us', compact('profile', 'url_menu', 'value_search','banner'));
    }
    public function store_contact_us(Request $request)
    {



        $requestData = $request->all();


        Contact_Us::create($requestData);


        // $emails = ['info@gcindonesia.com'];
        // $company = $request->first_name;
        // Mail::send('email_contact_us',
        // [
        //     'subject'           => $request->subject,
        //     'first_name'        => $request->first_name,
        //     'email'             => $request->email,
        //     'address'           => $request->address,
        //     'no_telp'           => $request->no_telp,
        //     'description'       => $request->description

        // ],

        // function($message) use ($emails, $company)
        // {
        //     $message->to($emails);
        //     $message->subject('Message from '.$company);
        //     $message->from('naifid@gmail.com', 'Admin Naif  Contact Us');
        // });

        return back()->with(['success' => 'Email Send Success']);
    }


}
