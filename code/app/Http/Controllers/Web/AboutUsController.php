<?php

namespace App\Http\Controllers\Web;
use App\Http\Controllers\Controller;


use Illuminate\Http\Request;
use DB;
use App\About_Us;

use App\Profile;
use App\Url_Menu;

class AboutUsController extends Controller
{

    public function index(Request $request)
    {
        $key = $request->search;
        $value_search = $key;
        $profile = Profile::all();
        $url_menu = Url_Menu::all();
        $section1 = DB::select("SELECT * FROM about_us WHERE section = 1");
        $section2 = DB::select("SELECT * FROM about_us WHERE section = 2");
        $section3 = DB::select("SELECT * FROM about_us WHERE section = 3");
        return view('web.page.about-us', compact('profile', 'url_menu', 'value_search', 'section1', 'section2', 'section3'));
    }


}
