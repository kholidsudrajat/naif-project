<?php

namespace App\Http\Controllers\Web;

use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use DB;
use App\Profile;
use App\Master_Product;
use App\Product_Details;
use App\Url_Menu;


class SearchResultController extends Controller
{

    public function index()
    {
        $profile = Profile::all();
        $url_menu = Url_Menu::all();
        // $product_data = DB::select("SELECT mp.id, pc.category, mp.name, pd.image, pd.image_thumbnail, mp.slug
        //             FROM master_products mp
        //             INNER JOIN product_categories pc ON (mp.category = pc.id)
        //             INNER JOIN product_details pd ON (mp.id = pd.name)
        //             WHERE pd.image_thumbnail ='1'
        //             GROUP BY mp.id
        //             ORDER BY mp.created_at DESC");

        return view('web.page.search-result', compact('profile', 'url_menu'));
    }

    public function search_data(Request $request){

        $profile = Profile::all();
        $url_menu = Url_Menu::all();
        // menangkap data key dari inputan search
        $key = $request->search;
        $value_search = $key;

        $article = DB::select("SELECT * FROM articles WHERE title LIKE '%$key%' OR  description LIKE '%$key%' ");

        $ingredients = DB::select("SELECT * FROM ingredients WHERE title LIKE '%$key%' OR  description LIKE '%$key%' ");

        return view('web.page.search-result', compact('profile', 'url_menu', 'value_search', 'ingredients', 'article'));
    }


    Public function livesearch(Request $request){

        $data = array();

        $articles = DB::select("SELECT * FROM articles");

        $ingredients = DB::select("SELECT * FROM ingredients");

        $data['products'] = $products;
        $data['ingredients'] = $ingredients;

        return response()->json($data);
    }


}
