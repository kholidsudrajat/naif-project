<?php

namespace App\Http\Controllers\Web;
use App\Http\Controllers\Controller;


use Illuminate\Http\Request;
use DB;
use App\What_Do_Our_Logo_Mean;

use App\Profile;
use App\Url_Menu;

class WhatDoOurLogoMeanController extends Controller
{

    public function index(Request $request)
    {
        $key = $request->search;
        $value_search = $key;
        $profile = Profile::all();
        $url_menu = Url_Menu::all();
        $banner = DB::select("SELECT * FROM banner_pages WHERE banner_category ='3' ORDER BY created_at DESC LIMIT 1");

        $section1 = DB::select("SELECT * FROM what_do_our_logo_mean WHERE section = 1");
        $section2 = DB::select("SELECT * FROM what_do_our_logo_mean WHERE section = 2");
        $section3 = DB::select("SELECT * FROM what_do_our_logo_mean WHERE section = 3");
        return view('web.page.what-do-our-logo-mean', compact('profile', 'url_menu', 'banner', 'value_search', 'section1', 'section2', 'section3'));
    }


}
