<?php

namespace App\Http\Controllers\Web;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use DB;
use App\Friends_Club;

use App\Profile;
use App\Province;
use App\City;
use App\Url_Menu;

class FriendsClubController extends Controller
{

    public function index(Request $request)
    {
        $key = $request->search;
        $value_search = $key;
        $profile = Profile::all();
        $url_menu = Url_Menu::all();
        $banner = DB::select("SELECT * FROM banner_pages WHERE banner_category ='8' ORDER BY created_at DESC LIMIT 1");
        $friends_club_benefit = DB::table('friends_clubs_benefit')
                            ->orderBy('id', 'DESC')
                            ->get();
        $province_data = Province::pluck('provinsi', 'id')->prepend('- Provinsi -', '');
        return view('web.page.friends-club', compact('profile', 'url_menu', 'value_search', 'banner', 'friends_club_benefit', 'province_data'));
    }
    public function get_city(Request $request)
    {
        $provinsi   = $request->provinsi;

        $html = '';

            // $city_datas = DB::select("SELECT * FROM cities WHERE province_id='$provinsi' ORDER BY city_name ASC");
            $city_datas = DB::select("SELECT * FROM cities WHERE province_id='$provinsi' GROUP BY city_name ORDER BY city_name ASC");
            $html .= '<option value="">- Kota/Kabupaten -</option>';
            foreach($city_datas as $city_data_view)
            {
                $html .= '<option value="'.$city_data_view->city_name.'" data-city="'.$city_data_view->city_name.'">'.$city_data_view->city_name.'</option>';

            }
            return $html;
    }
    public function get_subdistrict(Request $request)
    {
    $city   = $request->city;

    $html = '';

        // $subdistrict_datas = DB::select("SELECT * FROM subdistrict WHERE city_id='$city' ORDER BY subdistrict_name ASC");
        $subdistrict_datas = DB::select("SELECT sub.id, ct.city_name, sub.type, sub.subdistrict_name
                                FROM subdistrict sub
                                INNER JOIN cities ct ON (sub.city_id = ct.id)
                                WHERE ct.city_name ='$city' ORDER BY sub.type, sub.subdistrict_name ASC");
        $html .= '<option value="">- Kecamatan -</option>';
        foreach($subdistrict_datas as $subdistrict_data_view)
        {
            $html .= '<option value="'.$subdistrict_data_view->subdistrict_name.'">'.$subdistrict_data_view->subdistrict_name.'</option>';

        }
        return $html;
    }

    public function store_friendsClub(Request $request)
    {
        $requestData = $request->all();

        Friends_Club::create($requestData);


        // $emails = ['naif.friendsclub@gcindonesia.com'];
        // $company = $request->name;
        // Mail::send('email_friends',
        // [
        //     'name'              => $request->name,
        //     'email'             => $request->email,
        //     'address'           => $request->address,
        //     'instagram'         => $request->instagram,
        //     'no_telp'           => $request->no_telp,
        //     'information_from'  => $request->information_from

        // ],

        // function($message) use ($emails, $company)
        // {
        //     $message->to($emails);
        //     $message->subject('Message from '.$company);
        //     $message->from('naifid@gmail.com', 'Admin Naif Friends Club');
        // });

        return back()->with(['success' => 'Email Send Success']);
    }


}
