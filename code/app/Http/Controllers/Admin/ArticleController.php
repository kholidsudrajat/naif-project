<?php

namespace App\Http\Controllers\Admin;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use Illuminate\Support\Str;
use App\Article;
use File;
use DB;

class ArticleController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        $this->path= 'upload/article';
    }

    public function index()
    {
        $title = 'Article List';
        $article = DB::select("SELECT * FROM articles ORDER BY created_at DESC");
        return view('admin.forms.article.index', compact('title', 'article'));
    }


    public function create()
    {
        $title= 'Add Article Data';
        return view('admin.forms.article.create', compact('title'));
    }


    public function store(Request $request)
    {
        $request->validate([
            'images'     => 'required|mimes:jpeg,jpg,png',

        ]);
        $request->validate([
            'thumbnail'     => 'required|mimes:jpeg,jpg,png',

        ]);

        $file_image           = $request['images'];
        $file_thumbnail           = $request['thumbnails'];

        if ($file_image != "") {

            $name_image = date('YmdHis') .'_'. $file_image->getClientOriginalName();
            $file_image->move($this->path, $name_image);
            $request['image'] = $name_image;


        }
        if ($file_thumbnail != "") {

            $name_thumbnail = date('YmdHis') .'_thumbnail_'. $file_thumbnail->getClientOriginalName();
            $file_thumbnail->move($this->path, $name_thumbnail);
            $request['thumbnail'] = $name_thumbnail;

        }

        $request['slug'] = Str::slug($request['title'], '-');
        $requestData = $request->all();
        Article::create($requestData);
        return redirect('admin/article')->with('Success', 'Data Saved');
    }


    public function show($id)
    {
        //
    }


    public function edit($id)
    {
        $title = 'Edit Article Data';
        $article = Article::FindOrFail($id);
        return view('admin.forms.article.update', compact('title', 'article'));
    }


    public function update(Request $request, $id)
    {
        $article = Article::FindOrFail($id);

        $request->validate([
            'images'             => 'mimes:jpeg,jpg,png',
        ]);
        $request->validate([
            'thumbnail'             => 'mimes:jpeg,jpg,png',
        ]);

        $file_image = $request['images'];
        $file_thumbnail = $request['thumbnails'];

        if ($file_image != "") {

            if (File::exists($this->path .'/'. $article->image)) {
                File::delete($this->path .'/'. $article->image);

               $name_image = date('YmdHis') .'_'. $file_image->getClientOriginalName();
               $file_image->move($this->path, $name_image);
               $request['image'] = $name_image;
            } else {
                $name_image = date('YmdHis') .'_'. $file_image->getClientOriginalName();
                $file_image->move($this->path, $name_image);
                $request['image'] = $name_image;
            }

        }
        if ($file_thumbnail != "") {

            if (File::exists($this->path .'/'. $article->thumbnail)) {
                File::delete($this->path .'/'. $article->thumbnail);

               $name_thumbnail = date('YmdHis') .'_thumbnail_'. $file_thumbnail->getClientOriginalName();
               $file_thumbnail->move($this->path, $name_thumbnail);
               $request['thumbnail'] = $name_thumbnail;
            } else {
                $name_thumbnail = date('YmdHis') .'_'. $file_thumbnail->getClientOriginalName();
                $file_thumbnail->move($this->path, $name_thumbnail);
                $request['thumbnail'] = $name_thumbnail;
            }

        }
        $request['slug'] = Str::slug($request['title'], '-');
        $requestData = $request->all();
        $article->update($requestData);

        return redirect('/admin/article')->with('Success', 'Data Updated');
    }


    public function destroy($id)
    {
        $article = Article::FindOrFail($id);
        Article::destroy($id);

        File::delete($this->path .'/'. $article->image);
        return redirect('/admin/article');
    }
}
