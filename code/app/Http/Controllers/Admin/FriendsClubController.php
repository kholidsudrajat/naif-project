<?php

namespace App\Http\Controllers\Admin;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use DB;
use App\Friends_Club;
use App\Exports\FriendsClubExport;
use Maatwebsite\Excel\Facades\Excel;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use DataTables;
use Illuminate\Support\Str;

class FriendsClubController extends Controller
{


    public function index(Request $request)
    {
        $title = "Friends Club List";
        $startDate = $request->start_date;
        $endDate = $request->end_date;
        if ($request->ajax()) {

            if(is_null($startDate) && is_null($endDate))
            {
                $data = DB::table('friends_clubs')
                        ->join('provinces', 'provinces.id', '=', 'friends_clubs.provinsi')
                        // ->join('cities', 'cities.id', '=', 'friends_clubs.kota')
                        // ->join('subdistrict', 'subdistrict.id', '=', 'friends_clubs.kecamatan')
                        ->select('friends_clubs.*', 'provinces.provinsi')
                        ->orderBy('friends_clubs.created_at', 'DESC')
                        ->get();
            }
            else
            {
                $data = DB::table('friends_clubs')
                ->join('provinces', 'provinces.id', '=', 'friends_clubs.provinsi')
                // ->join('cities', 'cities.id', '=', 'friends_clubs.kota')
                // ->join('subdistrict', 'subdistrict.id', '=', 'friends_clubs.kecamatan')
                ->select('friends_clubs.*', 'provinces.provinsi')
                ->whereBetween('friends_clubs.created_at', [$startDate.' 00:00:00', $endDate.' 23:59:59'])
                ->orderBy('friends_clubs.created_at', 'DESC')
                ->get();


            }

            return datatables()->of($data)
            ->make(true);
        }
        return view('admin.forms.friends-club.index', compact('title'));
    }

    public function friends_confirm(Request $request, $id)
    {
        $friends_club = Friends_Club::FindOrFail($id);
        $friends_club->status = 1;
        $friends_club->save();

        return true;
    }


    public function destroy($id)
    {
        if(auth()->user()->hasRole('superadmin')){
            Friends_Club::destroy($id);
        }

    }
    public function show(Request $request)
	{
        $startDate = $request->start_date;
        $endDate = $request->end_date;
        if(is_null($request->start_date) && is_null($request->end_date))
        {
            $export = new FriendsClubExport();
            return Excel::download($export, 'Naif-friends-club.xlsx');
        }
        else{
            $export = new FriendsClubExport();
            $export->startDate($startDate);
            $export->endDate($endDate);
            return Excel::download($export, 'Naif-friends-club.xlsx');
        }


	}


}
