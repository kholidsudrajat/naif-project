<?php

namespace App\Http\Controllers\Admin;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use File;
use DB;
use App\Url_Menu;

class UrlMenuController extends Controller
{
    public function index()
    {
        $title = "URL Menu List";
        $url_menu = DB::table('url_menu')
                            ->orderBy('id', 'DESC')
                            ->get();
        return view('admin.forms.url-menu.index', compact('title', 'url_menu'));
    }


    public function create()
    {
        $title= 'Add URL Menu Data';
        return view('admin.forms.url-menu.create', compact('title'));
    }


    public function store(Request $request)
    {
        $requestData = $request->all();
        Url_Menu::create($requestData);
        return redirect('admin/url-menu')->with('Success', 'Data Saved');
    }


    public function show($id)
    {
        //
    }


    public function edit($id)
    {
        $title = 'Edit URL Menu Data';
        $url_menu = Url_Menu::FindOrFail($id);
        return view('admin.forms.url-menu.update', compact('title', 'url_menu'));
    }


    public function update(Request $request, $id)
    {
        $url_menu = Url_Menu::FindOrFail($id);

        $requestData = $request->all();
        $url_menu->update($requestData);

        return redirect('/admin/url-menu')->with('Success', 'Data Updated');
    }


    public function destroy($id)
    {
        $url_menu = Url_Menu::FindOrFail($id);
        Url_Menu::destroy($id);

        return redirect('/admin/url-menu');
    }
}
