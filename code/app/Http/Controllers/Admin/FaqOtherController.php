<?php

namespace App\Http\Controllers\Admin;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use File;
use DB;
use App\Faq_Other;

class FaqOtherController extends Controller
{
    public function index()
    {
        $title = "FAQ Other List";
        $faq_other = DB::table('faq_other')
                            ->orderBy('id', 'DESC')
                            ->get();
        return view('admin.forms.faq-other.index', compact('title', 'faq_other'));
    }


    public function create()
    {
        $title= 'Add FAQ Other Data';
        return view('admin.forms.faq-other.create', compact('title'));
    }


    public function store(Request $request)
    {
        $requestData = $request->all();
        Faq_Other::create($requestData);
        return redirect('admin/faq-other')->with('Success', 'Data Saved');
    }


    public function show($id)
    {
        //
    }


    public function edit($id)
    {
        $title = 'Edit FAQ Other Data';
        $faq_other = Faq_Other::FindOrFail($id);
        return view('admin.forms.faq-other.update', compact('title', 'faq_other'));
    }


    public function update(Request $request, $id)
    {
        $faq_other = Faq_Other::FindOrFail($id);

        $requestData = $request->all();
        $faq_other->update($requestData);

        return redirect('/admin/faq-other')->with('Success', 'Data Updated');
    }


    public function destroy($id)
    {
        $faq_other = Faq_Other::FindOrFail($id);
        Faq_Other::destroy($id);

        return redirect('/admin/faq-other');
    }
}
