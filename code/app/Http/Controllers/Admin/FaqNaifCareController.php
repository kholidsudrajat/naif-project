<?php

namespace App\Http\Controllers\Admin;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use File;
use DB;
use App\Faq_Naif_Care;

class FaqNaifCareController extends Controller
{
    public function index()
    {
        $title = "FAQ Naif Care List";
        $faq_naif_care = DB::table('faq_naif_care')
                            ->orderBy('id', 'DESC')
                            ->get();
        return view('admin.forms.faq-naif-care.index', compact('title', 'faq_naif_care'));
    }


    public function create()
    {
        $title= 'Add FAQ Naif Care Data';
        return view('admin.forms.faq-naif-care.create', compact('title'));
    }


    public function store(Request $request)
    {
        $requestData = $request->all();
        Faq_Naif_Care::create($requestData);
        return redirect('admin/faq-naif-care')->with('Success', 'Data Saved');
    }


    public function show($id)
    {
        //
    }


    public function edit($id)
    {
        $title = 'Edit FAQ Naif Care Data';
        $faq_naif_care = Faq_Naif_Care::FindOrFail($id);
        return view('admin.forms.faq-naif-care.update', compact('title', 'faq_naif_care'));
    }


    public function update(Request $request, $id)
    {
        $faq_naif_care = Faq_Naif_Care::FindOrFail($id);

        $requestData = $request->all();
        $faq_naif_care->update($requestData);

        return redirect('/admin/faq-naif-care')->with('Success', 'Data Updated');
    }


    public function destroy($id)
    {
        $faq_naif_care = Faq_Naif_Care::FindOrFail($id);
        Faq_Naif_Care::destroy($id);

        return redirect('/admin/faq-naif-care');
    }
}
