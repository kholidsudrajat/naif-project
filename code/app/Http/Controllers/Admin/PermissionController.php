<?php

namespace App\Http\Controllers\Admin;
use App\Http\Controllers\Controller;

use App\Permission;
use App\Role;
use App\User;
use Illuminate\Http\Request;

class PermissionController extends Controller
{
    public function permission()
    {
    	$dev_permission = Permission::where('slug','create-tasks')->first();
		$manager_permission = Permission::where('slug', 'edit-users')->first();

		//RoleTableSeeder.php
		$dev_role = new Role();
		$dev_role->slug = 'admin';
		$dev_role->name = 'Admin';
		$dev_role->save();
		$dev_role->permissions()->attach($dev_permission);

		$manager_role = new Role();
		$manager_role->slug = 'superadmin';
		$manager_role->name = 'Superadmin';
		$manager_role->save();
		$manager_role->permissions()->attach($manager_permission);

		$dev_role = Role::where('slug','admin')->first();
        $manager_role = Role::where('slug', 'superadmin')->first();

        // Role user===============================
        $listUsers = new Permission();
        $listUsers->name = 'List Users Access';
        $listUsers->slug = 'list-users-access';
        $listUsers->save();
        $listUsers->roles()->attach($manager_role);

        $createUsers = new Permission();
        $createUsers->name = 'Create Users Access';
        $createUsers->slug = 'create-users-access';
        $createUsers->save();
        $createUsers->roles()->attach($manager_role);

        $editUsers = new Permission();
        $editUsers->name = 'Edit Users Access';
        $editUsers->slug = 'edit-users-access';
        $editUsers->save();
        $editUsers->roles()->attach($manager_role);

        $deleteUsers = new Permission();
        $deleteUsers->name = 'Delete Users Access';
        $deleteUsers->slug = 'delete-users-access';
        $deleteUsers->save();
        $deleteUsers->roles()->attach($manager_role);

        // Role user===============================
        // Role Profile===============================

        $editProfile = new Permission();
        $editProfile->name = 'Edit Profile';
        $editProfile->slug = 'edit-profile';
        $editProfile->save();
        $editProfile->roles()->attach($manager_role);

        // Role Profile===============================



		$dev_role = Role::where('slug','admin')->first();
		$manager_role = Role::where('slug', 'superadmin')->first();
		$dev_perm = Permission::where('slug','create-users')->first();
		$manager_perm = Permission::where('slug','edit-users')->first();

		$developer = new User();
		$developer->name = 'Admin';
		$developer->email = 'admin@admin.com';
		$developer->password = bcrypt('secret');
		$developer->save();
		$developer->roles()->attach($dev_role);
		$developer->permissions()->attach($dev_perm);

		$manager = new User();
		$manager->name = 'Superadmin';
		$manager->email = 'superadmin@superadmin.com';
		$manager->password = bcrypt('secret');
		$manager->save();
		$manager->roles()->attach($manager_role);
		$manager->permissions()->attach($manager_perm);

		return redirect()->back();
    }
}
