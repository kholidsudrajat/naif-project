<?php

namespace App\Http\Controllers\Admin;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use DB;
use App\Contact_Us;
use App\Exports\ContactUsExport;
use Maatwebsite\Excel\Facades\Excel;
// use Yajra\Datatables\Datatables;
use DataTables;
use Illuminate\Support\Str;


class ContactUsController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(Request $request)
    {
        $title = 'Contact Us List';
        $startDate = $request->start_date;
        $endDate = $request->end_date;
        if ($request->ajax()) {

            if(is_null($startDate) && is_null($endDate))
            {
                $data = DB::table('contact_us')
                ->orderBy('created_at', 'desc')
                ->get();
            }
            else
            {
                $data = DB::table('contact_us')
                ->whereBetween('created_at', [$startDate.' 00:00:00', $endDate.' 23:59:59'])
                ->orderBy('created_at', 'desc')
                ->get();
            }
        return datatables()->of($data)
            ->make(true);
        }


        return view('admin.forms.contact-us.index', compact('title'));
    }

    public function contact_confirm(Request $request, $id)
    {
        $contact_us = Contact_Us::FindOrFail($id);
        $contact_us->status = 1;
        $contact_us->save();

        return true;
    }

    public function destroy($id)
    {
        if(auth()->user()->hasRole('superadmin')){
            Contact_Us::destroy($id);
        }
    }

    public function show(Request $request)
    {
        $startDate = $request->start_date;
        $endDate = $request->end_date;
        if(is_null($request->start_date) && is_null($request->end_date))
        {
            $export = new ContactUsExport();
            return Excel::download($export, 'Naif-contact-us.xlsx');
        }
        else{
            $export = new ContactUsExport();
            $export->startDate($startDate);
            $export->endDate($endDate);
            return Excel::download($export, 'Naif-contact-us.xlsx');
        }
    }




}
