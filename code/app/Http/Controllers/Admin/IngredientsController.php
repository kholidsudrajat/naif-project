<?php

namespace App\Http\Controllers\Admin;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use File;
use DB;
use App\Ingredients;

class IngredientsController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        $this->path = 'upload/ingredients';
    }
    public function index()
    {
        $title = "Ingredients List";
        $ingredients = DB::table('ingredients')
                            ->orderBy('id', 'DESC')
                            ->get();
        return view('admin.forms.ingredients.index', compact('title', 'ingredients'));
    }


    public function create()
    {
        $title= 'Add Ingredients Data';
        return view('admin.forms.ingredients.create', compact('title'));
    }


    public function store(Request $request)
    {
        $request->validate([
            'images'     => 'required|mimes:jpeg,jpg,png',

        ]);

        $file_image           = $request['images'];

        if ($file_image != "") {

            $name_image = date('YmdHis') .'_'. $file_image->getClientOriginalName();
            $file_image->move($this->path, $name_image);
            $request['image'] = $name_image;

        }

        $requestData = $request->all();
        Ingredients::create($requestData);
        return redirect('admin/ingredients')->with('Success', 'Data Saved');
    }


    public function show($id)
    {
        //
    }


    public function edit($id)
    {
        $title = 'Edit Ingredients Data';
        $ingredients = Ingredients::FindOrFail($id);
        return view('admin.forms.ingredients.update', compact('title', 'ingredients'));
    }


    public function update(Request $request, $id)
    {
        $ingredients = Ingredients::FindOrFail($id);

        $request->validate([
            'images'             => 'mimes:jpeg,jpg,png',
        ]);

        $file_image = $request['images'];

        if ($file_image != "") {

            if (File::exists($this->path .'/'. $ingredients->image)) {
                File::delete($this->path .'/'. $ingredients->image);

               $name_image = date('YmdHis') .'_'. $file_image->getClientOriginalName();
               $file_image->move($this->path, $name_image);
               $request['image'] = $name_image;
            } else {
                $name_image = date('YmdHis') .'_'. $file_image->getClientOriginalName();
                $file_image->move($this->path, $name_image);
                $request['image'] = $name_image;
            }

        }

        $requestData = $request->all();
        $ingredients->update($requestData);

        return redirect('/admin/ingredients')->with('Success', 'Data Updated');
    }


    public function destroy($id)
    {
        $ingredients = Ingredients::FindOrFail($id);
        Ingredients::destroy($id);

        File::delete($this->path .'/'. $ingredients->image);
        return redirect('/admin/ingredients');
    }
}
