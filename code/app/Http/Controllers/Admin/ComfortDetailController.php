<?php

namespace App\Http\Controllers\Admin;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use DB;
use File;
use App\Master_Product;
use App\Product_Details;
use App\Category;


class ComfortDetailController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        $this->path = 'upload/products';
    }
    public function index()
    {


    }


    public function create($id)
    {
        $title = 'Add Comfort Image';
        $id_product = Master_Product::where('id', $id)->first();

        if($id_product){
            $category_data = Category::pluck('category', 'id')->prepend('Please select category', '');

            $product_detail = DB::select("SELECT pd.id, mp.id AS id_product, pd.image, pd.image_thumbnail
                        FROM product_details pd
                        INNER JOIN master_products mp ON (mp.id = pd.name)
                        WHERE mp.id=$id_product->id
                        ORDER BY mp.id, pd.id DESC");

            return view('admin.forms.comfort-detail.form-aio', compact('title', 'category_data', 'product_detail', 'id'));
        }
        else{
            // abort(404);
            return redirect('admin/comfort-product/create');
        }
    }

    public function store(Request $request, $id)
    {

        $request->validate([
            'images'     => 'required|mimes:jpeg,jpg,png',
        ]);
        $file_image           = $request['images'];

        if ($file_image != "") {

            $name_image = date('YmdHis') .'_'. $file_image->getClientOriginalName();
            $file_image->move($this->path, $name_image);
            $request['image'] = $name_image;

        }
        $id_product = Master_Product::where('id', $id)->first();
        $request['name'] = $id_product->id;
        $requestData = $request->all();
        Product_Details::create($requestData);
        return redirect('admin/comfort/product-detail/'.$id_product->id)->with('Success', 'Data Saved');
    }





    public function destroy($id)
    {
        $comfort_detail = Product_Details::FindOrFail($id);
        Product_Details::destroy($id);

        File::delete($this->path .'/'. $comfort_detail->image);

    }
}
