<?php

namespace App\Http\Controllers\Admin;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use DB;
use App\Master_Product;
use App\Faq_Product;

class FaqProductController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    public function index()
    {
        //
    }


    public function create($id)

    {
        $title = 'Add Faq Product';
        $id_product = Master_Product::where('id', $id)->first();
        if ($id_product)
        {
            $faq_product = DB::select("SELECT fp.id, mp.id AS id_product, fp.question, fp.answer
                            FROM faq_products fp
                            INNER JOIN master_products mp ON (mp.id = fp.name)
                            WHERE mp.id=$id_product->id
                            ORDER BY mp.id, fp.id DESC");

            return view('admin.forms.comfort-detail.form-faq', compact('title', 'faq_product', 'id'));
        } else
        {
           return redirect('admin/feeding-product/create');
        }


    }


    public function store(Request $request, $id)
    {
        $id_product = Master_Product::where('id', $id)->first();
        $request['name'] = $id_product->id;
        $requestData = $request->all();
        Faq_Product::create($requestData);
        return redirect('admin/comfort/faq-product/'.$id_product->id)->with('Success', 'Data Saved');
    }


    public function edit($id)
    {
        //
    }


    public function update(Request $request, $id)
    {
        //
    }


    public function destroy($id)
    {
        $comfort_detail = Faq_Product::FindOrFail($id);
        Faq_Product::destroy($id);
    }
}
