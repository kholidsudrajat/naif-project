<?php

namespace App\Http\Controllers\Admin;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use File;
use DB;
use App\What_Do_Our_Logo_Mean;

class WhatDoOurLogoMeanController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        $this->path = 'upload/what-do-our-logo-mean';
    }
    public function index()
    {
        $title = "What Do Our Logo Mean List";
        $what_do_our_logo_mean = DB::table('what_do_our_logo_mean')
                            ->orderBy('section', 'ASC')
                            ->get();
        return view('admin.forms.what-do-our-logo-mean.index', compact('title', 'what_do_our_logo_mean'));
    }


    public function create()
    {
        $title= 'Add What Do Our Logo Mean Data';
        return view('admin.forms.what-do-our-logo-mean.create', compact('title'));
    }


    public function store(Request $request)
    {
        $request->validate([
            'images'     => 'required|mimes:jpeg,jpg,png',

        ]);

        $file_image           = $request['images'];

        if ($file_image != "") {

            $name_image = date('YmdHis') .'_'. $file_image->getClientOriginalName();
            $file_image->move($this->path, $name_image);
            $request['image'] = $name_image;

        }

        $requestData = $request->all();
        What_Do_Our_Logo_Mean::create($requestData);
        return redirect('admin/what-do-our-logo-mean')->with('Success', 'Data Saved');
    }


    public function show($id)
    {
        //
    }


    public function edit($id)
    {
        $title = 'Edit What Do Our Logo Mean Data';
        $what_do_our_logo_mean = What_Do_Our_Logo_Mean::FindOrFail($id);
        return view('admin.forms.what-do-our-logo-mean.update', compact('title', 'what_do_our_logo_mean'));
    }


    public function update(Request $request, $id)
    {
        $what_do_our_logo_mean = What_Do_Our_Logo_Mean::FindOrFail($id);

        $request->validate([
            'images'             => 'mimes:jpeg,jpg,png',
        ]);

        $file_image = $request['images'];

        if ($file_image != "") {

            if (File::exists($this->path .'/'. $what_do_our_logo_mean->image)) {
                File::delete($this->path .'/'. $what_do_our_logo_mean->image);

               $name_image = date('YmdHis') .'_'. $file_image->getClientOriginalName();
               $file_image->move($this->path, $name_image);
               $request['image'] = $name_image;
            } else {
                $name_image = date('YmdHis') .'_'. $file_image->getClientOriginalName();
                $file_image->move($this->path, $name_image);
                $request['image'] = $name_image;
            }

        }

        $requestData = $request->all();
        $what_do_our_logo_mean->update($requestData);

        return redirect('/admin/what-do-our-logo-mean')->with('Success', 'Data Updated');
    }


    public function destroy($id)
    {
        $what_do_our_logo_mean = What_Do_Our_Logo_Mean::FindOrFail($id);
        What_Do_Our_Logo_Mean::destroy($id);

        File::delete($this->path .'/'. $what_do_our_logo_mean->image);
        return redirect('/admin/what-do-our-logo-mean');
    }
}
