<?php

namespace App\Http\Controllers\Admin;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use File;
use DB;
use App\Faq_Naif_Baby;

class FaqNaifBabyController extends Controller
{
    public function index()
    {
        $title = "FAQ Naif Baby List";
        $faq_naif_baby = DB::table('faq_naif_baby')
                            ->orderBy('id', 'DESC')
                            ->get();
        return view('admin.forms.faq-naif-baby.index', compact('title', 'faq_naif_baby'));
    }


    public function create()
    {
        $title= 'Add FAQ Naif Baby Data';
        return view('admin.forms.faq-naif-baby.create', compact('title'));
    }


    public function store(Request $request)
    {
        $requestData = $request->all();
        Faq_Naif_Baby::create($requestData);
        return redirect('admin/faq-naif-baby')->with('Success', 'Data Saved');
    }


    public function show($id)
    {
        //
    }


    public function edit($id)
    {
        $title = 'Edit FAQ Naif Baby Data';
        $faq_naif_baby = Faq_Naif_Baby::FindOrFail($id);
        return view('admin.forms.faq-naif-baby.update', compact('title', 'faq_naif_baby'));
    }


    public function update(Request $request, $id)
    {
        $faq_naif_baby = Faq_Naif_Baby::FindOrFail($id);

        $requestData = $request->all();
        $faq_naif_baby->update($requestData);

        return redirect('/admin/faq-naif-baby')->with('Success', 'Data Updated');
    }


    public function destroy($id)
    {
        $faq_naif_baby = Faq_Naif_Baby::FindOrFail($id);
        Faq_Naif_Baby::destroy($id);

        return redirect('/admin/faq-naif-baby');
    }
}
