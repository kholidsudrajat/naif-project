<?php

namespace App\Http\Controllers\Admin;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use File;
use DB;
use App\Friends_Club_Benefit;

class FriendsClubBenefitController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        $this->path = 'upload/friends-club-benefit';
    }
    public function index()
    {
        $title = "Friends Club Benefit List";
        $friends_club_benefit = DB::table('friends_clubs_benefit')
                            ->orderBy('id', 'DESC')
                            ->get();
        return view('admin.forms.friends-club-benefit.index', compact('title', 'friends_club_benefit'));
    }


    public function create()
    {
        $title= 'Add Friends Club Benefit Data';
        return view('admin.forms.friends-club-benefit.create', compact('title'));
    }


    public function store(Request $request)
    {
        $request->validate([
            'images'     => 'required|mimes:jpeg,jpg,png',

        ]);

        $file_image           = $request['images'];

        if ($file_image != "") {

            $name_image = date('YmdHis') .'_'. $file_image->getClientOriginalName();
            $file_image->move($this->path, $name_image);
            $request['image'] = $name_image;

        }

        $requestData = $request->all();
        Friends_Club_Benefit::create($requestData);
        return redirect('admin/friends-club-benefit')->with('Success', 'Data Saved');
    }


    public function show($id)
    {
        //
    }


    public function edit($id)
    {
        $title = 'Edit Friends Club Benefit Data';
        $friends_club_benefit = Friends_Club_Benefit::FindOrFail($id);
        return view('admin.forms.friends-club-benefit.update', compact('title', 'friends_club_benefit'));
    }


    public function update(Request $request, $id)
    {
        $friends_club_benefit = Friends_Club_Benefit::FindOrFail($id);

        $request->validate([
            'images'             => 'mimes:jpeg,jpg,png',
        ]);

        $file_image = $request['images'];

        if ($file_image != "") {

            if (File::exists($this->path .'/'. $friends_club_benefit->image)) {
                File::delete($this->path .'/'. $friends_club_benefit->image);

               $name_image = date('YmdHis') .'_'. $file_image->getClientOriginalName();
               $file_image->move($this->path, $name_image);
               $request['image'] = $name_image;
            } else {
                $name_image = date('YmdHis') .'_'. $file_image->getClientOriginalName();
                $file_image->move($this->path, $name_image);
                $request['image'] = $name_image;
            }

        }

        $requestData = $request->all();
        $friends_club_benefit->update($requestData);

        return redirect('/admin/friends-club-benefit')->with('Success', 'Data Updated');
    }


    public function destroy($id)
    {
        $friends_club_benefit = Friends_Club_Benefit::FindOrFail($id);
        Friends_Club_Benefit::destroy($id);

        File::delete($this->path .'/'. $friends_club_benefit->image);
        return redirect('/admin/friends-club-benefit');
    }
}
