<?php

namespace App\Http\Controllers\Admin;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use File;
use DB;
use App\Banner_Page;
class BannerPageController extends Controller
{
   public function __construct()
   {
       $this->middleware('auth');
       $this->path = 'upload/banner';
   }
    public function index()
    {
        $title = 'Banner List';
        $banner = DB::select("SELECT * FROM banner_pages ORDER BY id DESC");
        return view('admin.forms.banner-page.index', compact('title', 'banner'));
    }

   
    public function create()
    {
        $title= 'Add Banner Data';
        $banner = null;
        return view('admin.forms.banner-page.create', compact('title', 'banner'));
    }

   
    public function store(Request $request)
    {
        $request->validate([
            'images'     => 'required|mimes:jpeg,jpg,png',

        ]);

        $file_image           = $request['images'];

        if ($file_image != "") {

            $name_image = date('YmdHis') .'_'. $file_image->getClientOriginalName();
            $file_image->move($this->path, $name_image);
            $request['image'] = $name_image;

        }

        $requestData = $request->all();
        Banner_Page::create($requestData);
        return redirect('admin/banner')->with('Success', 'Data Saved');
    }

   
    public function show($id)
    {
        //
    }

    
    public function edit($id)
    {
        $title = 'Edit Banner Data';
        $banner = Banner_Page::FindOrFail($id);
        return view('admin.forms.banner-page.update', compact('title', 'banner'));
    }

    
    public function update(Request $request, $id)
    {
        $banner = Banner_Page::FindOrFail($id);

        $request->validate([
            'images'             => 'mimes:jpeg,jpg,png',
        ]);

        $file_image = $request['images'];

        if ($file_image != "") {

            if (File::exists($this->path .'/'. $banner->image)) {
                File::delete($this->path .'/'. $banner->image);

               $name_image = date('YmdHis') .'_'. $file_image->getClientOriginalName();
               $file_image->move($this->path, $name_image);
               $request['image'] = $name_image;
            } else {
                $name_image = date('YmdHis') .'_'. $file_image->getClientOriginalName();
                $file_image->move($this->path, $name_image);
                $request['image'] = $name_image;
            }

        }

        $requestData = $request->all();
        $banner->update($requestData);

        return redirect('/admin/banner')->with('Success', 'Data Updated');
    }

    
    public function destroy($id)
    {
        $banner = Banner_Page::FindOrFail($id);
        Banner_Page::destroy($id);

        File::delete($this->path .'/'. $banner->image);
        return redirect('/admin/banner');
    }
}
