<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use App\Role;
use DB;
use App\User_Roles;
use App\User;



class UserAccessController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');

    }

    public function index(Request $request)
    {
        if(auth()->user()->hasRole('superadmin')){
            $title = "List User Access";
            $user_akses = DB::select("SELECT u.id, u.name, u.email, r.name AS role_name
                            FROM users_roles us
                            INNER JOIN roles r ON (us.role_id = r.id)
                            INNER JOIN users u ON (us.user_id = u.id)
                            ORDER BY u.id ASC");
            return view('admin.forms.user-akses.index', compact('title', 'user_akses'));
        }
    }


    public function create()
    {
        if(auth()->user()->hasRole('superadmin')){
            $title = 'Add User Akses';
            // $roles = Role::pluck('name','name')->all();
            $roles = DB::table('roles')
                        ->select('id', 'name')
                        ->get();
            return view('admin.forms.user-akses.create', compact('title', 'roles'));
        }
    }


    public function store(Request $request)
    {
        if(auth()->user()->hasRole('superadmin')){

            $email = $request->email;
            $emailcheck = DB::table('users')->where('email',$email)->count();
            $validator =[
                'password' => 'min:6|required_with:password_confirmation|same:password_confirmation',
                'password_confirmation' => 'min:6'
            ];

            $this->validate($request, $validator);
            if($emailcheck > 0)
            {
                echo "<script>
                    alert('Email Already In Use.')
                    window.location = '".url('/admin/user-access/create')."';
                    </script>";
            }
            else {
                $request['password'] = Hash::make($request->password);

                $requestData = $request->all();
                $user = User::create($requestData);

                $user_roles = DB::table('users_roles')->insert([
                    'user_id' => $user->id,
                    'role_id' => $request->roles
                ]);

                return redirect('admin/user-access')->with('Success', 'Data Saved');
            }
        }
    }

    public function edit($id)
    {
        if(auth()->user()->hasRole('superadmin')){
            $title = 'Edit User Akses';
            // $roles = Role::pluck('name','name')->all();
            // $userRole = $user->roles->pluck('name','name')->all();
            $roles = DB::table('roles')
                        ->select('id', 'name')
                        ->get();

            $user = User::FindOrFail($id);
            return view('admin.forms.user-akses.update', compact('title','user', 'roles'));
        }
    }


    public function update(Request $request, $id)
    {
        if(auth()->user()->hasRole('superadmin')){

            $validator =[
                'password' => 'min:6|required_with:password_confirmation|same:password_confirmation',
                'password_confirmation' => 'min:6'
            ];

            $this->validate($request, $validator);



            $request['password'] = Hash::make($request->password);
            $requestData = $request->all();

            $user = User::FindOrFail($id);

            $user->update($requestData);

            DB::table('users_roles')->where('user_id',$id)->delete();

            // $user->assignRole($request->input('roles'));
            $user_roles = DB::table('users_roles')->insert([
                'user_id' => $user->id,
                'role_id' => $request->roles
            ]);

            return redirect('/admin/user-access')->with('Success', 'Data Updated');
        }
    }


    public function destroy($id)
    {
        if(auth()->user()->hasRole('superadmin')){
            User::find($id)->delete();
            DB::table('users_roles')->where('user_id',$id)->delete();
            return redirect('/admin/user-access');
        }
    }
}
