<?php

namespace App\Http\Controllers\Admin;
use App\Http\Controllers\Controller;


use Illuminate\Http\Request;
use DB;
use File;
use App\Profile;
class ProfileController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        $this->path = 'upload/profile';
    }
    public function index()
    {
        $title = 'Profile';
        $profile = Profile::all();
        return view('admin.forms.profile.index', compact('title', 'profile'));
    }

    public function edit($id)
    {
        $title = "Edit Profile";
        $profile = Profile::FindOrFail($id);

        return view('admin.forms.profile.update', compact('title', 'profile'));
    }


    public function update(Request $request, $id)
    {
        $profile = Profile::FindOrFail($id);

        $request->validate([
            'images'             => 'mimes:jpeg,jpg,png',

        ]);

        $request->validate([
            'cs_images'             => 'mimes:jpeg,jpg,png',

        ]);

        $file_image = $request['images'];
        $file_cs_image = $request['cs_images'];


        if ($file_image != "") {

            if (File::exists($this->path .'/'. $profile->image)) {
                File::delete($this->path .'/'. $profile->image);

               $name_image = date('YmdHis') .'_'. $file_image->getClientOriginalName();
               $file_image->move($this->path, $name_image);
               $request['image'] = $name_image;
            } else {
                $name_image = date('YmdHis') .'_'. $file_image->getClientOriginalName();
                $file_image->move($this->path, $name_image);
                $request['image'] = $name_image;
            }

        }

        if ($file_cs_image != "") {

            if (File::exists($this->path .'/'. $profile->cs_image)) {
                File::delete($this->path .'/'. $profile->cs_image);

               $name_cs_image = date('YmdHis') .'_'. $file_cs_image->getClientOriginalName();
               $file_cs_image->move($this->path, $name_cs_image);
               $request['cs_image'] = $name_cs_image;
            } else {
                $name_cs_image = date('YmdHis') .'_'. $file_cs_image->getClientOriginalName();
                $file_cs_image->move($this->path, $name_cs_image);
                $request['cs_image'] = $name_cs_image;
            }

        }


        $requestData = $request->all();
        $profile->update($requestData);

        return redirect('/admin/profile')->with('Success', 'Data Updated');
    }


    public function destroy($id)
    {
        //
    }
}
