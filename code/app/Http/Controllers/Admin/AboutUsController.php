<?php

namespace App\Http\Controllers\Admin;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use File;
use DB;
use App\About_Us;

class AboutUsController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        $this->path = 'upload/about-us';
    }
    public function index()
    {
        $title = "About Us List";
        $about_us = DB::table('about_us')
                            ->orderBy('section', 'ASC')
                            ->get();
        return view('admin.forms.about-us.index', compact('title', 'about_us'));
    }


    public function create()
    {
        $title= 'Add About Us Data';
        return view('admin.forms.about-us.create', compact('title'));
    }


    public function store(Request $request)
    {
        $request->validate([
            'images'     => 'required|mimes:jpeg,jpg,png',

        ]);

        $file_image           = $request['images'];

        if ($file_image != "") {

            $name_image = date('YmdHis') .'_'. $file_image->getClientOriginalName();
            $file_image->move($this->path, $name_image);
            $request['image'] = $name_image;

        }

        $requestData = $request->all();
        About_Us::create($requestData);
        return redirect('admin/about-us')->with('Success', 'Data Saved');
    }


    public function show($id)
    {
        //
    }


    public function edit($id)
    {
        $title = 'Edit Friends Club Benefit Data';
        $about_us = About_Us::FindOrFail($id);
        return view('admin.forms.about-us.update', compact('title', 'about_us'));
    }


    public function update(Request $request, $id)
    {
        $about_us = About_Us::FindOrFail($id);

        $request->validate([
            'images'             => 'mimes:jpeg,jpg,png',
        ]);

        $file_image = $request['images'];

        if ($file_image != "") {

            if (File::exists($this->path .'/'. $about_us->image)) {
                File::delete($this->path .'/'. $about_us->image);

               $name_image = date('YmdHis') .'_'. $file_image->getClientOriginalName();
               $file_image->move($this->path, $name_image);
               $request['image'] = $name_image;
            } else {
                $name_image = date('YmdHis') .'_'. $file_image->getClientOriginalName();
                $file_image->move($this->path, $name_image);
                $request['image'] = $name_image;
            }

        }

        $requestData = $request->all();
        $about_us->update($requestData);

        return redirect('/admin/about-us')->with('Success', 'Data Updated');
    }


    public function destroy($id)
    {
        $about_us = About_Us::FindOrFail($id);
        About_Us::destroy($id);

        File::delete($this->path .'/'. $about_us->image);
        return redirect('/admin/about-us');
    }
}
