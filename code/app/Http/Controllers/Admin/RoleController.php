<?php

namespace App\Http\Controllers\Admin;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;
use DB;
class RoleController extends Controller
{


    public function index(Request $request)
    {
        if(auth()->user()->hasRole('superadmin')){
            $title = "List Role Access";
            $roles = Role::orderBy('id','DESC')->paginate(5);
            return view('permissions.roles.index',compact('title', 'roles'));
            }
    }
}
