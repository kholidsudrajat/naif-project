<?php

namespace App\Http\Controllers\Api;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;

class ApiYoutubeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     *
     */
    const URL = 'https://www.googleapis.com/youtube/v3/playlistItems?part=snippet&key=AIzaSyApEI0oFSx3Rnm60466TkaeruBCakohoEA&playlistId=';

    const URL_PLAYLIST = 'https://www.googleapis.com/youtube/v3/videos?part=statistics&key=AIzaSyApEI0oFSx3Rnm60466TkaeruBCakohoEA&id=';

    private function getViewCount($video_id){
        $api_viewCount = self::URL_PLAYLIST.$video_id;
        $sc = curl_init();
        curl_setopt($sc, CURLOPT_URL, $api_viewCount);
        curl_setopt($sc, CURLOPT_RETURNTRANSFER, 1);
        $data = curl_exec($sc);
        curl_close($sc);

        $datas = json_decode($data, true);

       return $datas;
    }

    public function mommy()
    {
        // helpful resource  recipe-for-mommy
        $playlistId = 'PL7vdwX11yNOilt42bhnbCJJFFhUSIDqKp';
        $maxResults = '2';
        $api_url = self::URL.$playlistId.'&maxResults='.$maxResults;
        $sc = curl_init();
        curl_setopt($sc, CURLOPT_URL, $api_url);
        curl_setopt($sc, CURLOPT_RETURNTRANSFER, 1);
        $data = curl_exec($sc);
        curl_close($sc);
        $datas = json_decode($data, true);

        foreach($datas["items"] as $item){
            $views = $this->getViewCount($item["snippet"]["resourceId"]["videoId"]);

            foreach($views["items"] as $view){
                $result_mommy[] = [
                    "id" => $item["id"],
                    "title" => $item["snippet"]["title"],
                    "description" => $item["snippet"]["description"],
                    "statistic_videos" => $view["statistics"],
                    "resourceId" =>$item["snippet"]["resourceId"]["videoId"],
                    "thumbnail" => [
                        $item["snippet"]["thumbnails"]
                    ],

                ];
            }
        }

        return $result_mommy;
    }


    public function recipe_baby()
    {
        // helpful resource  recipe-for-baby
        $playlistI = 'PL7vdwX11yNOh3-jQRPPgkAcVawH8eFr77';
        $maxResults = '1';
        $api_url = self::URL.$playlistI.'&maxResults='.$maxResults;
        $sc = curl_init();
        curl_setopt($sc, CURLOPT_URL, $api_url);
        curl_setopt($sc, CURLOPT_RETURNTRANSFER, 1);
        $data = curl_exec($sc);
        curl_close($sc);
        $datas = json_decode($data, true);

        foreach($datas["items"] as $item){
            $views = $this->getViewCount($item["snippet"]["resourceId"]["videoId"]);

            foreach($views["items"] as $view){
                $result_baby[] = [
                    "id" => $item["id"],
                    "title" => $item["snippet"]["title"],
                    "description" => $item["snippet"]["description"],
                    "statistic_videos" => $view["statistics"],
                    "resourceId" =>$item["snippet"]["resourceId"]["videoId"],
                    "thumbnail" => [
                        $item["snippet"]["thumbnails"]
                    ],

                ];
            }
        }
        return $result_baby;
    }
    public function moms_story()
    {
        // helpful resource  mom's-story
        $playlistId = 'PL7vdwX11yNOh9N-89TGVL0ItZtCwzUTRT';
        $maxResults = '1';
        $api_urlst = self::URL.$playlistId.'&maxResults='.$maxResults;
        $sc = curl_init();
        curl_setopt($sc, CURLOPT_URL, $api_urlst);
        curl_setopt($sc, CURLOPT_RETURNTRANSFER, 1);
        $data = curl_exec($sc);
        curl_close($sc);
        $datas = json_decode($data, true);

        foreach($datas["items"] as $item){
            $views = $this->getViewCount($item["snippet"]["resourceId"]["videoId"]);

            foreach($views["items"] as $view){
                $result_stroy[] = [
                    "id" => $item["id"],
                    "title" => $item["snippet"]["title"],
                    "description" => $item["snippet"]["description"],
                    "statistic_videos" => $view["statistics"],
                    "resourceId" =>$item["snippet"]["resourceId"]["videoId"],
                    "thumbnail" => [
                        $item["snippet"]["thumbnails"]
                    ],

                ];
            }
        }
        return $result_stroy;
    }







}
