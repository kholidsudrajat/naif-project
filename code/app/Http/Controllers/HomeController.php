<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Str;
// use App\Http\Controllers\Api\ApiYoutubeController;
use DB;

use App\Profile;
use App\Banner_Page;
use App\Article;
use App\Url_Menu;

class HomeController extends Controller
{


    public function index(Request $request)
    {
        $key = $request->search;
        $value_search = $key;
        $article = DB::select("SELECT * FROM articles ORDER BY created_at DESC");
        $profile = Profile::all();
        $url_menu = Url_Menu::all();
        $banner = DB::select("SELECT * FROM banner_pages WHERE banner_category ='1' ORDER BY created_at DESC LIMIT 5");

        return view('homepage', compact('profile', 'url_menu', 'value_search', 'banner','article'));
    }

    public function article(Request $request, $slug){
        $profile = Profile::all();
        $id_article = Article::where('slug',$slug)->firstOrFail();
        $key = $request->search;
        $value_search = $key;
        $url_menu = Url_Menu::all();

        $article_detail = DB::select("SELECT * FROM articles WHERE id=$id_article->id");
        return view('web.page.article', compact( 'profile', 'url_menu', 'article_detail', 'value_search'));
    }
    function load_data(Request $request)
    {
        if($request->ajax())
        {
            if($request->id > 0)
            {
                $data = DB::table('articles')
                    ->where('id', '<', $request->id)
                    ->orderBy('id', 'DESC')
                    ->limit(3)
                    ->get();
            }
            else
            {
                $data = DB::table('articles')

                            ->orderBy('id', 'DESC')
                            ->limit(4)
                            ->get();
            }
            $output = '';
            $last_id = '';
            if(!$data->isEmpty())
            {
                foreach($data as $row)
                {
                    $output .= '
                                <div class="box-article col-md-3 col-sm-6 col-xs-6 col-6">
                                    <div class="article-image">
                                        <img src="'.url("upload/article/".$row->thumbnail).'" class="img-fluid img-center" alt="Responsive image">
                                    </div>
                                    <div class="article-text article-limit">
                                        <h5>'.$row->title.'</h5>

                                        '. Str::limit($row->description, 100, $end='...')  .'

                                        <div class="article-readmore">
                                            <a href="'.url("article/".$row->slug).'">
                                                <button type="button" class="btn btn-light">Read more</button>
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        
                    ';

                    $last_id = $row->id;
                }
                // $output .= '
                //         <div id="load_more_button" data-id="'.$last_id.'" class="more-articles">
                //             Load more
                //         </div>
                // ';
            }
            else
            {
                // $output .= '
                //     <div id="load_more" class="more-articles width-100">
                //         <p href="#">Data Tidak Ditemukan</p>
                //     </div>
                // ';

            }
            echo $output;
        }
    }
    // fungsi ambil viewer recipe-for-mommy on api
}


